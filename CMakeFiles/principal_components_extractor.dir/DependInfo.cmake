# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/amsl/AMSL_ros_pkg/gauss_sphere/src/principal_components_extractor.cpp" "/home/amsl/AMSL_ros_pkg/gauss_sphere/CMakeFiles/principal_components_extractor.dir/src/principal_components_extractor.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DISABLE_DAVIDSDK"
  "DISABLE_ENSENSO"
  "DISABLE_OPENNI2"
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "ROS_PACKAGE_NAME=\"gauss_sphere\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/amsl/AMSL_ros_pkg/time_util/include"
  "/home/amsl/ros_catkin_ws/devel/include"
  "/opt/ros/indigo/include"
  "/usr/include/vtk-5.8"
  "/usr/local/include/pcl-1.8"
  "/usr/include/eigen3"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
