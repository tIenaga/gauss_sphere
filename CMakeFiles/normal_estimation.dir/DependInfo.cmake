# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/amsl/AMSL_ros_pkg/gauss_sphere/src/normal_estimation.cpp" "/home/amsl/AMSL_ros_pkg/gauss_sphere/CMakeFiles/normal_estimation.dir/src/normal_estimation.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_PACKAGE_NAME=\"gauss_sphere\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/amsl/ros_catkin_ws/devel/include"
  "/opt/ros/indigo/include"
  "/usr/include/vtk-5.8"
  "/usr/include/pcl-1.7"
  "/usr/include/eigen3"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
