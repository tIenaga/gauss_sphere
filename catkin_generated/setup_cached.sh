#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/amsl/AMSL_ros_pkg/gauss_sphere/test_results"
export ROS_TEST_RESULTS_DIR="/home/amsl/AMSL_ros_pkg/gauss_sphere/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/amsl/AMSL_ros_pkg/gauss_sphere/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/amsl/ros_catkin_ws/devel/lib:/opt/ros/kinetic/lib:/usr/local/cuda/lib64"
export PATH="/opt/ros/kinetic/bin:/usr/local/cuda/bin:/home/amsl/bin:/home/amsl/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export ROSLISP_PACKAGE_DIRECTORIES="/home/amsl/AMSL_ros_pkg/gauss_sphere/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/amsl/AMSL_ros_pkg/gauss_sphere:/home/amsl/infant_AMSL_ros_pkg/gauss_sphere:/home/amsl/ros_catkin_ws/src:/opt/ros/kinetic/share"