//
//	pose_estimation.cpp
//
//	last update 2016 / 10 / 25
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;


const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/din_20161109.txt";

bool pc_callback_flag = false;
bool DGauss_match = true;
bool match = false;
bool dgauss_flag = false;
bool is_first = true;
bool lcl_is_first = true;
bool dist_flag = false;
bool is_start = true;

const float RATIO = 0.50;
float accum_dist = 0.0;
float dist_threshold = 1.0;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 rotated_pc2_debug;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pc2_rot;
sensor_msgs::PointCloud2 pre_pc2;
sensor_msgs::PointCloud2 pc2_model;
sensor_msgs::PointCloud2 pc2_query;
nav_msgs::Odometry lcl;
nav_msgs::Odometry lcl_ini;

nav_msgs::Odometry lcl_pre;

nav_msgs::Odometry Dlcl;
nav_msgs::Odometry Dlcl_;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pub2_rot;
ros::Publisher pre_pub2;
ros::Publisher pub_DGauss_pose;
ros::Publisher pub_DGauss_yaw;
ros::Publisher pub_model;
ros::Publisher pub_query;


pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_1 (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_2 (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZ>);

const float gauss_sphere_range = 1.0;
const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]
//const float D_THRESHOLD = 2.0; //[m] model gaussとの距離の閾値
//0825
const float D_THRESHOLD = 3.0; //[m] model gaussとの距離の閾値

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

int cnt=0;
int search_gauss_num = 0;
float dist = 0.0;
vector< vector<float> > dgauss_position;
vector< vector<double> > gmodel;

pcl::PointCloud<pcl::PointXYZ>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZ>);//前ステップにおけるガウス球のための箱
//pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

float getdyaw(float ini, float azm){
	return azm - ini;
}

float calcdist(float x1, float x2, float y1, float y2){
	return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
cout<<"■ ■ ■ ■ ■"<<endl;


	
	//一番初めのループのエラー処理
	//
	/*
	if(is_first){
		pcl::fromROSMsg(*msg, *tmp_1);
		is_first = false;
		return;
	}else{
		*tmp_2 = *tmp_1;
		pcl::fromROSMsg(*msg, *tmp_1);
	}
	*/
	pcl::fromROSMsg(*msg, *tmp_1);
	if(tmp_1->points.size()==0){//msgに主平面が含まれていない場合
		ROS_FATAL("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////

	double yaw = (-M_PI * 0.50) + lcl_ini.pose.pose.orientation.z + getdyaw(lcl_ini.pose.pose.orientation.z,lcl.pose.pose.orientation.z);//座標系を揃えている(?) おそらくmap座標系で考えている
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZ>);
	
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_m (new pcl::PointCloud<pcl::PointXYZ>);//モデル、クエリの可視化用
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	pcl::fromROSMsg(*msg, *tmp);
	
	MatrixXd Vector(3,1);
	Vector<<0.0,
			0.0,
			0.0;
	
	int m_pair[(size_t)gmodel.size()];//modelのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)gmodel.size();i++)	m_pair[i]=0;
	int q_pair[(size_t)tmp_cloud->points.size()];//queryのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++)	q_pair[i]=0;	
	
	int gcnt = tmp_1->points.size();
	size_t cnt_pair=0;//軸方向が何面存在するかのカウンタ

	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	Eigen::Vector3f model_vector;
	Eigen::Vector3f query_vector;
	float dot_ij[(int)gmodel.size()][3];
	float aaa[3] = {0.000,0.000,0.000};
	float theta[tmp_cloud->points.size()];
	float dyaw = 0.0;
	float theta_count = 0.0;
	
	*tmp_rot = *tmp_1;

	double cos_ = cos(yaw);
	double sin_ = sin(yaw);
	
	//cout << "tmp_cloud->points[i]_____"<<endl;
	
	for(size_t i=0;i<(size_t)tmp_1->points.size();i++){
		tmp_1->points[i].x = ( tmp_rot->points[i].x * cos_ ) - ( tmp_rot->points[i].y * sin_ );
		tmp_1->points[i].y = ( tmp_rot->points[i].x * sin_ ) + ( tmp_rot->points[i].y * cos_ );
		//cout << tmp_cloud->points[i].x <<","<< tmp_cloud->points[i].y <<","<< tmp_cloud->points[i].z <<","<< tmp_cloud->points[i].intensity << endl;
	}
	
	
	//tmp_1,tmp_2をgmodel[][]とtmp_cloudに代入する(簡単のため)
	//	gmodel		->	tmp_1(新しい主平面)
	//	tmp_cloud	->	1[m]おきに設置する主平面
	//モデルとして設置 //初めのループ
	if(is_start){
		*tmp_cloud = *tmp_1;
		is_start = false;
		return;
	}
	
	//*tmp_cloud = *tmp_1;
	
	gmodel.resize(tmp_1->points.size());
	for(size_t i=0;i<tmp_1->points.size();i++){
		gmodel[i].resize(3);
	}
	
	for(size_t i=0;i<tmp_1->points.size();i++){
		gmodel[i][0] = tmp_1->points[i].x;
		gmodel[i][1] = tmp_1->points[i].y;
		gmodel[i][2] = tmp_1->points[i].z;
	}
	
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		if(calcdist(tmp_cloud->points[i].x,0.0,tmp_cloud->points[i].y,0.0)>=1.0){
			for(int j=0;j<(int)gmodel.size();j++){
			
				model_vector << (gmodel[j][0])
							   ,(gmodel[j][1])
							   ,(gmodel[j][2]);
				query_vector << (tmp_cloud->points[i].x)
							   ,(tmp_cloud->points[i].y)
							   ,(tmp_cloud->points[i].z);
			
				dot_ij[j][0] = i;
				dot_ij[j][1] = j;
				dot_ij[j][2] = model_vector.dot(query_vector) / ( model_vector.norm() * query_vector.norm() );//cosΘ
				
//cout<<"( i , j ) : ( "<<i<<" , "<<j<<" )"<<endl;
			}
			//並び替え(大きい順に)
			//
			for(int j=0;j<(int)gmodel.size()-1;j++){
				for(int k=1;k<(int)gmodel.size();k++){
					if(dot_ij[j][2] < dot_ij[k][2]){
						aaa[0] = dot_ij[k][0];
						aaa[1] = dot_ij[k][1];
						aaa[2] = dot_ij[k][2];
					
						dot_ij[k][0] = dot_ij[j][0];
						dot_ij[k][1] = dot_ij[j][1];
						dot_ij[k][2] = dot_ij[j][2];
					
						dot_ij[j][0] = aaa[0];
						dot_ij[j][1] = aaa[1];
						dot_ij[j][2] = aaa[2];
					}
				}
			}
			int box = dot_ij[0][1];
			
			//ここにi,jがはいっている
			//dot_ij[0][0] = i;
			//dot_ij[0][1] = j;
			//cout<<"gmodel : "<<gmodel<<endl;
			
			
			float dist_mq;
			float cross = (tmp_cloud->points[dot_ij[0][0]].x * gmodel[dot_ij[0][1]][1]) - (gmodel[dot_ij[0][1]][0] * tmp_cloud->points[dot_ij[0][0]].y);
			
			float norm = sqrt( (tmp_cloud->points[dot_ij[0][0]].x)*(tmp_cloud->points[dot_ij[0][0]].x) + (tmp_cloud->points[dot_ij[0][0]].y)*(tmp_cloud->points[dot_ij[0][0]].y) )
							* sqrt( (gmodel[dot_ij[0][1]][0])*(gmodel[dot_ij[0][1]][0]) + (gmodel[dot_ij[0][1]][1])*(gmodel[dot_ij[0][1]][1]));

			theta[i] = asin(cross/norm);
			
			dot_ij[0][1] = box;
			
			dist_mq = calcdist(tmp_cloud->points[dot_ij[0][0]].x ,gmodel[dot_ij[0][1]][0] ,tmp_cloud->points[dot_ij[0][0]].y ,gmodel[dot_ij[0][1]][1] );
			if( (abs(theta[i])<=0.1736) && (dot_ij[0][2]>=0.9848) && dist_mq<=3.0){//10[deg]以下なら
			//if( (abs(theta[i])<=0.261799) && (dot_ij[0][2]>=0.9659258) && dist_mq<=3.0){//15[deg]以下なら
				dyaw += theta[i];
				theta_count++;
	
			}
		}
		
	}
	//dyaw /= tmp_cloud->points.size();
	cout<<"dyaw割る前 : "<<dyaw<<endl;
	dyaw /= theta_count;//前ステップと今のステップでの主平面の対応点の数
	if(theta_count == 0.0 || dyaw == 0.000){
		cout<<"theta_count : 0"<<endl;
		Dlcl_.pose.pose.orientation.w = 0.0;//flagとして使った
	}else{
		Dlcl_.pose.pose.orientation.w = theta_count;
	}
	Dlcl_.pose.pose.orientation.z = dyaw;// - 0.03338629;
	pub_DGauss_yaw.publish(Dlcl_);
	
	if(theta_count == 0.0){
		cout<<" RETURN "<<endl;
		return;
	}
	/*
	if(isnan(dyaw)){
//0905		ROS_FATAL("■■■■■ NAN IS INCLUDED IN THE VALUE ■■■■■");
	}else{
	}
	*/
	if(dist_flag){
		*tmp_rot = *tmp_1;
	
		cos_ = cos(dyaw);
		sin_ = sin(dyaw);
	
		for(size_t i=0;i<(size_t)tmp_1->points.size();i++){
			tmp_1->points[i].x = ( tmp_rot->points[i].x * cos_ ) - ( tmp_rot->points[i].y * sin_ );
			tmp_1->points[i].y = ( tmp_rot->points[i].x * sin_ ) + ( tmp_rot->points[i].y * cos_ );
		}
		*tmp_cloud = *tmp_1;
		
		cout<<"modelの変更！！！！！！"<<endl;
		accum_dist = 0.0;
		dist_flag = false;
	}
	
	cout<<" dyaw[rad] : "<<dyaw<<endl;
	cout<<" dyaw[deg] : "<<dyaw * 180.0 / M_PI<<endl;
	return;

	//	debug用にモデルとクエリを可視化する
	//
	
	pcl::toROSMsg(*pub_cloud_m, pc2_model);
	pcl::toROSMsg(*pub_cloud_q, pc2_query);
	
	pc2_query.header.frame_id = "/velodyne";
	pc2_query.header.stamp = ros::Time::now();
	pub_query.publish(pc2_query);
	pc2_query.data.clear();
	
	pc2_model.header.frame_id = "/velodyne";
	pc2_model.header.stamp = ros::Time::now();
	pub_model.publish(pc2_model);
	pc2_model.data.clear();

	//cout<<"cnt_pair : "<<cnt_pair<<endl;

	if(cnt_pair == 0){
		//cout<<"--  NO PAIR  --"<<endl;
		return;
	}else if(cnt_pair == 1){

		//cout<<" !!!!! cnt_pair = 1 !!!!! "<<endl;
		double R[2],M[2],r[2],d[2],theta,abs_d,abs_r;
		//R : subscribeした自己位置の座標(/map上)
		R[0] = lcl_ini.pose.pose.position.x;
		R[1] = lcl_ini.pose.pose.position.y;
//0905		cout<<"R[0] : "<<R[0]<<"  R[1] : "<<R[1]<<endl;
		//M : model gaussの設置位置(/map上)
		cout<<"search_gauss_num : "<<search_gauss_num<<endl;
		M[0] = dgauss_position[search_gauss_num-1][0];
		M[1] = dgauss_position[search_gauss_num-1][1];
//0905		cout<<"M[0] : "<<M[0]<<"  M[1] : "<<M[1]<<endl;
		//r = R - M
		r[0] = R[0] - M[0];
		r[1] = R[1] - M[1];
		//d = Vec
		d[0] = Vector(0,0);
		d[1] = Vector(1,0);
		
		abs_d = sqrt( d[0]*d[0] + d[1]*d[1] );
		abs_r = sqrt( r[0]*r[0] + r[1]*r[1] );
		theta = acos( ( d[0]*r[0] + d[1]*r[1] ) / ( sqrt( abs_d ) * sqrt( abs_r ) ) );
		
		Vector(0,0) = r[0] + ( d[0] * ( 1.0 - ( (abs_r / abs_d) * cos(theta) ) ) );
		Vector(1,0) = r[1] + ( d[1] * ( 1.0 - ( (abs_r / abs_d) * cos(theta) ) ) );
		cout<<"Vector(0,0) : "<<Vector(0,0)<<" Vector(1,0) : "<<Vector(1,0)<<endl;
		return;//1010
	}

	//if(match) printf("x, y, z = %1.3f ,%1.3f ,%1.3f ",Vector(0,0),Vector(1,0),Vector(2,0));
//0905	cout<<"■■■■■■■■■■■■■■■■■■■■"<<endl;
	cout << " x : " << Vector(0,0) << endl;
	cout << " y : " << Vector(1,0) << endl;
//0905	cout << " z : " << Vector(2,0) << endl;
	
	if(calcdist(0.0,Vector(0,0),0.0,Vector(1,0))>=D_THRESHOLD){
		ROS_FATAL("!!!!! MATCHING RESULT SEEMS WRONG !!!!!!");
		return;
	}
	
	if(isnan(Vector(0,0)) || isnan(Vector(1,0)) ){
		ROS_FATAL("■■■■■ NAN IS INCLUDED IN THE VALUE ■■■■■");
		return;
	}else{
				}
	//	
	//	matching しなかったときの対応(flagを使う)
	//	
	
	//0825
	//Dlcl.pose.pose.position.x = Vector(0,0);//localization_testにおいて変位量だけを受け取りたいため//0927
	//Dlcl.pose.pose.position.y = Vector(1,0);
	
	/*
	double estimated_pose_x = 0.0;
	double estimated_pose_y = 0.0;
	estimated_pose_x = Vector(0,0) + dgauss_position[search_gauss_num-1][0];
	estimated_pose_y = Vector(1,0) + dgauss_position[search_gauss_num-1][1];
	Dlcl.pose.pose.position.x = estimated_pose_x - lcl.pose.pose.position.x;//localization_testにおいて変位量だけを受け取りたいため//0927
	Dlcl.pose.pose.position.y = estimated_pose_y - lcl.pose.pose.position.y;
	*/
cout<<"■■■■■■■■■"<<endl;		
	Dlcl.pose.pose.position.x = Vector(0,0) + dgauss_position[search_gauss_num-1][0];//txtから読み込んだ際は0始まり，
	Dlcl.pose.pose.position.y = Vector(1,0) + dgauss_position[search_gauss_num-1][1];
	//Dlcl.pose.pose.orientation.z = dyaw;
	
	cout<<"Dlcl.x : "<<Dlcl.pose.pose.position.x<<endl;
	cout<<"Dlcl.y : "<<Dlcl.pose.pose.position.y<<endl;
	
	pub_DGauss_pose.publish(Dlcl);
	
//	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud, rotated_pc2_debug);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
//	pcl::toROSMsg(*tmp_rot, pc2_rot);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	rotated_pc2_debug.header.frame_id = "/velodyne";//クラスタした点
	rotated_pc2_debug.header.stamp = ros::Time::now();
	pub2.publish(rotated_pc2_debug);
	rotated_pc2_debug.data.clear();

	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();

	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
//0905	cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	dgauss_flag=false;
}

void lcl_callback(nav_msgs::Odometry msg){
	if(lcl_is_first == false){
		lcl_pre.pose.pose.position.x = lcl.pose.pose.position.x;
		lcl_pre.pose.pose.position.y = lcl.pose.pose.position.y;
		lcl_pre.pose.pose.position.z = 0.0;
		lcl_pre.pose.pose.orientation.z = lcl.pose.pose.orientation.z;
	}
	
	lcl.pose.pose.position.x = msg.pose.pose.position.x;
	lcl.pose.pose.position.y = msg.pose.pose.position.y;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = msg.pose.pose.orientation.z;
//	cout<< lcl.pose.pose.orientation.z * 180 / M_PI <<endl;
	cout<<"accum_dist : "<<accum_dist<<endl;
	if(lcl_is_first == false){
		accum_dist += calcdist(lcl_pre.pose.pose.position.x , lcl.pose.pose.position.x , lcl_pre.pose.pose.position.y , lcl.pose.pose.position.y);
		if(accum_dist >= dist_threshold){
			dist_flag = true;
			cout<<"accum_dist : "<<accum_dist<<endl;
			//accum_dist = 0.0;
		}
	}
	
	lcl_is_first = false;
}

void lcl_ini_callback(nav_msgs::Odometry msg){
	lcl_ini.pose.pose.position.x = msg.pose.pose.position.x;
	lcl_ini.pose.pose.position.	y = msg.pose.pose.position.y;
	lcl_ini.pose.pose.orientation.z = msg.pose.pose.orientation.z;
	
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "relative_pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate loop(20);
	
	//ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);//for perfect velodyne pose_estimation//1105
	
	ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	ros::Subscriber sub_lcl_ini = n.subscribe("/lcl/initial_pose",1,lcl_ini_callback);
	
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rotated_pc2_query",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	//pub2_rot = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rot",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	pub_DGauss_yaw = n.advertise<nav_msgs::Odometry>("/gauss_sphere/yaw", 10);
	
	pub_model = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/model",1);
	pub_query = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/query",1);
	
	//////////////////////////////////////////////////////////////////
	//	Dgaussを Dgauss_list/ikuta.txtから読み取る
	//	txt内は
	//	
	//	x , y <- /map でのDgaussの位置を保存していく
	//	
	//////////////////////////////////////////////////////////////////
	
	FILE* fp;

	dgauss_position.resize(10000);
	for(int i=0;i<10000;i++){
		dgauss_position[i].resize(2);
	}
	
	if( (fp = fopen(file_name,"r")) == NULL){
		cout << "Dgauss_list の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}else{
		float a,b = 0.0;
		while(fscanf(fp, "%f,%f", &a, &b) != EOF) {	
			dgauss_position[cnt][0] = a;
			dgauss_position[cnt][1] = b;
			cout<<dgauss_position[cnt][0]<<"  "<<dgauss_position[cnt][1]<<endl;
			
			/*
			visualization_msgs::Marker marker;
			marker.header.frame_id = "map";
			marker.header.stamp = ros::Time();
			marker.ns = "dgauss_point";
			marker.id = cnt;
			marker.type = visualization_msgs::Marker::SPHERE;
			marker.action = visualization_msgs::Marker::ADD;
			marker.pose.position.x = a;
			marker.pose.position.y = b;
			marker.pose.position.z = 0;
			marker.pose.orientation.x = 0.0;
			marker.pose.orientation.y = 0.0;
			marker.pose.orientation.z = 0.0;
			marker.pose.orientation.w = 0.0;
			marker.scale.x = 1;
			marker.scale.y = 0.1;
			marker.scale.z = 0.1;
			marker.color.a = 1.0; // Don't forget to set the alpha!
			marker.color.r = 0.0;
			marker.color.g = 1.0;
			marker.color.b = 0.0;
			vis_pub.publish( marker );
			
			
			*/
			cnt++;
		}
		dgauss_position.resize(cnt);
	}
	fclose(fp);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		
		//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_test.txt";
		
		//FILE* fp2;
		//char file_name_1[100];
		//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",search_gauss_num);
		//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_test/%d.txt",search_gauss_num);
	
		
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
