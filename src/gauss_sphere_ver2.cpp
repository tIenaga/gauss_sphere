//
//	last update 1105
//


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <ctime>


using namespace std;
using namespace Eigen;

typedef pcl::PointXYZINormal PointIN;
typedef pcl::PointCloud<PointIN> CloudIN;
typedef CloudIN::Ptr CloudINPtr;

bool pc_callback_flag = false;
CloudINPtr pc_input (new CloudIN);

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

//Voxel grid
const double LEAF = 0.075;

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

inline void pubPointCloud2(	ros::Publisher& pub,
							const CloudIN& pcl_in,
							const char* frame_id,
							ros::Time& time)
{
		sensor_msgs::PointCloud2 ros_out;
		pcl::toROSMsg(pcl_in, ros_out);
		ros_out.header.frame_id = frame_id;
		ros_out.header.stamp    = time;
		pub.publish(ros_out);
}

inline bool IsPointGround(const pcl::PointXYZINormal& p)
{
		if(fabs(p.normal_z) > 0.8 || p.z <= -0.5){
			return true;
		}
		return false;
}

inline void calcColor(float& color, const pcl::PointXYZINormal& p)
{
	float xyz[3] = {0.0};
	xyz[0] = p.x;
	xyz[1] = p.y;
	xyz[2] = p.z;

	float max = 0.0, min = 1.0;
	for(int j=0;j<3;j++){
		if(max < fabs(xyz[j])){
			max = fabs(xyz[j]);
		}
		if(min > fabs(xyz[j])){
			min = fabs(xyz[j]);
		}
	}

	if((fabs(p.x)>fabs(p.y)) && (fabs(p.x)>fabs(p.z))){
		if(p.x<0){
			color = 30*(fabs(p.y)-fabs(p.z))/(max-min)+180.0;
		}
		else{
			color = 30*(fabs(p.y)-fabs(p.z))/(max-min);
		}
	}else if((fabs(p.y)>fabs(p.z)) && (fabs(p.y)>fabs(p.x))){
		if(p.y<0){
			color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0+180.0;
		}
		else{
			color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0;
		}
	}else if((fabs(p.z)>fabs(p.x)) && (fabs(p.z)>fabs(p.y))){
		if(p.z<0){
			color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0+180.0;
		}
		else{
			color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0;
		}
	}

	if(color<0.0) color = 0.0;
	else if(color>360.0) color = 360.0;
}

inline void Point2Gauss(pcl::PointXYZINormal& point, pcl::PointXYZINormal& gauss)
{
	gauss.x         = gauss_sphere_range * point.normal_x;
	gauss.y         = gauss_sphere_range * point.normal_y;
	gauss.z         = gauss_sphere_range * point.normal_z;
	gauss.normal_x  = point.normal_x;
	gauss.normal_y  = point.normal_y;
	gauss.normal_z  = point.normal_z;
	gauss.curvature = point.curvature;
}

inline void Point2DGauss(	pcl::PointXYZINormal& point,
							pcl::PointXYZINormal& gauss,
							pcl::PointXYZINormal& dgauss)
{
	float range = (point.normal_x*point.x+point.normal_y*point.y+point.normal_z*point.z)
					/(sqrt(pow(point.normal_x,2)+pow(point.normal_y,2)+pow(point.normal_z,2)));
	dgauss.x         = range * gauss.x;
	dgauss.y         = range * gauss.y;
	dgauss.z         = range * gauss.z;
	dgauss.normal_x  = point.x;
	dgauss.normal_y  = point.y;
	dgauss.normal_z  = point.z;
	dgauss.curvature = point.curvature;
}

void pc_callback(sensor_msgs::PointCloud2::ConstPtr msg){
	pc_callback_flag = true;
	pcl::fromROSMsg(*msg , *pc_input);
}

void MAX(int in, int *max){
	if(in > *max) *max =in;
	else{
	}
}

void filtering(CloudIN pc_in, CloudIN *pc_output){

	CloudIN pc, pc_out1;
	pc = pc_in;

	int color_cluster_num = int(color_num/color_res);
	hist color[color_cluster_num];
	//init
	for(int i=0;i<color_cluster_num;i++){
		color[i].num = 0;
		color[i].normal_num = 0.0;
		color[i].x = 0.0;
		color[i].y = 0.0;
		color[i].z = 0.0;
		color[i].nx = 0.0;
		color[i].ny = 0.0;
		color[i].nz = 0.0;
		color[i].curv = 0.0;
		color[i].inten = 0.0;
	}

	//make_color_hist
	for(size_t i=0;i<pc.points.size();i++){
		int c_num = int(pc.points[i].intensity/color_res);
		color[c_num].num++;
		color[c_num].x += pc.points[i].x;
		color[c_num].y += pc.points[i].y;
		color[c_num].z += pc.points[i].z;
		color[c_num].nx += pc.points[i].normal_x;
		color[c_num].ny += pc.points[i].normal_y;
		color[c_num].nz += pc.points[i].normal_z;
		color[c_num].curv += pc.points[i].curvature;
		color[c_num].inten += pc.points[i].intensity;
	}

	int max = 0;
	for(int i=0;i<color_cluster_num;i++) MAX(color[i].num, &max);
	for(int i=0;i<color_cluster_num;i++){
		color[i].normal_num = color[i].num/float(max);
		color[i].x /= color[i].num;
		color[i].y /= color[i].num;
		color[i].z /= color[i].num;
		color[i].nx /= color[i].num;
		color[i].ny /= color[i].num;
		color[i].nz /= color[i].num;
		color[i].curv /= color[i].num;
		color[i].inten /= color[i].num;
		if(color[i].normal_num > efficient){
			pcl::PointXYZINormal p;
			p.x = color[i].x;
			p.y = color[i].y;
			p.z = color[i].z;
			p.normal_x = color[i].nx;
			p.normal_y = color[i].ny;
			p.normal_z = color[i].nz;
			p.curvature = color[i].curv;
			p.intensity = color[i].inten;
			pc_out1.points.push_back(p);
		}else{
		}
	}

	*pc_output = pc_out1;
}



void make_gauss_sphere(	CloudINPtr pc, 
						CloudIN *gauss_cloud,
						CloudIN *gauss_cloud_rm, 
						CloudIN *dgauss_cloud,
						CloudIN *dgauss_cloud_rm)
{
	CloudIN pc_out, pc_out_rm;
	CloudIN pc_out2, pc_out2_rm;
	CloudIN pc_out_c;
	size_t size_pc_points = pc->points.size();
	for(size_t i=0;i<size_pc_points;i++){
		//for localization
		if (IsPointGround(pc->points[i])){
			continue;
		}
		
		PointIN gauss;
		Point2Gauss(pc->points[i], gauss);
		float color = 0.0;
		calcColor(color, gauss);
		gauss.intensity = color;
		pc_out.points.push_back(gauss);

		PointIN dgauss;
		Point2DGauss(pc->points[i], gauss, dgauss);
		dgauss.intensity = color;
		pc_out2.points.push_back(dgauss);
	}

	filtering(pc_out, &pc_out_c);//これがモデルとの違いの場所

	*gauss_cloud = pc_out;
	//*gauss_cloud_rm = pc_out_rm;
	*gauss_cloud_rm = pc_out_c;
	*dgauss_cloud = pc_out2;
	*dgauss_cloud_rm = pc_out2_rm;
}

void downSample(CloudINPtr pcl_in, CloudINPtr pcl_out)
{
	//--- Voxel Grid Filter ---//
	pcl::VoxelGrid<pcl::PointXYZINormal> vg_filter;
	vg_filter.setLeafSize(LEAF, LEAF, LEAF);
	vg_filter.setInputCloud (pcl_in);
	vg_filter.filter(*pcl_out);
}

int main (int argc, char** argv)
{
	ros::init(argc, argv, "gauss_sphere");
	ros::NodeHandle n;
	
	ros::Rate roop(10);//10//0803にこの値から変更(主平面の確認のために)
						//20//1008
						
	
	//
	//use ↓ with "rosrun pcd_loader pcd_loader"
	//ros::Subscriber sub = n.subscribe("pcd_file_all",1,pc_callback);//1026hagi
	
	ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	
	//ros::Subscriber sub = n.subscribe("PointCloud_21",1,pc_callback);//1026hagi
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォのトピック名
	
   	ros::Publisher pub_gauss_sphere         = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f       = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth   = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	cout << "Here We Go !!!" << endl;
	
	while(ros::ok()){

		if(pc_callback_flag && pc_input->points.size()>0){
			//////////////処理時間の計測//////////////
			struct timeval b_sec, a_sec;
			gettimeofday(&b_sec, NULL);
			/////////////////////////////////////////
			
			CloudIN pc_gauss_sphere, pc_gauss_sphere_f;
			CloudIN pc_gauss_sphere_depth, pc_gauss_sphere_depth_f;
			CloudINPtr dw_p (new CloudIN);
			downSample(pc_input, dw_p);
			make_gauss_sphere(dw_p, &pc_gauss_sphere, &pc_gauss_sphere_f,
								&pc_gauss_sphere_depth, &pc_gauss_sphere_depth_f);
			
			//////////////処理時間の計測//////////////
			gettimeofday(&a_sec, NULL);
			cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
			/////////////////////////////////////////
			
			ros::Time time = ros::Time::now();
			pubPointCloud2(pub_gauss_sphere, pc_gauss_sphere, "/velodyne", time);
			pubPointCloud2(pub_gauss_sphere_f, pc_gauss_sphere_f, "/velodyne", time);
			pubPointCloud2(pub_gauss_sphere_depth, pc_gauss_sphere_depth, "/velodyne", time);
			pubPointCloud2(pub_gauss_sphere_depth_f, pc_gauss_sphere_depth_f,
							"/velodyne", time);

			pc_callback_flag = false;
		}		

		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
