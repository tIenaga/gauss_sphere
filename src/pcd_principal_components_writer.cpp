//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : model_principal_components_writer.cpp
//author    : x.xxx
//created   : 2016.08.02
//lastupdate: 2016.



#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

#include <clustering_writer.h>
#include <time_util/stopwatch.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

sensor_msgs::PointCloud2 ros_dgs;
sensor_msgs::PointCloud2 pc2;

ros::Publisher pub_normal;
ros::Publisher pub_gs;
ros::Publisher pub_dgs;
ros::Publisher pub_gsc;
ros::Publisher pub_dgsc;

pcl::PointCloud<pcl::PointSurfel> pc_input;
pcl::PointCloud<pcl::PointSurfel> pc_tmp;
pcl::PointCloud<pcl::PointSurfel> dom_clusters;//dom=dominant
pcl::PointCloud<pcl::PointSurfel> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
pcl::PointCloud<pcl::PointSurfel>pc_out;
bool pc_callback_flag = false;

const float THRESH_NUM=0.0;
const float gauss_sphere_range = 1.0;
const float DIST = 20.0;
const float DIST_NODE = 5.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}

void pcl2ros(pcl::PointCloud<pcl::PointSurfel> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

//	/backup/1_model_principal_components_writer.cpp におけるmake_gauss_sphereをコピペした
//
//
void make_gauss_sphere_color(	pcl::PointCloud<pcl::PointSurfel> pc,
					  	pcl::PointCloud<pcl::PointSurfel> *pc_gs,
						pcl::PointCloud<pcl::PointSurfel> *pc_dgs)
{
	pcl::PointCloud<pcl::PointSurfel> pc_out, pc_out2;
	pcl::PointCloud<pcl::PointSurfel> pc_out_c;
	size_t size_pc_points = pc.points.size();
	for(size_t i=0;i<size_pc_points;i++){
		pcl::PointSurfel p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;
		
		//for localization
		if(fabs(p.normal_z) > 0.8) continue;
		if(p.z <= -0.5) continue;
		
		float color = 0.0;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;

		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			else{
			}

			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
			else{
			}
		}

		if((fabs(p.x)>fabs(p.y)) && (fabs(p.x)>fabs(p.z))){
			if(p.x<0) color = 30*(fabs(p.y)-fabs(p.z))/(max-min)+180.0;
			else color = 30*(fabs(p.y)-fabs(p.z))/(max-min);
		}else if((fabs(p.y)>fabs(p.z)) && (fabs(p.y)>fabs(p.x))){
			if(p.y<0) color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0;
		}else if((fabs(p.z)>fabs(p.x)) && (fabs(p.z)>fabs(p.y))){
			if(p.z<0)color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0;
		}else{
		}

		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		//p.intensity = color;
		p.radius = color;
		pc_out.points.push_back(p);//単位ガウス球

		pcl::PointSurfel pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		//pp.intensity = color;
		pp.radius = color;
			
		float range = (pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		pc_out2.points.push_back(pp);//深さつきガウス球
	}
	
	*pc_gs = pc_out;
	*pc_dgs = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
	
}

//	perfect_velodyneのものはcallbackなので名前を変更した
//  元の名前はclustering
//	<-perfect_velodyneで用いているクラスタリングの名前を変更した奴
//
void dominant_clustering(pcl::PointCloud<pcl::PointSurfel> *dgauss_sphere)
{
	Clustering clustering;
	clustering.setMinMaxNum(100,INT_MAX);
	//clustering.setThreshold(0.6, 0.1);	//2016.08.25 comment out -> principal_componets_extracter_for_DGaussとパラメータを同じにした（↓）
	clustering.setThreshold(0.80, 0.1);	//2016.01.21 comment out
	//clustering.setThreshold(0.80, 0.5);	//angle and distance
	//clustering.setThreshold(0.80, 0.8);	//angle and distance
	
	//*ros_dgs->data.clear();
	pcl::toROSMsg(*dgauss_sphere, ros_dgs);
	clustering.putTank(ros_dgs);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc2);
	
	//ros_dgs.header.frame_id = "/velodyne";
	//ros_dgs.header.stamp = ros::Time::now();
	pcl::fromROSMsg(pc2, dom_clusters);

	//pcl::toROSMsg(dom_clusters, pc2_w);
	//pc2_w.header.frame_id="velodyne";
	//pc2_w.header.stamp=ros::Time::now();
	//pub2.publish(pc2_w);
	//pcl::PointCloud<pcl::PointSurfel> pc_dgsc;

}


int main (int argc, char** argv)
{

	ros::init(argc, argv, "model_principal_components_writer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!model_principal_components_writer!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	//graphファイルの読み込み
	//
	FILE *fp;
	fp = fopen("aft.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	//保存先ファイルの作成
	//
	FILE *fp2;
	char filename_2[100];
	
	sprintf(filename_2,"/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_robosym2016.txt");//ファイルの作成
	
	
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	pub_normal = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal",1);
	pub_gs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	pub_gsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_clustered",1);
   	pub_dgs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	pub_dgsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_clustered",1);
	
	ros::Publisher pub_drawed_map;
	sensor_msgs::PointCloud2 pc;

	int file_num = 1;

		
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			//////////////処理時間の計測//////////////
			struct timeval b_sec, a_sec;
			gettimeofday(&b_sec, NULL);
			/////////////////////////////////////////
			
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );//nodeのx,y座標をtsukuba_all.txtに書き込む
			
			char filename[100];
			//sprintf(filename,"clouds/cloud_%d.pcd",num);
			sprintf(filename,"drawed_map/%d.pcd",file_num);
	
			pc_tmp.points.clear();
			pcl::PointCloud<pcl::PointSurfel>::Ptr map_cloud (new pcl::PointCloud<pcl::PointSurfel>);
			if (pcl::io::loadPCDFile<pcl::PointSurfel> (filename, *map_cloud) == -1){
				cout << file_num <<".pcd not found." << endl;
				break;
			}
	
			//読み込んだ地図のpublish
			//
			cout<<"Map Visualization .."<<endl;
			sensor_msgs::PointCloud2 ros_normal;
			pcl::toROSMsg(*map_cloud, ros_normal);
			ros_normal.header.frame_id = "/velodyne";
			ros_normal.header.stamp = ros::Time::now();
			pub_normal.publish(ros_normal);
		    
			//Dgauss_sphereを作る(make_gauss_sphere)
			//
			pcl::PointCloud<pcl::PointSurfel> pc_gs, pc_dgs;
			make_gauss_sphere_color(*map_cloud, &pc_gs, &pc_dgs);
			
			//主平面を取り出す(filtering)
			//
			dominant_clustering(&pc_dgs);
			
			char outfile[100];
			for(size_t i=0;i<dom_clusters.points.size();i++){
				cout<<dom_clusters.points[i].x<<" , "<<dom_clusters.points[i].y<<endl;
			}
			FILE *fp1;
			char filename_1[100];
			sprintf(filename_1,"/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_robosym2016/%d.txt",file_num);//ファイルの作成
			 
			if((fp1 = fopen(filename_1, "a+")) == NULL){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			
			// 処理（ここでは、下記文字をファイルに書き込む）
			for(size_t i=0;i<dom_clusters.points.size();i++){
				if(calcdist(dom_clusters.points[i].x,dom_clusters.points[i].y,0.0,0.0)>=1.0){//0912 0.0->1.0へ変更
					fprintf(fp1, "%f,%f,%f\n", dom_clusters.points[i].x,dom_clusters.points[i].y,dom_clusters.points[i].z);
				}
			}
			
			// ファイルを閉じる 
			fclose(fp1);
			
			//pc_dgsc の初期化
			//
			pc_dgsc.points.clear();
			
			//////////////処理時間の計測//////////////
			gettimeofday(&a_sec, NULL);
			cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
			/////////////////////////////////////////
			cnt += skip;
			file_num ++;
			
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);
/////////////////////////////		
	
	cout<<"Finish."<<endl;
}

