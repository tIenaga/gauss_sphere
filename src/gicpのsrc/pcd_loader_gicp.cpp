//pcd_loader.cpp
//s.shimizu
//指定されたpcdファイルをsensor_msgs::PointCloud2に変換し、publishします。

#include <stdio.h>
#include <iostream>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Point.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <ros/ros.h>
#include <pcl_conversions/pcl_conversions.h>

#define MAX_NUM 100

unsigned int mode;

using namespace std;	

float filter;
float cross_product(std::vector<float> v1, std::vector<float> v2);
float dot_product(std::vector<float> v1, std::vector<float> v2);
pcl::PointXYZRGB n_dist(pcl::PointNormal p);

void copy (pcl::PointCloud<pcl::PointNormal> &in, pcl::PointCloud<pcl::PointXYZ> &rsl)
{
	size_t size = in.points.size();
	rsl.points.resize(size);
	for(size_t i=0;i<size;i++){
		for(size_t j=0;j<3;j++){	
			rsl.points[i].data[j]=in.points[i].data[j];
		}
	}
}

void cnv(pcl::PointCloud<pcl::PointNormal> org, pcl::PointCloud<pcl::PointXYZRGB> &rsl)
{
	cout << "normal conv" << endl;
	pcl::PointXYZRGB p;
	for(size_t i=0;i<org.points.size();i++){	

		pcl::PointNormal org_p;
		org_p = org.points[i];
//		cout << org_p << endl;
if(mode == 1){		
		p = n_dist(org_p);

}else if(mode == 2){
		p.x = org.points[i].x;
		p.y = org.points[i].y;
		p.z = org.points[i].z;
	//	p.r = (unsigned int)255*fabs(org.points[i].normal_x);
	//	p.g = (unsigned int)255*fabs(org.points[i].normal_y);
	//	p.b = (unsigned int)255*fabs(org.points[i].normal_z);
	//	p.r = (unsigned int)128*(0.5+0.5*org.points[i].normal_x);
	//	p.g = (unsigned int)128*(0.5+0.5*org.points[i].normal_y);
	//	p.b = (unsigned int)128*(0.5+0.5*org.points[i].normal_z);

	//#p.r = (unsigned int)255*fabs(org_p.curvature*5.0);
	//#p.g = 0.0;
	//#p.b = (unsigned int)(255 - 255*fabs(org_p.curvature*5.0));
//---------------ark_tuika----------------------------------------------
		float curv = 0.0;
		curv = org_p.curvature;//max = 1.0, min = 0.0
 if(curv > 0.0)
    cout<< curv << endl;
        if(curv < 0.1*0.5){//0.1
			p.r = 0.0;
			p.g = 255;
			p.b = 0.0;
		}else if(curv >= 0.1*0.5 && curv < 0.3*0.5){
			p.r = 255*(1.0 - (0.3*0.5-curv)/0.2*0.5);
			p.g = 255;
			p.b = 0.0;
		}else if(curv >= 0.3*0.5 && curv < 0.5*0.5){
			p.r = 255;
			p.g = 255*(0.5*0.5 - curv)/0.2*0.5;
			p.b = 0.0;
		}else if(curv >= 0.5*0.5 && curv < 0.7*0.5){
			p.r = 255;
			p.g = 0.0;
			p.b = 255*(1.0 - (0.7*0.5-curv)/0.2*0.5);
		}else if(curv >= 0.7*0.5 && curv < 0.9*0.5){
			p.r = 255*(0.9*0.5 - curv)/0.2*0.5;
			p.g = 0.0;
			p.b = 255;
		}else{
			p.r = 0.0;
			p.g = 0.0;
			p.b = 255;
		}

}else if(mode == 3){
		p.x = -org.points[i].normal_x;
		p.y = -org.points[i].normal_y;
		p.z = -org.points[i].normal_z;
	//	if(i%1000 == 0)cout << p.x <<" "<< p.y << endl;
		p.r = (unsigned int)128*(0.5+0.5*org.points[i].normal_x);
		p.g = (unsigned int)128*(0.5+0.5*org.points[i].normal_y);
		p.b = (unsigned int)128*(0.5+0.5*org.points[i].normal_z);
//		p.r = (unsigned int)255*fabs(org_p.curvature*3.0);
//		p.g = 0.0;
//		p.b = (unsigned int)(255 - 255*fabs(org_p.curvature*3.0));

}
		//if(org_p.curvature < 0.0015) ///////////////////////////////////CURVATURE FILTER ///////////////////////////////
		//if(org_p.curvature < filter) ///////////////////////////////////CURVATURE FILTER ///////////////////////////////
		rsl.points.push_back(p);
	}
}

void cnv(pcl::PointCloud<pcl::PointXYZ> org, pcl::PointCloud<pcl::PointXYZRGB> &rsl)
{
	pcl::PointXYZRGB p;
	for(size_t i=0;i<org.points.size();i++){	
		p.x = org.points[i].x;
		p.y = org.points[i].y;
		p.z = org.points[i].z;
		rsl.points.push_back(p);
	}
}

float cross_product ( vector<float> v1, vector<float>v2 )
{
	return 	asin(sqrt(	 (v1[1]*v2[2]-v1[2]*v2[1])*(v1[1]*v2[2]-v1[2]*v2[1])
			+(v1[2]*v2[0]-v1[0]*v2[2])*(v1[2]*v2[0]-v1[0]*v2[2])
			+(v1[0]*v2[1]-v1[1]*v2[0])*(v1[0]*v2[1]-v1[1]*v2[0])
		));
}

float dot_product ( vector<float> v1, vector<float>v2 )
{
	return v1[1]*v2[1] + v1[2]*v2[2];
}

pcl::PointXYZRGB n_dist(pcl::PointNormal p)
{
	pcl::PointXYZRGB rsl;
	rsl.r = (unsigned int)255*fabs(p.normal_x);
	rsl.g = (unsigned int)255*fabs(p.normal_y);
	rsl.b = (unsigned int)255*fabs(p.normal_z);
	
	float norm = sqrt(p.normal_x*p.normal_x + p.normal_y*p.normal_y + p.normal_z*p.normal_z);
//	cout << fabs(p.normal_x*p.x + p.normal_y*p.y + p.normal_z*p.z) << endl;
	rsl.x = -p.normal_x * fabs(p.normal_x*p.x + p.normal_y*p.y + p.normal_z*p.z);
	rsl.y = -p.normal_y * fabs(p.normal_x*p.x + p.normal_y*p.y + p.normal_z*p.z);
	rsl.z = -p.normal_z * fabs(p.normal_x*p.x + p.normal_y*p.y + p.normal_z*p.z);
	
	return rsl;
}

int main (int argc, char** argv)
{
//	filter	0 
//	num		1
//	mode	2
//	
//	file name = "cloud_20.pcd"
//	としてひとまず処理を書く
//
//	TODO
//	pcdファイルを読み込み、座標変換してpointcloud2で受け取りrvizで表示
//	
//	
//	
//	
//	UNDONE
//
//	HACK
//


	ros::init(argc, argv, "pcd_loader");
  	ros::NodeHandle n;
	ros::Rate roop(1);
	
	filter = 0;
	
	string str[MAX_NUM];
	unsigned int num = 1;;

	str[0] = "cloud_20.pcd"; //ここを動的に変える必要あり
	mode = 2;

	ros::Publisher pub[MAX_NUM];
	
	//sensor_msgs::PointCloud pc[N];
	sensor_msgs::PointCloud2 pc[MAX_NUM];
	for(int i=0;i<num;i++){
		char topic[100]="PointCloud_";
		char topic2[100]="normal";
		char tmp[10];
		sprintf(tmp,"%d",i+1);
		strcat(topic,tmp);
	 	//pub[i] = n.advertise<sensor_msgs::PointCloud>(topic, 1);
	 	pub[i] = n.advertise<sensor_msgs::PointCloud2>(topic2, 1);
	}

	pcl::PointCloud<pcl::PointXYZ> cloud[MAX_NUM];// (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointNormal> cloud_n[MAX_NUM];// (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZRGB> cloud_rgb[MAX_NUM];// (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZRGB> cloud_c[MAX_NUM];// (new pcl::PointCloud<pcl::PointXYZ>);

	unsigned short int type[MAX_NUM];
	

	for(unsigned int i=0; i<num; i++){
		type[i] = 0;
		try{
			pcl::io::loadPCDFile<pcl::PointNormal>(str[i], cloud_n[i]);
		}catch(std::exception e1){
			try{
				pcl::io::loadPCDFile<pcl::PointXYZRGB>(str[i], cloud_c[i]);
				type[i] = 2;
			}catch(std::exception e2){
				pcl::io::loadPCDFile<pcl::PointXYZ>(str[i], cloud[i]);
				type[i] = 1;
			}
		}
	}
/*****************************/
char henji='\0';
bool simple_flag = false;
cout << "simple viewer??[y/n]";
cin >> henji;
if(henji=='y') type[0] = 3;	
/*****************************/
	for(unsigned int i=0; i<num; i++){
		cout << "type = " << type[i] << endl;
		if(type[i] == 0)
			cnv(cloud_n[i], cloud_c[i]);
		else if(type[i] == 1)
			cnv(cloud[i], cloud_c[i]);
        else if(type[i] == 3)
            simple_flag = true;
		else {
			return 1;
		}
	}
/*
	pcl::io::savePCDFileBinary("ex_sphere.pcd", cloud_c[1]);
*/
	for(unsigned int i=0; i<num; i++){
		if(simple_flag) pcl::toROSMsg(cloud_n[i], pc[i]);
		else pcl::toROSMsg(cloud_c[i], pc[i]);
	}

	cout << "注意：うまく表示されないときは曲率フィルターチェック" << endl;
	while(ros::ok()){
		for(unsigned int i=0;i<num;i++){
			pc[i].header.frame_id = "map";
			pc[i].header.stamp = ros::Time::now();
			pub[i].publish(pc[i]);
		}
		roop.sleep();
	}
	return (0);
}
