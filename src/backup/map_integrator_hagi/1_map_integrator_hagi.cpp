//
//Author: Takashi.Hagiwara
//
//pkg       : xxx
//filename  : xxx.cpp
//author    : x.xxx
//created   : 20xx.xx.xx
//lastupdate: 20xx.xx.xx
/*
	20161012
	処理
		/AMSL_ros_pkg/Dgauss_sphere/Dgauss/■■■/●●●.txtにaft.csvのnodeの位置情報を読み取る
		clouds/clouds_●●.pcdを読み込み/map座標における見え方へ回転
		depth_gauss_sphereを重みをかけて作成
		Euclidean Clusterの処理をかけ主平面の座標を/AMSL_ros_pkg/Dgauss_sphere/Dgauss/■■■/●●●.txtに保存

/*

470~473	コメントアウト
480		コメントアウト
481		コメントアウト解除


*/
#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
//#include <sensor_msgs/Imu.h>

//nav_msgs
//#include <nav_msgs/Odometry.h>

//geometry_msgs

//user defined
//#include <ceres_msgs/AMU_data.h>
//#include<>


//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const size_t skip = 10;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}

CloudN shift(Eigen::Matrix4f m, CloudN cloud){
	CloudN rsl;
	//	cout << "mat=" << endl <<  m << endl << endl;
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p << cloud.points[i].x, cloud.points[i].y, cloud.points[i].z, 1.0;
		n << cloud.points[i].normal_x, cloud.points[i].normal_y, cloud.points[i].normal_z, 0.0;
		p = m*p;
		n = m*n;
		pcl::PointNormal pnt;
		pnt.x = p(0);
		pnt.y = p(1);
		pnt.z = p(2);
		pnt.normal_x = n(0);
		pnt.normal_y = n(1);
		pnt.normal_z = n(2);
		pnt.curvature = cloud.points[i].curvature;
		rsl.push_back(pnt);
	}
	return rsl;
}

void addCloud(const CloudN &cloud){
	for(size_t i=0;i<cloud.points.size();i++){
		whole_map.push_back(cloud.points[i]);
	}
}

void merge(const Nodes &nodes, const CloudNs &clouds){
	for(size_t i=0;i<nodes.size();i++){
		addCloud( shift(nodes[i],clouds[i]) );
	}
}

Eigen::Matrix4f trans(float dat[7]){
	Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
	Eigen::Matrix3f r;
	/*
	   m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	   m(0,1) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	   m(0,2) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	   m(1,0) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	   m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	   m(1,2) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	   m(2,0) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	   m(2,1) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	   m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	 */////////////
	m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	m(1,0) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	m(2,0) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	m(0,1) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	m(2,1) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	m(0,2) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	m(1,2) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	////////////////
	m(0,3) = dat[0];
	m(1,3) = dat[1];
	m(2,3) = dat[2];

	return m;
	//return m.inverse();
}

void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal> pc, pcl::PointCloud<pcl::PointXYZINormal> *pc_output,pcl::PointCloud<pcl::PointXYZINormal> *pc_output2){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out2;

	for(size_t i=0;i<pc.points.size();i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;

		//for localization
		if(fabs(p.normal_z) > 0.9) continue;
		if(pc.points[i].z < 0.2) continue;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;
		float c = color(xyz);

		p.intensity = c;
		pc_out.points.push_back(p);

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = c;

		float range = fabs(pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		//天井デバッグ
/*		if(c>75 && c<85 && sqrt(pp.x*pp.x+pp.y*pp.y+pp.z*pp.z)>5){
			cout<<pc.points[i].x<<" "<<pc.points[i].y<<" "<<pc.points[i].z<<endl;
			pc_out2.points.push_back(pp);
		}
*/
		//藤野さんの最適化プログラム
		int r = int(sqrt(pow(pp.x,2)+pow(pp.y,2)+pow(pp.z,2)));
		for(int j=0;j<r;j++) pc_out2.points.push_back(pp);
	}
	
	*pc_output = pc_out;
	*pc_output2 = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
}

void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}

void clustering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output, bool dgs){
	pc_output->points.clear();
	pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	for(size_t i=0;i<pc_in.points.size();i+=skip){
		pcl::PointXYZ p;
		p.x = pc_in.points[i].x;
		p.y = pc_in.points[i].y;
		p.z = pc_in.points[i].z;
		input_cloud->points.push_back(p);
	}
	pc_in.points.clear();

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (input_cloud);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

	if(!dgs){
		ec.setClusterTolerance (0.01); 
		ec.setMinClusterSize (50);
		ec.setMaxClusterSize (1000);
	}else{
		ec.setClusterTolerance (0.15); 
		ec.setMinClusterSize (150);//150->50
		ec.setMaxClusterSize (1000);
	}	
	ec.setSearchMethod (tree);
	ec.setInputCloud (input_cloud);
	ec.extract (cluster_indices);

	std::vector<pcl::PointIndices>::const_iterator it;
	std::vector<int>::const_iterator pit;
	
	pcl::PointCloud<pcl::PointXYZINormal>pc_out;
	int cluster_num = 0;
	for(it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

		cluster_num++;
		int pc_num = 0; 
		float cx = 0.0;
		float cy = 0.0;
		float cz = 0.0;

		for(pit = it->indices.begin(); pit != it->indices.end(); pit++) {
			cloud_cluster->points.push_back(input_cloud->points[*pit]); 
			cx += input_cloud->points[*pit].x;
			cy += input_cloud->points[*pit].y;
			cz *= input_cloud->points[*pit].z;
			pc_num++;
		}
		
		pcl::PointXYZINormal p2;
		p2.x = cx/pc_num;
		p2.y = cy/pc_num;
		p2.z = cz/pc_num;
		p2.curvature = pc_num;	//point cloud density
		float r = sqrt(pow(p2.x,2)+pow(p2.y,2)+pow(p2.z,2));
		
		float xyz[3] = {0.0};
		xyz[0] = p2.x;
		xyz[1] = p2.y;
		xyz[2] = p2.z;
		p2.intensity = color(xyz);
		
		if(!dgs) pc_out.points.push_back(p2);
		else if(r>1.0) pc_out.points.push_back(p2);
		else{
		}
	}

	if(pc_out.points.size()==0){
		pc_out.points.resize(1);
		pc_out.points[0].x = 1000.0;
		pc_out.points[0].y = 1000.0;
		pc_out.points[0].z = 1000.0;
	}
	*pc_output = pc_out;
	//cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	//for(size_t i=0;i<pc_out.points.size();i++){
	//	cout<<pc_out.points[i].x<<endl;
	//	cout<<pc_out.points[i].y<<endl;
	//}
}

void process(){
	bool pc_flag = true;
	pcl::PointCloud<pcl::PointXYZINormal> pc_in;
	pc_in = pc_tmp;
	
	//cout<<pc_in<<endl;
	
	pc_callback_flag = true;
	
	if(pc_callback_flag && pc_in.points.size()>0){
		//pc_callback_flag = false;
		//pc_in = pc_input;
		//pc_input.points.clear();
		pc_flag = true;
	}else{
		cout<<"subscribe no points."<<endl;
	}

	//pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
	if(pc_flag){
		make_gauss_sphere(pc_in, &pc_gs, &pc_dgs);
		pc_in.points.clear();
		clustering(pc_gs, &pc_gsc, false);
		clustering(pc_dgs, &pc_dgsc, true);
		//cout<<"!!!!!!!!pc_dgsc!!!!!!!!!!!"<<endl;
		//for(size_t i=0;i<pc_dgsc.points.size();i++){
		//	cout<<pc_dgsc.points[i].x<<endl;
		//	cout<<pc_dgsc.points[i].y<<endl;
		//}
	}else{
	
		//cout<<"aaaaa"<<endl;
	}
	
	//sensor_msgs::PointCloud2 ros_gs, ros_dgs, ros_gsc, ros_dgsc;
	//sensor_msgs::PointCloud2 ros_dgsc;
	
	
	//pcl2ros(&pc_gs, &ros_gs);
	//pcl2ros(&pc_dgs, &ros_dgs);
	//pcl2ros(&pc_gsc, &ros_gsc);
	//pcl2ros(&pc_dgsc, &ros_dgsc);
	
	//cout<<"***************************"<<endl;
	//cout<<pc_tmp<<endl;
	
	//for(size_t i=0;i<pc_dgsc.points.size();i++){
	//	cout<<pc_dgsc.points[i].x<<endl;
	//	cout<<pc_dgsc.points[i].y<<endl;
	//}
	//pub_gs.publish(ros_gs);
	//pub_dgs.publish(ros_dgs);
	//pub_gsc.publish(ros_gsc);
	//pub_dgsc.publish(ros_dgsc);

}

int main (int argc, char** argv)
{
	cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	cout<<"model gauss保存先のフォルダが空であることを確認すること"<<endl;
	
	
	cout << "output_path = " << OUTPUT_PATH << endl;
	size_t max_index = 5000;
	cout << "max index = ";
	//$$$cin >> max_index ;
	cout << "skip = ";
	size_t skip = 0;
	cin >> skip;

	ros::init(argc, argv, "MAP_INTEGRATOR"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	//	ros::Publisher pub = n.advertise<sensor_msgs::PointCloud>(topic,1);
	//	ros::Subscriber sub = n.subscribe("/sub_topic",1,callback);
	//	sensor_msgs::PointCloud pc;
	//	pc.header.frame_id = frame;


	/////graphファイルの読み込み
	FILE *fp;
	//fp = fopen("for_model.csv","r");
	fp = fopen("aft.csv","r");
	//fp = fopen("bfr.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	///////////////////////////////////////////////
	FILE *fp2;
	char filename_2[100];
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_all.txt");//ファイルの作成
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/tsukuba_all.txt");//ファイルの作成
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/D_kan.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	

	
	
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );//nodeのx,y座標をtsukuba_all.txtに書き込む

			
			dat[0] = 0.0;
			dat[1] = 0.0;
			dat[2] = 0.0;
			
			//
			//for tsukuba
			//dat[3] = 0.0;
			//dat[4] = 0.0;
			//dat[5] = 0.0;
			//dat[6] = 0.0;
		
			Eigen::Matrix4f node;
			node = trans(dat);

			cout << "num = " << num << endl;
			char filename[100];
			//sprintf(filename,"clouds_cnvrt/cloud_%d.pcd",num);
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			
			pcl::PointCloud<pcl::PointNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointNormal>);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud) == -1){
				cout << "cloud_" << num << "not found." << endl;
				break;
			}
            
            ////////////////////////////////////////////////////////
            char outfile[100];
			//addCloud( shift(node, *cloud) );
	        pcl::PointCloud<pcl::PointNormal> cloud_fnl;
            cloud_fnl = shift(node, *cloud);
            
            pc_tmp.points.resize( cloud_fnl.points.size() );
            for(size_t i=0;i<cloud_fnl.points.size();i++){
            	pc_tmp.points[i].x = cloud_fnl.points[i].x;
            	pc_tmp.points[i].y = cloud_fnl.points[i].y;
            	pc_tmp.points[i].z = cloud_fnl.points[i].z;
            	
            	pc_tmp.points[i].normal_x = cloud_fnl.points[i].normal_x;
            	pc_tmp.points[i].normal_y = cloud_fnl.points[i].normal_y;
            	pc_tmp.points[i].normal_z = cloud_fnl.points[i].normal_z;
            	
            	//for TSUKUBA MAP
            	//pc_tmp.points[i].normal_x = -1.0* cloud_fnl.points[i].normal_x;
            	//pc_tmp.points[i].normal_y = -1.0* cloud_fnl.points[i].normal_y;
            	//pc_tmp.points[i].normal_z = -1.0* cloud_fnl.points[i].normal_z;
            }
            
            ////////////////////////////////////////////////////////
            //回転後のpcdが cloud_fnlに入っている <<dat=0にしているのでvelodyne原点で取得した点群をそのまま利用している
            //1スキャン分の点群(normalつき)をprincipal_componentsの処理にかけ、クラスタされたものを保存していく
            process();
            
            for(size_t i=0;i<pc_dgsc.points.size();i++){
				cout<<pc_dgsc.points[i].x<<endl;
				cout<<pc_dgsc.points[i].y<<endl;
			}
            FILE *fp1;
            char filename_1[100];
            //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
            //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/tsukuba_all/%d.txt",num);//ファイルの作成
            sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/Dkan_all/%d.txt",num);//ファイルの作成
            //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_all/%d.txt",num);//ファイルの作成 for TSUKUBA
            
		        
		     
		    if((fp1 = fopen(filename_1, "a+")) == NULL){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			
			
		    // 処理（ここでは、下記文字をファイルに書き込む）
			for(size_t i=0;i<pc_dgsc.points.size();i++){
				fprintf(fp1, "%f,%f,%f\n", pc_dgsc.points[i].x,pc_dgsc.points[i].y,pc_dgsc.points[i].z);
			}
			// ファイルを閉じる 
			fclose(fp1);
			
            
            
            ///////////////////
            //pc_dgsc の初期化
            pc_dgsc.points.clear();
            
            
            ////////////////////////////////////////////////////////
            //
			//for tsukuba
            //sprintf(outfile, "clouds_cnvrt_fnl/%d.pcd", num);
            
            
            
            
            sprintf(outfile, "clouds_cnvrt/%d.pcd", num);
            pcl::io::savePCDFileBinary(outfile, cloud_fnl);
			/////////////////////////////////////////////////////////
            
            cout << cnt << " / " << max_index << endl << endl;
			if(cnt == max_index)break;
			cnt += skip;
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);
	
	
	//$$$pcl::io::savePCDFileBinary(OUTPUT_PATH, whole_map);
	cout << "num = " << cnt << endl;
	return 0;
}

