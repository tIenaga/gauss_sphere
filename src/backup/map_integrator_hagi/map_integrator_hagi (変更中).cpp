//This template is made for .cpp
//Author: s.shimizu
//
//pkg       : xxx
//filename  : xxx.cpp
//author    : x.xxx
//created   : 20xx.xx.xx
//lastupdate: 20xx.xx.xx


#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <clustering_.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
//#include <sensor_msgs/Imu.h>

//nav_msgs
//#include <nav_msgs/Odometry.h>

//geometry_msgs

//user defined
//#include <ceres_msgs/AMU_data.h>
//#include<>


//namespaces
const float THRESH_NUM=0.0;
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

typedef pcl::PointXYZINormal PointA;
typedef pcl::PointCloud<PointA>  CloudA;
typedef pcl::PointCloud<PointA>::Ptr  CloudAPtr;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const size_t skip = 10;
const string OUTPUT_PATH = "map/map_hagi.pcd";
const int color_num = 360;
const int color_res = 10;
//const string OUTPUT_PATH = "refined/map_0.pcd";
const float efficient = 0.05; // = 5%
int min_n = 0;

CloudN whole_map;

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

CloudAPtr ptrTransform(CloudA pcl_in){
	CloudAPtr output(new CloudA);

	// output->width=1;
	// output->height=pcl_in.points.size();
	output->points.resize(pcl_in.points.size());
	for(size_t i=0;i<pcl_in.points.size();i++)
	{
		output->points[i].x=pcl_in.points[i].x;
		output->points[i].y=pcl_in.points[i].y;
		output->points[i].z=pcl_in.points[i].z;
		output->points[i].intensity=pcl_in.points[i].intensity;
		output->points[i].normal_x=pcl_in.points[i].normal_x;
		output->points[i].normal_y=pcl_in.points[i].normal_y;
		output->points[i].normal_z=pcl_in.points[i].normal_z;
		output->points[i].curvature=pcl_in.points[i].curvature;
	}
	return output;
}

void MAX(int in, int *max){
	if(in > *max) *max =in;
	else{
	}
}
float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

void filtering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output){

	pcl::PointCloud<pcl::PointXYZINormal> pc, pc_out1;
	pc = pc_in;

	int color_cluster_num = int(color_num/color_res);
	hist color[color_cluster_num];
	//init
	for(int i=0;i<color_cluster_num;i++){
		color[i].num = 0;
		color[i].normal_num = 0.0;
		color[i].x = 0.0;
		color[i].y = 0.0;
		color[i].z = 0.0;
		color[i].nx = 0.0;
		color[i].ny = 0.0;
		color[i].nz = 0.0;
		color[i].curv = 0.0;
		color[i].inten = 0.0;
	}

	//make_color_hist
	for(size_t i=0;i<pc.points.size();i++){
		int c_num = int(pc.points[i].intensity/color_res);
		color[c_num].num++;
		color[c_num].x += pc.points[i].x;
		color[c_num].y += pc.points[i].y;
		color[c_num].z += pc.points[i].z;
		color[c_num].nx += pc.points[i].normal_x;
		color[c_num].ny += pc.points[i].normal_y;
		color[c_num].nz += pc.points[i].normal_z;
		color[c_num].curv += pc.points[i].curvature;
		color[c_num].inten += pc.points[i].intensity;
	}

	int max = 0;
	for(int i=0;i<color_cluster_num;i++) MAX(color[i].num, &max);
	for(int i=0;i<color_cluster_num;i++){
		color[i].normal_num = color[i].num/float(max);
		color[i].x /= color[i].num;
		color[i].y /= color[i].num;
		color[i].z /= color[i].num;
		color[i].nx /= color[i].num;
		color[i].ny /= color[i].num;
		color[i].nz /= color[i].num;
		color[i].curv /= color[i].num;
		color[i].inten /= color[i].num;
		if(color[i].normal_num > efficient){
			pcl::PointXYZINormal p;
			p.x = color[i].x;
			p.y = color[i].y;
			p.z = color[i].z;
			p.normal_x = color[i].nx;
			p.normal_y = color[i].ny;
			p.normal_z = color[i].nz;
			p.curvature = color[i].curv;
			p.intensity = color[i].inten;
			pc_out1.points.push_back(p);
		}else{
		}
	}

	*pc_output = pc_out1;
}


/*
float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}
*/
CloudN shift(Eigen::Matrix4f m, CloudN cloud){
	CloudN rsl;
	//	cout << "mat=" << endl <<  m << endl << endl;
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p << cloud.points[i].x, cloud.points[i].y, cloud.points[i].z, 1.0;
		n << cloud.points[i].normal_x, cloud.points[i].normal_y, cloud.points[i].normal_z, 0.0;
		p = m*p;
		n = m*n;
		pcl::PointNormal pnt;
		pnt.x = p(0);
		pnt.y = p(1);
		pnt.z = p(2);
		pnt.normal_x = n(0);
		pnt.normal_y = n(1);
		pnt.normal_z = n(2);
		pnt.curvature = cloud.points[i].curvature;
		rsl.push_back(pnt);
	}
	return rsl;
}

void addCloud(const CloudN &cloud){
	for(size_t i=0;i<cloud.points.size();i++){
		whole_map.push_back(cloud.points[i]);
	}
}

void merge(const Nodes &nodes, const CloudNs &clouds){
	for(size_t i=0;i<nodes.size();i++){
		addCloud( shift(nodes[i],clouds[i]) );
	}
}

Eigen::Matrix4f trans(float dat[7]){
	Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
	Eigen::Matrix3f r;
	/*
	   m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	   m(0,1) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	   m(0,2) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	   m(1,0) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	   m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	   m(1,2) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	   m(2,0) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	   m(2,1) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	   m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	 */////////////
	m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	m(1,0) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	m(2,0) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	m(0,1) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	m(2,1) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	m(0,2) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	m(1,2) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	////////////////
	m(0,3) = dat[0];
	m(1,3) = dat[1];
	m(2,3) = dat[2];

	return m;
	//return m.inverse();
}
void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal>& pc, 
pcl::PointCloud<pcl::PointXYZINormal> &pc_output, pcl::PointCloud<pcl::PointXYZINormal> &pc_output_rm, 
pcl::PointCloud<pcl::PointXYZINormal> &pc_output2, pcl::PointCloud<pcl::PointXYZINormal> &pc_output2_rm){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out_rm, pc_out2, pc_out2_rm;
	pcl::PointCloud<pcl::PointXYZINormal> pc_out_c;

	pc_output.points.resize(pc.points.size());
	// pc_output2.points.resize(pc.points.size());
	for(size_t i=0;i<pc.points.size();i++){
		pc_output.points[i].x = gauss_sphere_range * pc.points[i].normal_x;
		pc_output.points[i].y = gauss_sphere_range * pc.points[i].normal_y;
		pc_output.points[i].z = gauss_sphere_range * pc.points[i].normal_z;
		pc_output.points[i].normal_x = pc.points[i].normal_x;
		pc_output.points[i].normal_y = pc.points[i].normal_y;
		pc_output.points[i].normal_z = pc.points[i].normal_z;
		pc_output.points[i].curvature = pc.points[i].curvature;

		float color = 0.0;

		//find max and min of normal element
		float xyz[3] = {0.0};
		xyz[0] = pc_output.points[i].x;
		xyz[1] = pc_output.points[i].y;
		xyz[2] = pc_output.points[i].z;
		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
		}

		if((fabs(pc_output.points[i].x)>fabs(pc_output.points[i].y))
					&& (fabs(pc_output.points[i].x)>fabs(pc_output.points[i].z))){
			if(pc_output.points[i].x<0)
				color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min)+180.0;
			else color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min);
		}else if((fabs(pc_output.points[i].y)>fabs(pc_output.points[i].z))
					&& (fabs(pc_output.points[i].y)>fabs(pc_output.points[i].x))){
			if(pc_output.points[i].y<0)
				color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0;
		}else if((fabs(pc_output.points[i].z)>fabs(pc_output.points[i].x))
					&& (fabs(pc_output.points[i].z)>fabs(pc_output.points[i].y))){
			if(pc_output.points[i].z<0)
				color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0;
		}else{
		}
		// limit
		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		pc_output.points[i].intensity = color;

		// float range = (pc_output2.points[i].normal_x*pc_output2.points[i].x
						// +pc_output2.points[i].normal_y*pc_output2.points[i].y
						// +pc_output2.points[i].normal_z*pc_output2.points[i].z);
		float range = fabs(-pc.points[i].normal_x*pc.points[i].x
						-pc.points[i].normal_y*pc.points[i].y
						-pc.points[i].normal_z*pc.points[i].z);
		// float tmp_range = fabs(pc.points[i].x*pc.points[i].x
						// +pc.points[i].y*pc.points[i].y
						// +pc.points[i].z*pc.points[i].z);
		// if (tmp_range<range) cout<<"over"<<endl;
		if (fabs(pc.points[i].normal_z)<0.9 && pc.points[i].z>0.2){//0.7
			PointA tmp;
			tmp.x = range * pc_output.points[i].normal_x;
			tmp.y = range * pc_output.points[i].normal_y;
			tmp.z = range * pc_output.points[i].normal_z;
			tmp.normal_x=pc.points[i].normal_x;
			tmp.normal_y=pc.points[i].normal_y;
			tmp.normal_z=pc.points[i].normal_z;
			tmp.intensity = color;
			tmp.intensity = color;
			pc_output2.points.push_back(tmp);
			// pc_output2.points[i].x = range * pc_output.points[i].normal_x;
			// pc_output2.points[i].y = range * pc_output.points[i].normal_y;
			// pc_output2.points[i].z = range * pc_output.points[i].normal_z;
			// pc_output2.points[i].normal_x=pc.points[i].normal_x;
			// pc_output2.points[i].normal_y=pc.points[i].normal_y;
			// pc_output2.points[i].normal_z=pc.points[i].normal_z;
			// pc_output2.points[i].intensity = color;
			// pc_output2.points[i].intensity = color;
		}
		
	}
	
  	// Create the filtering object
	CloudAPtr pc_output2_ptr;
	CloudAPtr pc_output2_fil_ptr(new CloudA);
	pc_output2_ptr=ptrTransform(pc_output2);
  	pcl::StatisticalOutlierRemoval<pcl::PointXYZINormal> sor;
  	sor.setInputCloud (pc_output2_ptr);
  	sor.setMeanK (20);
  	sor.setStddevMulThresh (0.1);
	sor.filter (*pc_output2_fil_ptr);
	pc_output2.points.resize(pc_output2_fil_ptr->points.size());
	for (size_t i=0; i<pc_output2_fil_ptr->points.size(); ++i){
		pc_output2.points[i].x=pc_output2_fil_ptr->points[i].x;
		pc_output2.points[i].y=pc_output2_fil_ptr->points[i].y;
		pc_output2.points[i].z=pc_output2_fil_ptr->points[i].z;
		pc_output2.points[i].normal_x=pc_output2_fil_ptr->points[i].normal_x;
		pc_output2.points[i].normal_y=pc_output2_fil_ptr->points[i].normal_y;
		pc_output2.points[i].normal_z=pc_output2_fil_ptr->points[i].normal_z;
	}
	// CloudAPtr pc_output2_ptr;
	// pc_output2_ptr=ptrTransform(pc_output2);

	// pcl::search::KdTree<PointA>::Ptr tree (new pcl::search::KdTree<PointA>);
	// tree->setInputCloud (pc_output2_ptr);
	// std::vector<pcl::PointIndices> cluster_indices;
	// pcl::EuclideanClusterExtraction<PointA> ec;
	// ec.setClusterTolerance (0.3); 
	// ec.setMinClusterSize (300);
	// ec.setMaxClusterSize (10000);
	// ec.setSearchMethod (tree);
	// ec.setInputCloud(pc_output2_ptr);
	// ec.extract (cluster_indices);
  
    // //get cluster information//
	// size_t num=0;
	// pc_output2.points.clear();
	// for(size_t iii=0;iii<cluster_indices.size();iii++){
		// //cluster points
		// pc_output2.points.resize(cluster_indices[iii].indices.size()+num);
		// for(size_t jjj=0;jjj<cluster_indices[iii].indices.size();jjj++){
			// int p_num=cluster_indices[iii].indices[jjj];
			// pc_output2.points[num+jjj]=pc_output2_ptr->points[p_num];
		// }
		// num=pc_output2.points.size();//save previous size
	// }

	//filtering(pc_out, &pc_out_c);
	// pcl::PointCloud<pcl::PointXYZINormal> debug_pcl;
	// for (size_t i=0; i<pc_output2.points.size(); ++i){
		// if (pc_output2[i].z>4.0){
			// debug_pcl.points.push_back(pc.points[i]);
			// cout<<pc.points[i].x<<","<<pc.points[i].y<<","<<pc.points[i].z<<endl;
			// cout<<"ang_xy:"<<atan2(pc.points[i].normal_y,pc.points[i].normal_x)*180.0/M_PI<<endl;
			// cout<<"ang_yz:"<<atan2(pc.points[i].normal_z,pc.points[i].normal_y)*180.0/M_PI<<endl;
			// cout<<"ang_zx:"<<atan2(pc.points[i].normal_x,pc.points[i].normal_z)*180.0/M_PI<<endl<<endl;
		// }
		// else {
			// pc_output2[i].x=0;
			// pc_output2[i].y=0;
			// pc_output2[i].z=0;
		// }
	// }
	// cout<<"----------------------------------"<<endl;
	// sensor_msgs::PointCloud2 debug_ros;
	// pcl::toROSMsg(debug_pcl,debug_ros);
	// debug_ros.header.frame_id="velodyne";
	// debug_ros.header.stamp=ros::Time::now();
	// pub_debug.publish(debug_ros);

	// pc_output = pc_out;
	//*pc_output_rm = pc_out_rm;
	pc_output_rm = pc_out_c;
	// pc_output2 = pc_out2;
	pc_output2_rm = pc_out2_rm;
	pc_out.points.clear();
	pc_out_rm.points.clear();
	pc_out2.points.clear();
	pc_out2_rm.points.clear();

}

void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}
/*
void clustering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output, bool dgs){
	pc_output->points.clear();
	pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	for(size_t i=0;i<pc_in.points.size();i+=skip){
		pcl::PointXYZ p;
		p.x = pc_in.points[i].x;
		p.y = pc_in.points[i].y;
		p.z = pc_in.points[i].z;
		input_cloud->points.push_back(p);
	}
	pc_in.points.clear();

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (input_cloud);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

	if(!dgs){
		ec.setClusterTolerance (0.01); 
		ec.setMinClusterSize (50);
		ec.setMaxClusterSize (1000);
	}else{
		ec.setClusterTolerance (0.15); 
		ec.setMinClusterSize (150);
		ec.setMaxClusterSize (1000);
	}	
	ec.setSearchMethod (tree);
	ec.setInputCloud (input_cloud);
	ec.extract (cluster_indices);

	std::vector<pcl::PointIndices>::const_iterator it;
	std::vector<int>::const_iterator pit;
	
	pcl::PointCloud<pcl::PointXYZINormal>pc_out;
	int cluster_num = 0;
	for(it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

		cluster_num++;
		int pc_num = 0; 
		float cx = 0.0;
		float cy = 0.0;
		float cz = 0.0;

		for(pit = it->indices.begin(); pit != it->indices.end(); pit++) {
			cloud_cluster->points.push_back(input_cloud->points[*pit]); 
			cx += input_cloud->points[*pit].x;
			cy += input_cloud->points[*pit].y;
			cz *= input_cloud->points[*pit].z;
			pc_num++;
		}
		
		pcl::PointXYZINormal p2;
		p2.x = cx/pc_num;
		p2.y = cy/pc_num;
		p2.z = cz/pc_num;
		p2.curvature = pc_num;	//point cloud density
		float r = sqrt(pow(p2.x,2)+pow(p2.y,2)+pow(p2.z,2));
		
		//float xyz[3] = {0.0};
		//xyz[0] = p2.x;
		//xyz[1] = p2.y;
		//xyz[2] = p2.z;
		//p2.intensity = color(xyz);
		
		if(!dgs) pc_out.points.push_back(p2);
		else if(r>1.0) pc_out.points.push_back(p2);
		else{
		}
	}

	if(pc_out.points.size()==0){
		pc_out.points.resize(1);
		pc_out.points[0].x = 1000.0;
		pc_out.points[0].y = 1000.0;
		pc_out.points[0].z = 1000.0;
	}
	*pc_output = pc_out;
	//cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	//for(size_t i=0;i<pc_out.points.size();i++){
	//	cout<<pc_out.points[i].x<<endl;
	//	cout<<pc_out.points[i].y<<endl;
	//}
}
*/
int main (int argc, char** argv)
{
	cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	cout<<"model gauss保存先のフォルダが空であることを確認すること"<<endl;
	

	cout << "output_path = " << OUTPUT_PATH << endl;
	size_t max_index = 5000;
	cout << "max index = ";
	//$$$cin >> max_index ;
	cout << "skip = ";
	size_t skip = 0;
	cin >> skip;

	ros::init(argc, argv, "MAP_INTEGRATOR"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	//	ros::Publisher pub = n.advertise<sensor_msgs::PointCloud>(topic,1);
	//	ros::Subscriber sub = n.subscribe("/sub_topic",1,callback);
	//	sensor_msgs::PointCloud pc;
	//	pc.header.frame_id = frame;


	/////graphファイルの読み込み
	FILE *fp;
	//fp = fopen("for_model.csv","r");
	fp = fopen("aft.csv","r");
	//fp = fopen("bfr.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	///////////////////////////////////////////////
	FILE *fp2;
	char filename_2[100];
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	
	
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );

			
			dat[0] = 0.0;
			dat[1] = 0.0;
			dat[2] = 0.0;
			
			Eigen::Matrix4f node;
			node = trans(dat);

			cout << "num = " << num << endl;
			char filename[100];
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			pcl::PointCloud<pcl::PointNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointNormal>);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud) == -1){
				cout << "cloud_" << num << "not found." << endl;
				break;
			}
            
            ////////////////////////////////////////////////////////
            char outfile[100];
			//addCloud( shift(node, *cloud) );
	        pcl::PointCloud<pcl::PointNormal> cloud_fnl;
            cloud_fnl = shift(node, *cloud);
            
            pc_tmp.points.resize( cloud_fnl.points.size() );
            for(size_t i=0;i<cloud_fnl.points.size();i++){
            	pc_tmp.points[i].x = cloud_fnl.points[i].x;
            	//cout<<pc_tmp.points[i].x<<endl;
            	pc_tmp.points[i].y = cloud_fnl.points[i].y;
            	pc_tmp.points[i].z = cloud_fnl.points[i].z;
            	pc_tmp.points[i].normal_x = cloud_fnl.points[i].normal_x;
            	pc_tmp.points[i].normal_y = cloud_fnl.points[i].normal_y;
            	pc_tmp.points[i].normal_z = cloud_fnl.points[i].normal_z;
            }
            
            ////////////////////////////////////////////////////////
            //回転後のpcdが cloud_fnlに入っている
            //
            //	pc_tmp.points を subscribeした点としてまずはgauss球の処理を行う ros_gauss_sphere_depth_f がpublishされている
            //	そのgauss球に対してprincipal componentsを取り出す．
            //	pc -> pc2 ->
            //	
            pcl::PointCloud<pcl::PointXYZINormal> pc_gauss_sphere, pc_gauss_sphere_depth, pc_gauss_sphere_f, pc_gauss_sphere_depth_f;
            make_gauss_sphere(pc_tmp, pc_gauss_sphere, pc_gauss_sphere_f, pc_gauss_sphere_depth, pc_gauss_sphere_depth_f);
            
            //cout<<"pc_gauss_sphere_depth : "<<pc_gauss_sphere_depth<<endl;
            
            sensor_msgs::PointCloud2 ros_gauss_sphere, ros_gauss_sphere_f, ros_gauss_sphere_depth, ros_gauss_sphere_depth_f;
            pcl::toROSMsg(pc_gauss_sphere_depth, ros_gauss_sphere_depth);
            //sensor_msgs::PointCloud2ConstPtr msg = *ros_gauss_sphere_depth_f;
            sensor_msgs::PointCloud2 pc2;
            Clustering clustering;
			clustering.setMinMaxNum(min_n,INT_MAX);
			//clustering.setThreshold(0.6, 0.1);	//angle and distance
			clustering.setThreshold(0.80, 0.1);	//angle and distance
			//clustering.setThreshold(0.80, 0.05);	//angle and distance
			clustering.putTank(ros_gauss_sphere_depth);
			clustering.process();
			clustering.calcWeight();
			clustering.showClusterSize();
			//clustering.showClusters();
			clustering.getClusters(pc2);
			
			pcl::PointCloud<pcl::PointXYZINormal> tmp_clusters;
			pcl::PointCloud<pcl::PointXYZINormal> dom_clusters;//dom=dominant
			pcl::fromROSMsg(pc2, tmp_clusters);
			size_t num_=tmp_clusters.size();
			if (num_>0){
				//get the number of member and weight
				double total_mnum=0;
				for (size_t i=0; i<num_; i++)
					total_mnum+=(double)clustering.getNMember(i);
				double norm_num, weight;
				for (size_t i=0; i<num_; ++i){
					norm_num=(double)clustering.getNMember(i)/total_mnum;
					// weight=clustering.getWeight(i);
					// cout<<weight<<endl;
					// if (norm_num>THRESH_NUM && weight>0.02){
					if (norm_num>THRESH_NUM){
						tmp_clusters.points[i].curvature=norm_num;
						tmp_clusters.points[i].intensity=weight;
						dom_clusters.push_back(tmp_clusters.points[i]);
					}
				}
				if (dom_clusters.points.size()==0){
					dom_clusters.points.resize(1);
					dom_clusters.width=1;
					dom_clusters.height=1;
					dom_clusters.points[0].x=1000;
					dom_clusters.points[0].y=1000;
					dom_clusters.points[0].z=1000;
					dom_clusters.points[0].curvature=0;
				}
			}
			else{ //to delete the remained points
				dom_clusters.points.resize(1);
				dom_clusters.width=1;
				dom_clusters.height=1;
				dom_clusters.points[0].x=1000;
				dom_clusters.points[0].y=1000;
				dom_clusters.points[0].z=1000;
				dom_clusters.points[0].curvature=0;
			}
			sensor_msgs::PointCloud2 pc2_w;
			//pcl::toROSMsg(dom_clusters, pc2_w);
            
            //process();
            //*pc_dgsc = *pc_output;
            
            //pcl::fromROSMsg(pc2_w, pc_dgsc);
			for(size_t i=0;i<dom_clusters.size();i++){
            //for(size_t i=0;i<pc_dgsc.points.size();i++){
				//cout<<pc_dgsc.points[i].x<<endl;
				//cout<<pc_dgsc.points[i].y<<endl;
			}
            FILE *fp1;
            char filename_1[100];
            //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
            sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",num);//ファイルの作成
            
		        
		     
		    if((fp1 = fopen(filename_1, "a+")) == NULL){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			
			
		    // 処理（ここでは、下記文字をファイルに書き込む）
			for(size_t i=0;i<pc_dgsc.points.size();i++){
				fprintf(fp1, "%f,%f,%f\n", dom_clusters.points[i].x,dom_clusters.points[i].y,dom_clusters.points[i].z);
			}
			// ファイルを閉じる 
			fclose(fp1);
			
            
            
            ///////////////////
            //pc_dgsc の初期化
            pc_dgsc.points.clear();
            
            
            ////////////////////////////////////////////////////////
            sprintf(outfile, "clouds_cnvrt/%d.pcd", num);
            pcl::io::savePCDFileBinary(outfile, cloud_fnl);
			/////////////////////////////////////////////////////////
            
            cout << cnt << " / " << max_index << endl << endl;
			if(cnt == max_index)break;
			cnt += skip;
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);
	
	
	//$$$pcl::io::savePCDFileBinary(OUTPUT_PATH, whole_map);
	cout << "num = " << cnt << endl;
	return 0;
}

