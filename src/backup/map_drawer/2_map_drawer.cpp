//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : map_drawer.cpp
//author    : x.xxx
//created   : 2016.07.28
//lastupdate: 2016.07.28

/*
起動方法
aft.csvのある場所でrosrun

/map/map_0から
全体の三次元地図を読み取って
aft.csvにおける各node中心DISTmの地図を切り抜く

topic名(publish)
	/perfect_velodyne/normal
		（/gauss_sphere/drawed_map）
		
読み込むpcd file名
	map/map_0.pcd



*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const float DIST = 50.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}


void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

int main (int argc, char** argv)
{

	ros::init(argc, argv, "map_drawer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!map_drawer START!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	/////graphファイルの読み込み
	FILE *fp;
	fp = fopen("aft.csv","r");
	
/*
	
	aft.csvに書いてあるnode情報を参考に
	そのnodeの座標中心における地図を切り抜き保存していく	
	
*/	

	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	ros::Publisher pub_drawed_map;
	
	sensor_msgs::PointCloud2 pc;
	
	
 	pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
	
	///////////////////////////////////////////////

	char filename[100] = "map/map_0.pcd";
	
	pcl::PointCloud<pcl::PointNormal>::Ptr map_cloud (new pcl::PointCloud<pcl::PointNormal>);
	pcl::PointCloud<pcl::PointNormal>::Ptr drawed_cloud (new pcl::PointCloud<pcl::PointNormal>);
	if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *map_cloud) == -1){
		cout <<filename<<"not found." << endl;
		return 0;
	}
	
	cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
	cout<<"Visualize Start!"<<endl;
	//while(ros::ok() ){
///////////////////////////////////////////////////////////////////////////////////////
	
	int file_num = 1;
	
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
		
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
		
//if(num == 1245){//1245ノード、200.pcdの場所
//if(num == 1202){//1245ノード、200.pcdの場所
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			//fprintf(fp2, "%f,%f\n",dat[0],dat[1] );//nodeのx,y座標をtsukuba_all.txtに書き込む
			float x_ = dat[0];
			float y_ = dat[1];
			float z_ = dat[2];
			
			//
			//並列処理じゃないと遅すぎるので並列化しよう
			
			for(size_t i=0;i<map_cloud->points.size();i++){
				float dist = calcdist( x_, y_, map_cloud->points[i].x, map_cloud->points[i].y);
				printf("\ri = %d",i);
				if(dist <= DIST){
					
					//内積計算
					//
					Eigen::Vector3f point;
					point << (map_cloud->points[i].x - x_)
							,(map_cloud->points[i].y - y_)
							,(map_cloud->points[i].z - z_);
					Eigen::Vector3f normal;
					normal <<(map_cloud->points[i].normal_x)
							,(map_cloud->points[i].normal_y)
							,(map_cloud->points[i].normal_z);
					float cos_theta = point.dot(normal) / ( point.norm() * normal.norm() );
					
					//値が逆になっているのが謎だが、これで大丈夫そう
					//
					if( cos_theta >= 0.0 ){
						pcl::PointNormal pnt;
						pnt.x = map_cloud->points[i].x - x_;
						pnt.y = map_cloud->points[i].y - y_;
						pnt.z = map_cloud->points[i].z - z_;
						pnt.normal_x = map_cloud->points[i].normal_x;
						pnt.normal_y = map_cloud->points[i].normal_y;
						pnt.normal_z = map_cloud->points[i].normal_z;
						pnt.curvature = map_cloud->points[i].curvature;
						drawed_cloud->push_back(pnt);
					}
				}
			}
			pcl::toROSMsg(*drawed_cloud, pc);
			
			char outfile[100];
			sprintf(outfile, "drawed_map/%d.pcd", file_num);
            pcl::io::savePCDFileBinary(outfile, *drawed_cloud);
            cout<<"Finish saving drawed_cloud to : "<<outfile<<endl;
//}
			drawed_cloud->points.clear();
			file_num++;
		}
////////////////////////////////////////////////////////////////////		

		pc.header.frame_id = "velodyne";
		//pc.header.frame_id = "map";
	
		pc.header.stamp = ros::Time::now();
		pub_drawed_map.publish(pc);
		
	}
	
	cout<<"Finish map drawing."<<endl;
}

