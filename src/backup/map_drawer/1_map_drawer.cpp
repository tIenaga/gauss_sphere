//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : map_drawer.cpp
//author    : x.xxx
//created   : 2016.07.28
//lastupdate: 2016.07.28

/*
起動方法
aft.csvのある場所でrosrun

/map/map_0から
全体の三次元地図を読み取って原点中心DISTmの地図を切り抜く

topic名(publish)
	/perfect_velodyne/normal
		（/gauss_sphere/drawed_map）
		
読み込むpcd file名
	map/map_0.pcd



*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const float DIST = 10.0;
const size_t skip = 10;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}


void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

int main (int argc, char** argv)
{

	ros::init(argc, argv, "map_drawer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!map_drawer START!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	/////graphファイルの読み込み
	FILE *fp;
	fp = fopen("aft.csv","r");
	
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	ros::Publisher pub_drawed_map;
	
	sensor_msgs::PointCloud2 pc;
	
	
 	pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
	
	///////////////////////////////////////////////

	char filename[100] = "map/map_0.pcd";
	
	pcl::PointCloud<pcl::PointNormal>::Ptr map_cloud (new pcl::PointCloud<pcl::PointNormal>);
	pcl::PointCloud<pcl::PointNormal>::Ptr drawed_cloud (new pcl::PointCloud<pcl::PointNormal>);
	if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *map_cloud) == -1){
		cout <<filename<<"not found." << endl;
		return 0;
	}
	
	cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
	
	float x_ = 31.5;
	float y_ = -8.0;
	
	for(size_t i=0;i<map_cloud->points.size();i++){
		float dist = calcdist( x_, y_, map_cloud->points[i].x, map_cloud->points[i].y);
		printf("\ri = %d",i);
		if(dist <= DIST){
			pcl::PointNormal pnt;
			pnt.x = map_cloud->points[i].x - x_;
			pnt.y = map_cloud->points[i].y - y_;
			pnt.z = map_cloud->points[i].z;
			pnt.normal_x = map_cloud->points[i].normal_x;
			pnt.normal_y = map_cloud->points[i].normal_y;
			pnt.normal_z = map_cloud->points[i].normal_z;
			pnt.curvature = map_cloud->points[i].curvature;
			drawed_cloud->push_back(pnt);
		}
	}
	
	pcl::toROSMsg(*drawed_cloud, pc);
	
	cout<<"Visualize Start!"<<endl;
	while(ros::ok() ){
	
		pc.header.frame_id = "velodyne";
		//pc.header.frame_id = "map";
		
		pc.header.stamp = ros::Time::now();
		pub_drawed_map.publish(pc);
		roop.sleep();
	
	}
	return 0;
}

