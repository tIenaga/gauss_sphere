//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : map_drawer.cpp
//author    : x.xxx
//created   : 2016.07.28
//lastupdate: 2016.08.25

/*
起動方法
aft.csvのある場所でrosrun


//////////////////////////////////

aft.csvにおける各node前後1スキャンを含め3スキャン分を保存するsrc

topic名(publish)
	/perfect_velodyne/normal
		（/gauss_sphere/drawed_map）
		
読み込むpcd file名
	/clouds/cloud_●●.pcd



*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const float DIST = 50.0;
const float DIST_NODE = 5.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}


void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}

pcl::PointCloud<pcl::PointNormal> transformer(const pcl::PointCloud<pcl::PointNormal> &cloud_org, Eigen::Matrix4f m){
	pcl::PointCloud<pcl::PointNormal> cloud;
	cloud.points.resize(cloud_org.points.size());
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p(0) = cloud_org.points[i].x;
		p(1) = cloud_org.points[i].y;
		p(2) = cloud_org.points[i].z;
		p(3) = 1.0;
		n(0) = cloud_org.points[i].normal_x;
		n(1) = cloud_org.points[i].normal_y;
		n(2) = cloud_org.points[i].normal_z;
		n(3) = 0.0;
		p = m * p;
		n = m * n;
		cloud.points[i].x = p(0);
		cloud.points[i].y = p(1);
		cloud.points[i].z = p(2);
		cloud.points[i].normal_x = n(0);
		cloud.points[i].normal_y = n(1);
		cloud.points[i].normal_z = n(2);
		cloud.points[i].curvature = cloud_org.points[i].curvature;
	}
	return cloud;
}

Eigen::Matrix3f quat2mat(float x,float y,float z,float w){
	Eigen::Matrix3f rsl = Eigen::Matrix3f::Identity();
	rsl << 
		1-2*y*y-2*z*z,	2*x*y+2*w*z,	2*x*z-2*w*y,
		2*x*y-2*w*z,	1-2*x*x-2*z*z,	2*y*z+2*w*x,
		2*x*z+2*w*y,	2*y*z-2*w*x,	1-2*x*x-2*y*y;
	return rsl.inverse();
}

float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

int main (int argc, char** argv)
{

	ros::init(argc, argv, "map_drawer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!map_drawer START!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	/////graphファイルの読み込み
	FILE *fp;
	fp = fopen("aft.csv","r");
	
/*
	
	aft.csvに書いてあるnode情報を参考に
	そのnodeの座標中心における地図を切り抜き保存していく	
	
*/	

	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	ros::Publisher pub_drawed_map;
	
	sensor_msgs::PointCloud2 pc;
	
	
 	pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
	
	///////////////////////////////////////////////
	
	
	
	
	//cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
	//cout<<"Visualize Start!"<<endl;
	//while(ros::ok() ){
///////////////////////////////////////////////////////////////////////////////////////
	
	int file_num = 1;
	
	pcl::PointCloud<pcl::PointNormal>::Ptr map_cloud (new pcl::PointCloud<pcl::PointNormal>);//いま
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_1 (new pcl::PointCloud<pcl::PointNormal>);//いま
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_2 (new pcl::PointCloud<pcl::PointNormal>);//1stepまえ
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_3 (new pcl::PointCloud<pcl::PointNormal>);//2stepまえ
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_1_ (new pcl::PointCloud<pcl::PointNormal>);//いま：代入用
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_2_ (new pcl::PointCloud<pcl::PointNormal>);//1stepまえ：代入用
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_3_ (new pcl::PointCloud<pcl::PointNormal>);//2stepまえ：代入用
	pcl::PointCloud<pcl::PointNormal>::Ptr drawed_cloud (new pcl::PointCloud<pcl::PointNormal>);

	int cloud_count = 0;
	float dat_1[8];
	float dat_2[8];
	float dat_3[8];
	
	while(fscanf(fp,"%s",s) != EOF){
		float dat[8];
		int num;
		
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
		
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			dat[7] = num;
			
			Eigen::Matrix4f S_1 = Eigen::Matrix4f::Identity();
			Eigen::Matrix4f S_2 = Eigen::Matrix4f::Identity();
			Eigen::Matrix4f S_3 = Eigen::Matrix4f::Identity();
			
			//x,y,z,quat情報を保存
			//
			for(int i=0;i<8;i++){
				dat_3[i] = dat_2[i];
			}			
			cloud_3->points.clear();
			*cloud_3 = *cloud_2;
			//cout<<"cloud_3->points.size() : "<<cloud_3->points.size()<<endl;
			cout<<"dat_3[] : "<< dat_3[0] <<" , "<< dat_3[1] <<" , "<< dat_3[7] <<" , "<<endl;
			for(int i=0;i<8;i++){
				dat_2[i] = dat_1[i];
			}
			cloud_2->points.clear();
			*cloud_2 = *cloud_1;
			//cout<<"cloud_2->points.size() : "<<cloud_2->points.size()<<endl;
			cout<<"dat_2[] : "<< dat_2[0] <<" , "<< dat_2[1] <<" , "<< dat_2[7] <<" , "<<endl;
			for(int i=0;i<8;i++){
				dat_1[i] = dat[i];
			}
			cloud_1->points.clear();
			char filename[100];
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud_1) == -1){
				cout << "cloud_" << num << "not found." << endl;
				return 0;
			}
			//cout<<"cloud_1->points.size() : "<<cloud_1->points.size()<<endl;
			cout<<"dat_1[] : "<< dat_1[0] <<" , "<< dat_1[1] <<" , "<< dat_1[7] <<" , "<<endl;
			
			//3スキャン分用意できたら
			//
			if(cloud_count != 3){
				cloud_count ++;
				cout<<"num : "<<num<<endl;
			}
			
			//nodeが断裂している場合の処理
			//
			cout<<"d1 : "<<calcdist(dat_1[0],dat_1[1],dat_2[0],dat_2[1])<<endl;
			//if(calcdist(dat_1[0],dat_2[0],dat_1[1],dat_2[1])<=DIST_NODE && calcdist(dat_2[0],dat_3[0],dat_2[1],dat_3[1])<=DIST_NODE ){
			if(calcdist(dat_1[0],dat_1[1],dat_2[0],dat_2[1])<=DIST_NODE && calcdist(dat_2[0],dat_2[1],dat_3[0],dat_3[1])<=DIST_NODE ){
				if(cloud_count == 3){
					cout<<"   num : "<<num<<endl;
					
					//回転行列に代入していく
					//
					S_1(0,3) = dat_1[0] - dat_2[0];
					S_1(1,3) = dat_1[1] - dat_2[1];
					S_1(2,3) = dat_1[2] - dat_2[2];
				
					S_2(0,3) = 0.0;
					S_2(1,3) = 0.0;
					S_2(2,3) = 0.0;
			
					S_3(0,3) = dat_3[0] - dat_2[0];
					S_3(1,3) = dat_3[1] - dat_2[1];
					S_3(2,3) = dat_3[2] - dat_2[2];
			
					Eigen::Matrix3f kaiten_1 = quat2mat(dat_1[3],dat_1[4],dat_1[5],dat_1[6]);
					for(int i=0;i<3;i++){
						for(int j=0;j<3;j++){
							S_1(i,j) = kaiten_1(i,j);
						}
					}
					Eigen::Matrix3f kaiten_2 = quat2mat(dat_2[3],dat_2[4],dat_2[5],dat_2[6]);
					for(int i=0;i<3;i++){
						for(int j=0;j<3;j++){
							S_2(i,j) = kaiten_2(i,j);
						}
					}
					Eigen::Matrix3f kaiten_3 = quat2mat(dat_3[3],dat_3[4],dat_3[5],dat_3[6]);
					for(int i=0;i<3;i++){
						for(int j=0;j<3;j++){
							S_3(i,j) = kaiten_3(i,j);
						}
					}
					
					//2番nodeを基準として1,3を平行移動，回転させmap_cloudへpush_back
					//
					*cloud_1_ = transformer(*cloud_1,S_1);
					*cloud_2_ = transformer(*cloud_2,S_2);
					*cloud_3_ = transformer(*cloud_3,S_3);
				
					for(size_t i=0;i<cloud_3->points.size();i++){
						map_cloud->push_back(cloud_3_->points[i]);
					}
					for(size_t i=0;i<cloud_2->points.size();i++){
						map_cloud->push_back(cloud_2_->points[i]);
					}
					for(size_t i=0;i<cloud_1->points.size();i++){
						map_cloud->push_back(cloud_1_->points[i]);
					}
				
					cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
				
					/*
					pcl::toROSMsg(*drawed_cloud, pc);
					cout<<"drawed_cloud->points.size() : "<<drawed_cloud->points.size()<<endl;
					char outfile[100];
					sprintf(outfile, "drawed_map/%d.pcd", file_num);
				    pcl::io::savePCDFileBinary(outfile, *drawed_cloud);
				    cout<<"Finish saving drawed_cloud to : "<<outfile<<endl;
				    */
				    pcl::toROSMsg(*map_cloud, pc);
					cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
					char outfile[100];
					sprintf(outfile, "drawed_map/%d.pcd", file_num);
				    pcl::io::savePCDFileBinary(outfile, *map_cloud);
				    cout<<"Finish saving map_cloud to : "<<outfile<<endl;
				    
					map_cloud->points.clear();
					drawed_cloud->points.clear();
					file_num++;
				
				}
			}
		}
////////////////////////////////////////////////////////////////////		

		pc.header.frame_id = "velodyne";
		//pc.header.frame_id = "map";
	
		pc.header.stamp = ros::Time::now();
		pub_drawed_map.publish(pc);
		
	}
	
	cout<<"Finish map drawing."<<endl;
}

