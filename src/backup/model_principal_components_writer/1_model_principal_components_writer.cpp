//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : model_principal_components_writer.cpp
//author    : x.xxx
//created   : 2016.08.02
//lastupdate: 2016.

/*
起動方法
pkg:gauss_sphere
src:map_drawer.cpp
をaft.csvのある階層で起動後
drawed_mapに切り抜きされたmapが作成される

その後aft.csvのある場所で
rosrun gauss_sphere model_principal_components_writer

処理内容
切り抜かれた地図に対して主平面を取り出しファイルに書き込んでいくプログラム
	/AMSL_ros_pkg/Dgauss_sphere/Dgauss/■■■/●●●.txtにaft.csvのnodeの位置情報を保存している
	/AMSL_ros_pkg/Dgauss_sphere/Dgaiss_list/●●●.txtには上から順に保存（1046.txtが一番最初ならそれを1行目にそのx,y座標を保存している）

*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

#include <clustering_writer.h>
#include <time_util/stopwatch.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

sensor_msgs::PointCloud2 ros_dgs;
sensor_msgs::PointCloud2 pc2;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> dom_clusters;//dom=dominant
bool pc_callback_flag = false;

const float THRESH_NUM=0.0;
const float gauss_sphere_range = 1.0;
const float DIST = 20.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}


void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

void make_gauss_sphere(	pcl::PointCloud<pcl::PointNormal> pc,
					  	pcl::PointCloud<pcl::PointXYZINormal> *pc_gs,
						pcl::PointCloud<pcl::PointXYZINormal> *pc_dgs)
{
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out2;
	pcl::PointCloud<pcl::PointXYZINormal> pc_out_c;
	size_t size_pc_points = pc.points.size();
	for(size_t i=0;i<size_pc_points;i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;
		
		//for localization
		if(fabs(p.normal_z) > 0.8) continue;
		if(p.z <= -0.5) continue;
		
		float color = 0.0;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;

		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			else{
			}

			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
			else{
			}
		}

		if((fabs(p.x)>fabs(p.y)) && (fabs(p.x)>fabs(p.z))){
			if(p.x<0) color = 30*(fabs(p.y)-fabs(p.z))/(max-min)+180.0;
			else color = 30*(fabs(p.y)-fabs(p.z))/(max-min);
		}else if((fabs(p.y)>fabs(p.z)) && (fabs(p.y)>fabs(p.x))){
			if(p.y<0) color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0;
		}else if((fabs(p.z)>fabs(p.x)) && (fabs(p.z)>fabs(p.y))){
			if(p.z<0)color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0;
		}else{
		}

		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		p.intensity = color;
		pc_out.points.push_back(p);//単位ガウス球

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = color;

		float range = (pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		pc_out2.points.push_back(pp);//深さつきガウス球
	}
	
	*pc_gs = pc_out;
	*pc_dgs = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
	
}

//	perfect_velodyneのものはcallbackなので名前を変更した
//  元の名前はclustering
//
void dominant_clustering(pcl::PointCloud<pcl::PointXYZINormal> *dgauss_sphere)
{
	Clustering clustering;
	clustering.setMinMaxNum(100,INT_MAX);
	clustering.setThreshold(0.6, 0.1);	//2016.01.21 comment out
	//clustering.setThreshold(0.80, 0.5);	//angle and distance
	//clustering.setThreshold(0.80, 0.8);	//angle and distance
	
	//*ros_dgs->data.clear();
	pcl::toROSMsg(*dgauss_sphere, ros_dgs);
	clustering.putTank(ros_dgs);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc2);
	
	//ros_dgs.header.frame_id = "/velodyne";
	//ros_dgs.header.stamp = ros::Time::now();
	pcl::fromROSMsg(pc2, dom_clusters);

	//pcl::toROSMsg(dom_clusters, pc2_w);
	//pc2_w.header.frame_id="velodyne";
	//pc2_w.header.stamp=ros::Time::now();
	//pub2.publish(pc2_w);
	//pcl::PointCloud<pcl::PointXYZINormal> pc_dgsc;

}


int main (int argc, char** argv)
{

	ros::init(argc, argv, "model_principal_components_writer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!model_principal_components_writer!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	//graphファイルの読み込み
	//
	FILE *fp;
	fp = fopen("aft.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	//保存先ファイルの作成
	//
	FILE *fp2;
	char filename_2[100];
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_all.txt");//ファイルの作成
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/tsukuba_all.txt");//ファイルの作成
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/D_kan.txt");//ファイルの作成
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/building_d.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	//TODO
	//Publisher
	//
	//	/perfect_velodyne/normal
	//	/gauss_shpere_depth
	//	/主平面
/*	
	aft.csvを順に読んでいく
		x,yを書き込む
		一つ目と、drawed_map内の1.pcdが対応する
		1.pcdを読み込んでくる
		Dgauss_sphereを作る(make_gauss_sphere)
		主平面を取り出す(filtering)
		その座標を書き込む
	
*/	

	
	ros::Publisher pub_drawed_map;
	sensor_msgs::PointCloud2 pc;
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
	
	//while(ros::ok() ){
///////////////////////////////////////////////////////////////////////////////////////

	int file_num = 1;
		
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			//////////////処理時間の計測//////////////
			struct timeval b_sec, a_sec;
			gettimeofday(&b_sec, NULL);
			/////////////////////////////////////////
			
			
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );//nodeのx,y座標をtsukuba_all.txtに書き込む
		
			cout << "file_num = " << file_num << endl;
			char filename[100];
			//sprintf(filename,"clouds/cloud_%d.pcd",num);
			sprintf(filename,"drawed_map/%d.pcd",file_num);
			
			pcl::PointCloud<pcl::PointNormal>::Ptr map_cloud (new pcl::PointCloud<pcl::PointNormal>);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *map_cloud) == -1){
				cout << file_num <<".pcd not found." << endl;
				break;
			}
            
            //Dgauss_sphereを作る(make_gauss_sphere)
			//
			pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs;
			make_gauss_sphere(*map_cloud, &pc_gs, &pc_dgs); 
			
			//主平面を取り出す(filtering)
          	//
          	dominant_clustering(&pc_dgs);

            char outfile[100];
            for(size_t i=0;i<dom_clusters.points.size();i++){
				cout<<dom_clusters.points[i].x<<" , "<<dom_clusters.points[i].y<<endl;
			}
            FILE *fp1;
            char filename_1[100];
            //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
            //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/tsukuba_all/%d.txt",num);//ファイルの作成
            sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/building_d/%d.txt",file_num);//ファイルの作成
            //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_all/%d.txt",num);//ファイルの作成 for TSUKUBA
            
		        
		     
		    if((fp1 = fopen(filename_1, "a+")) == NULL){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			
		    // 処理（ここでは、下記文字をファイルに書き込む）
			for(size_t i=0;i<dom_clusters.points.size();i++){
				fprintf(fp1, "%f,%f,%f\n", dom_clusters.points[i].x,dom_clusters.points[i].y,dom_clusters.points[i].z);
			}
			// ファイルを閉じる 
			fclose(fp1);
            
            //dom_clusters の初期化
			//
            dom_clusters.points.clear();
            
            
            //////////////処理時間の計測//////////////
			gettimeofday(&a_sec, NULL);
			cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
			/////////////////////////////////////////
           
            
            //cout << cnt << " / " << max_index << endl << endl;
			//if(cnt == max_index)break;
			cnt += skip;
			file_num ++;
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);



	//char outfile[100] = "../../AMSL_ros_pkg/test_for_principal_components.pcd";
	//sprintf(outfile, "../../AMSL_ros_pkg/test_for_principal_components.pcd", file_num);
    //pcl::io::savePCDFileBinary(outfile, *map_cloud);
	//drawed_cloud->points.clear();
	//file_num++;
	
////////////////////////////////////////////////////////////////////		
	
	cout<<"Finish."<<endl;
}

