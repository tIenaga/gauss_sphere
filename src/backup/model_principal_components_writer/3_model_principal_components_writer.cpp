//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : model_principal_components_writer.cpp
//author    : x.xxx
//created   : 2016.08.02
//lastupdate: 2016.

/*
起動方法
pkg:gauss_sphere
src:map_drawer.cpp
をaft.csvのある階層で起動後
drawed_mapに切り抜きされたmapが作成される

その後aft.csvのある場所で
rosrun gauss_sphere model_principal_components_writer

処理内容
切り抜かれた地図に対して主平面を取り出しファイルに書き込んでいくプログラム
	/AMSL_ros_pkg/Dgauss_sphere/Dgauss/■■■/●●●.txtにaft.csvのnodeの位置情報を保存している
	/AMSL_ros_pkg/Dgauss_sphere/Dgaiss_list/●●●.txtには上から順に保存（1046.txtが一番最初ならそれを1行目にそのx,y座標を保存している）
	
	クラスタリングはperfect_velodyneにおけるクラスタリングを用いている
	euclidian clusterを利用する場合をコメントアウトしている
	
*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

#include <clustering_writer.h>
#include <time_util/stopwatch.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

sensor_msgs::PointCloud2 ros_dgs;
sensor_msgs::PointCloud2 pc2;

ros::Publisher pub_normal;
ros::Publisher pub_gs;
ros::Publisher pub_dgs;
ros::Publisher pub_gsc;
ros::Publisher pub_dgsc;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> dom_clusters;//dom=dominant
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
pcl::PointCloud<pcl::PointXYZINormal>pc_out;
bool pc_callback_flag = false;

const float THRESH_NUM=0.0;
const float gauss_sphere_range = 1.0;
const float DIST = 20.0;
const float DIST_NODE = 5.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}

void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal> pc, pcl::PointCloud<pcl::PointXYZINormal> *pc_output,pcl::PointCloud<pcl::PointXYZINormal> *pc_output2){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out2;

	for(size_t i=0;i<pc.points.size();i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;

		//for localization
		if(fabs(p.normal_z) > 0.9) continue;
		if(pc.points[i].z < 0.2) continue;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;
		float c = color(xyz);

		p.intensity = c;
		pc_out.points.push_back(p);

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = c;

		float range = fabs(pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		//天井デバッグ
/*		if(c>75 && c<85 && sqrt(pp.x*pp.x+pp.y*pp.y+pp.z*pp.z)>5){
			cout<<pc.points[i].x<<" "<<pc.points[i].y<<" "<<pc.points[i].z<<endl;
			pc_out2.points.push_back(pp);
		}
*/
		//藤野さんの最適化プログラム
		//int r = int(sqrt(pow(pp.x,2)+pow(pp.y,2)+pow(pp.z,2)));
		//for(int j=0;j<r;j++) pc_out2.points.push_back(pp);
		pc_out2.points.push_back(pp);
	}
	
	*pc_output = pc_out;
	*pc_output2 = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
}

//	/backup/1_model_principal_components_writer.cpp におけるmake_gauss_sphereをコピペした
//
//
void make_gauss_sphere_color(	pcl::PointCloud<pcl::PointNormal> pc,
					  	pcl::PointCloud<pcl::PointXYZINormal> *pc_gs,
						pcl::PointCloud<pcl::PointXYZINormal> *pc_dgs)
{
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out2;
	pcl::PointCloud<pcl::PointXYZINormal> pc_out_c;
	size_t size_pc_points = pc.points.size();
	for(size_t i=0;i<size_pc_points;i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;
		
		//for localization
		if(fabs(p.normal_z) > 0.8) continue;
		if(p.z <= -0.5) continue;
		
		float color = 0.0;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;

		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			else{
			}

			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
			else{
			}
		}

		if((fabs(p.x)>fabs(p.y)) && (fabs(p.x)>fabs(p.z))){
			if(p.x<0) color = 30*(fabs(p.y)-fabs(p.z))/(max-min)+180.0;
			else color = 30*(fabs(p.y)-fabs(p.z))/(max-min);
		}else if((fabs(p.y)>fabs(p.z)) && (fabs(p.y)>fabs(p.x))){
			if(p.y<0) color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0;
		}else if((fabs(p.z)>fabs(p.x)) && (fabs(p.z)>fabs(p.y))){
			if(p.z<0)color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0;
		}else{
		}

		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		p.intensity = color;
		pc_out.points.push_back(p);//単位ガウス球

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = color;

		float range = (pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		pc_out2.points.push_back(pp);//深さつきガウス球
	}
	
	*pc_gs = pc_out;
	*pc_dgs = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
	
}

//	perfect_velodyneのものはcallbackなので名前を変更した
//  元の名前はclustering
//	<-perfect_velodyneで用いているクラスタリングの名前を変更した奴
//
void dominant_clustering(pcl::PointCloud<pcl::PointXYZINormal> *dgauss_sphere)
{
	Clustering clustering;
	clustering.setMinMaxNum(100,INT_MAX);
	//clustering.setThreshold(0.6, 0.1);	//2016.08.25 comment out -> principal_componets_extracter_for_DGaussとパラメータを同じにした（↓）
	clustering.setThreshold(0.80, 0.1);	//2016.01.21 comment out
	//clustering.setThreshold(0.80, 0.5);	//angle and distance
	//clustering.setThreshold(0.80, 0.8);	//angle and distance
	
	//*ros_dgs->data.clear();
	pcl::toROSMsg(*dgauss_sphere, ros_dgs);
	clustering.putTank(ros_dgs);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc2);
	
	//ros_dgs.header.frame_id = "/velodyne";
	//ros_dgs.header.stamp = ros::Time::now();
	pcl::fromROSMsg(pc2, dom_clusters);

	//pcl::toROSMsg(dom_clusters, pc2_w);
	//pc2_w.header.frame_id="velodyne";
	//pc2_w.header.stamp=ros::Time::now();
	//pub2.publish(pc2_w);
	//pcl::PointCloud<pcl::PointXYZINormal> pc_dgsc;

}

void clustering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output, bool dgs){
	pc_output->points.clear();
	pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	for(size_t i=0;i<pc_in.points.size();i+=skip){
		pcl::PointXYZ p;
		p.x = pc_in.points[i].x;
		p.y = pc_in.points[i].y;
		p.z = pc_in.points[i].z;
		input_cloud->points.push_back(p);
	}
	pc_in.points.clear();

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (input_cloud);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

	if(!dgs){
		ec.setClusterTolerance (0.01); 
		ec.setMinClusterSize (50);
		ec.setMaxClusterSize (1000);
	}else{
		ec.setClusterTolerance (0.05); 
		ec.setMinClusterSize (300);//150->50
		ec.setMaxClusterSize (100000);
	}	
	ec.setSearchMethod (tree);
	ec.setInputCloud (input_cloud);
	ec.extract (cluster_indices);

	std::vector<pcl::PointIndices>::const_iterator it;
	std::vector<int>::const_iterator pit;
	
	
	pc_out.points.clear();
	int cluster_num = 0;
	for(it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

		cluster_num++;
		int pc_num = 0; 
		float cx = 0.0;
		float cy = 0.0;
		float cz = 0.0;

		for(pit = it->indices.begin(); pit != it->indices.end(); pit++) {
			cloud_cluster->points.push_back(input_cloud->points[*pit]); 
			cx += input_cloud->points[*pit].x;
			cy += input_cloud->points[*pit].y;
			cz *= input_cloud->points[*pit].z;
			pc_num++;
		}
		
		pcl::PointXYZINormal p2;
		p2.x = cx/pc_num;
		p2.y = cy/pc_num;
		p2.z = cz/pc_num;
		p2.curvature = pc_num;	//point cloud density
		float r = sqrt(pow(p2.x,2)+pow(p2.y,2)+pow(p2.z,2));
		
		float xyz[3] = {0.0};
		xyz[0] = p2.x;
		xyz[1] = p2.y;
		xyz[2] = p2.z;
		p2.intensity = color(xyz);
		
		if(!dgs) pc_out.points.push_back(p2);
		else if(r>1.0) pc_out.points.push_back(p2);
		else{
		}
	}

	if(pc_out.points.size()==0){
		pc_out.points.resize(1);
		pc_out.points[0].x = 1000.0;
		pc_out.points[0].y = 1000.0;
		pc_out.points[0].z = 1000.0;
	}
	*pc_output = pc_out;
	//cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	if(dgs){
		for(size_t i=0;i<pc_out.points.size();i++){
			cout<<pc_out.points[i].x<<" , "<<pc_out.points[i].y<<endl;
		}
	}
}

void process(){
	bool pc_flag = false;
	pcl::PointCloud<pcl::PointXYZINormal> pc_in;
	if(pc_tmp.points.size()>0){
		//pc_callback_flag = false;
		pc_in = pc_tmp;
		pc_tmp.points.clear();
		pc_flag = true;
	}else{
		cout<<"subscribe no points."<<endl;
	}
	
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
	/////////////////////////////////////////

	//pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
	if(pc_flag){
		//gauss sphere 作成
		//
		cout<<"  gauss sphere making"<<endl;
		make_gauss_sphere(pc_in, &pc_gs, &pc_dgs);
		pc_in.points.clear();
		
		//clustering
		//
		cout<<"  clustering"<<endl;
		clustering(pc_gs, &pc_gsc, false);
		clustering(pc_dgs, &pc_dgsc, true);
	}else{
	}
	
	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
	cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	sensor_msgs::PointCloud2 ros_gs, ros_dgs, ros_gsc, ros_dgsc;
	pcl2ros(&pc_gs, &ros_gs);
	pcl2ros(&pc_dgs, &ros_dgs);
	pcl2ros(&pc_gsc, &ros_gsc);
	pcl2ros(&pc_dgsc, &ros_dgsc);
	
	pub_gs.publish(ros_gs);
	pub_gsc.publish(ros_gsc);
	pub_dgs.publish(ros_dgs);
	pub_dgsc.publish(ros_dgsc);
}

int main (int argc, char** argv)
{

	ros::init(argc, argv, "model_principal_components_writer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!model_principal_components_writer!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	//graphファイルの読み込み
	//
	FILE *fp;
	fp = fopen("aft.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	//保存先ファイルの作成
	//
	FILE *fp2;
	char filename_2[100];
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_all.txt");//ファイルの作成
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/tsukuba_all.txt");//ファイルの作成
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/D_kan.txt");//ファイルの作成
	
	//sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/building_d.txt");//ファイルの作成
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/jrm123_map.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	

	
	pub_normal = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal",1);
	pub_gs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	pub_gsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_clustered",1);
   	pub_dgs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	pub_dgsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_clustered",1);
	
	ros::Publisher pub_drawed_map;
	sensor_msgs::PointCloud2 pc;
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
	
	//while(ros::ok() ){
///////////////////////////////////////////////////////////////////////////////////////

	int file_num = 1;
	int dat_count = 0;
	float dat_1[7];
	float dat_2[7];
	float dat_3[7];
		
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			//x,y,z,quat情報を保存
			//
			for(int i=0;i<7;i++){
				dat_3[i] = dat_2[i];
				dat_2[i] = dat_1[i];
				dat_1[i] = dat[i];
			}			
			cout<<"dat_3[] : "<< dat_3[0] <<" , "<< dat_3[1] <<endl;
			cout<<"dat_2[] : "<< dat_2[0] <<" , "<< dat_2[1] <<endl;
			cout<<"dat_1[] : "<< dat_1[0] <<" , "<< dat_1[1] <<endl;
			
			
			if(dat_count != 3){
				dat_count ++;
				cout<<"num : "<<num<<endl;
			}
			cout<<"d1 : "<<calcdist(dat_1[0],dat_1[1],dat_2[0],dat_2[1])<<endl;
			//if(calcdist(dat_1[0],dat_2[0],dat_1[1],dat_2[1])<=DIST_NODE && calcdist(dat_2[0],dat_3[0],dat_2[1],dat_3[1])<=DIST_NODE ){
			if(calcdist(dat_1[0],dat_1[1],dat_2[0],dat_2[1])<=DIST_NODE && calcdist(dat_2[0],dat_2[1],dat_3[0],dat_3[1])<=DIST_NODE ){
				if(dat_count == 3){

					//////////////処理時間の計測//////////////
					struct timeval b_sec, a_sec;
					gettimeofday(&b_sec, NULL);
					/////////////////////////////////////////
			
			
					///////////////////////////////
					// 処理（ここでは、下記文字をファイルに書き込む）
					fprintf(fp2, "%f,%f\n",dat_2[0],dat_2[1] );//nodeのx,y座標をtsukuba_all.txtに書き込む
		
					cout << "file_num = " << file_num << endl;
					char filename[100];
					//sprintf(filename,"clouds/cloud_%d.pcd",num);
					sprintf(filename,"drawed_map/%d.pcd",file_num);
			
					pc_tmp.points.clear();
					pcl::PointCloud<pcl::PointNormal>::Ptr map_cloud (new pcl::PointCloud<pcl::PointNormal>);
					if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *map_cloud) == -1){
						cout << file_num <<".pcd not found." << endl;
						break;
					}
			
					//読み込んだ地図のpublish
					//
					cout<<"Map Visualization .."<<endl;
					sensor_msgs::PointCloud2 ros_normal;
					pcl::toROSMsg(*map_cloud, ros_normal);
					ros_normal.header.frame_id = "/velodyne";
					ros_normal.header.stamp = ros::Time::now();
					pub_normal.publish(ros_normal);
				    
				    //euclidian clusterを利用する場合はここから
				    //
				    /*
				    pc_tmp.points.resize( map_cloud->points.size() );
				    for(size_t i=0;i<map_cloud->points.size();i++){
				    	pc_tmp.points[i].x = map_cloud->points[i].x;
				    	pc_tmp.points[i].y = map_cloud->points[i].y;
				    	pc_tmp.points[i].z = map_cloud->points[i].z;
				    	
				    	pc_tmp.points[i].normal_x = map_cloud->points[i].normal_x;
				    	pc_tmp.points[i].normal_y = map_cloud->points[i].normal_y;
				    	pc_tmp.points[i].normal_z = map_cloud->points[i].normal_z;
				    }
				    
				    process();
				    
				    char outfile[100];
				    cout<<"NUMBER IS "<<pc_out.points.size()<<endl;
				    for(size_t i=0;i<pc_out.points.size();i++){
						cout<<"   "<<pc_out.points[i].x<<" , "<<pc_out.points[i].y<<endl;
					}
				    FILE *fp1;
				    char filename_1[100];
				    //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
				    //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/tsukuba_all/%d.txt",num);//ファイルの作成
				    sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/building_d/%d.txt",file_num);//ファイルの作成
				    //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_all/%d.txt",num);//ファイルの作成 for TSUKUBA
				    
						
					 
					if((fp1 = fopen(filename_1, "a+")) == NULL){
						fprintf(stderr, "%s\n", strerror(errno));
						exit(EXIT_FAILURE);
					}
			
					// 処理（ここでは、下記文字をファイルに書き込む）
					for(size_t i=0;i<pc_out.points.size();i++){
						fprintf(fp1, "%f,%f,%f\n", pc_out.points[i].x,pc_out.points[i].y,pc_out.points[i].z);
					}
					*/
					//
					//ここまで
					
					//perfect_velodyneと同じ方法でクラスタリングする場合はここ
					//
					//Dgauss_sphereを作る(make_gauss_sphere)
					//
					pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs;
					make_gauss_sphere_color(*map_cloud, &pc_gs, &pc_dgs); 
			
					//主平面を取り出す(filtering)
				  	//
				  	dominant_clustering(&pc_dgs);
					
					char outfile[100];
				    for(size_t i=0;i<dom_clusters.points.size();i++){
						cout<<dom_clusters.points[i].x<<" , "<<dom_clusters.points[i].y<<endl;
					}
				    FILE *fp1;
				    char filename_1[100];
				    //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
				    //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/tsukuba_all/%d.txt",num);//ファイルの作成
				    
				    //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/building_d/%d.txt",file_num);//ファイルの作成
				    sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/jrm123_map/%d.txt",file_num);//ファイルの作成
				    
				    //sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_all/%d.txt",num);//ファイルの作成 for TSUKUBA
					 
					if((fp1 = fopen(filename_1, "a+")) == NULL){
						fprintf(stderr, "%s\n", strerror(errno));
						exit(EXIT_FAILURE);
					}
			
					// 処理（ここでは、下記文字をファイルに書き込む）
					for(size_t i=0;i<dom_clusters.points.size();i++){
						fprintf(fp1, "%f,%f,%f\n", dom_clusters.points[i].x,dom_clusters.points[i].y,dom_clusters.points[i].z);
					}
					//
					//ここまで
				    
					// ファイルを閉じる 
					fclose(fp1);
				    
				    //pc_dgsc の初期化
					//
				    pc_dgsc.points.clear();
				    
				    
				    //////////////処理時間の計測//////////////
					gettimeofday(&a_sec, NULL);
					cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
					/////////////////////////////////////////
				    
				    //cout << cnt << " / " << max_index << endl << endl;
					//if(cnt == max_index)break;
					cnt += skip;
					file_num ++;
				}
			}
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);



	//char outfile[100] = "../../AMSL_ros_pkg/test_for_principal_components.pcd";
	//sprintf(outfile, "../../AMSL_ros_pkg/test_for_principal_components.pcd", file_num);
    //pcl::io::savePCDFileBinary(outfile, *map_cloud);
	//drawed_cloud->points.clear();
	//file_num++;
	
////////////////////////////////////////////////////////////////////		
	
	cout<<"Finish."<<endl;
}

