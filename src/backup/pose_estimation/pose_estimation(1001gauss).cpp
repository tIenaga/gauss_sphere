#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>


//principal componentsを受け取り，z成分のものを削除後，クラスタしてpublishするソース

using namespace std;
using namespace Eigen;

bool pc_callback_flag = false;
pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
ros::Publisher pub;
ros::Publisher pub_;

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

void pc_callback(sensor_msgs::PointCloud2ConstPtr msg){

	double y = 0.0;
	
	y *= M_PI/180.0;
	Matrix3d rot;
	rot << cos(y), -sin(y), 0.0, sin(y), cos(y), 0.0, 0.0, 0.0, 1.0;
	//reg.specialFeedBack(rot);

//	cout << "callback" << endl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::fromROSMsg(*msg, *tmp);
	size_t num = tmp->points.size();
	size_t cluster_number = 0;
	double dist = 0.0;
	//主平面の代入
	for(size_t i=0;i<num;i++){
		if(fabs(tmp->points[i].z)<=0.40){
			tmp->points[i].intensity = 0;
			tmp_cloud->points.push_back(tmp->points[i]);
		}
	}
	tmp_cloud_ = tmp_cloud;
	//cout << "*************************************" << endl;
	//主平面群のクラスタリング
	if(tmp_cloud->points.size() != 0){
		for(size_t i=0;i<tmp_cloud->points.size()-1;i++){
			//cout << "1*************************************" << endl;
			//tmp_cloud_cluster->points[cluster_number].intensity = 1;
			//cout << "2*************************************" << endl;
			if(tmp_cloud->points[i].intensity != 1.0){
				cluster_number++;
				tmp->points[cluster_number].x = tmp_cloud->points[i].x;
				tmp->points[cluster_number].y = tmp_cloud->points[i].y;
				tmp->points[cluster_number].z = tmp_cloud->points[i].z;
				tmp->points[cluster_number].intensity = 1.0;
				for(size_t j=i+1;j<tmp_cloud->points.size();j++){
					if(tmp_cloud->points[j].intensity != 1.0){
						//cout << "3*************************************" << endl;
						dist = sqrt( pow( (tmp_cloud->points[i].x - tmp_cloud->points[j].x) , 2.0)
							   + pow( (tmp_cloud->points[i].y - tmp_cloud->points[j].y) , 2.0) + pow( (tmp_cloud->points[i].z - tmp_cloud->points[j].z) , 2.0) );
						if(dist <= 0.1){
							//cout << "4*************************************" << endl;
							tmp->points[cluster_number].x += tmp_cloud->points[j].x;
							tmp->points[cluster_number].y += tmp_cloud->points[j].y;
							tmp->points[cluster_number].z += tmp_cloud->points[j].z;
							tmp->points[cluster_number].intensity += 1.0;
							tmp_cloud->points[j].intensity = 1;
						}
					}
				}
				tmp->points[cluster_number].x /= tmp->points[cluster_number].intensity;
				tmp->points[cluster_number].y /= tmp->points[cluster_number].intensity;
				tmp->points[cluster_number].z /= tmp->points[cluster_number].intensity;
				tmp_cloud_cluster->points.push_back(tmp->points[cluster_number]);	
			}
		}
	}
	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud_cluster, pc);
	pcl::toROSMsg(*tmp_cloud_, pc_);
	
	pc.header.frame_id = "/velodyne";
	pc.header.stamp = ros::Time::now();
	pub.publish(pc);

	pc_.header.frame_id = "/velodyne";
	pc_.header.stamp = ros::Time::now();
	pub_.publish(pc_);
//	pub_pose.publish(pose);
//	pub_pair.publish(marker);
//	cout <<"aaa"<< endl;
//	cout << "*************************************" << endl;
}

int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate roop(20);
	
	ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	//ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォルのトピック名
	/*ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	*/
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		//cout << "     Here We Go !!!" << endl;	
		
		MatrixXd Model(3,4);
		Model<< 1.001, 0.001, -1.001, 0.00,
				0.003, 1.001, 0.00, -1.00,
				0.002, 0.002, 0.00, 0.00;
		MatrixXd Query(3,4);
		Query<< 1.002, 0.004, -1.003, 0.00,
				0.003, 1.001, 0.00, -1.00,
				0.002, 0.004, 0.00, 0.00;
		double angle = -4.000;
		double query_x,query_y;
		double theta = angle / 180.0 * 3.14159265358979;
		double dist[4]=	{0.0, 0.0, 0.0, 0.0};
		double min[4] =	{0.0, 0.0, 0.0, 0.0};
		int pair[4] = {0, 0, 0, 0};//Queryのprincipal componentの数分確保する
		
		//クエリの回転部分
		for(int i=0;i<4;i++){
			query_x = Query(0,i);
			query_y = Query(1,i);
			Query(0,i) = query_x * cos(theta) - query_y * sin(theta);
			Query(1,i) = query_x * sin(theta) + query_y * cos(theta);
		}
		
		//ペアリング
		for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){
				dist[j] = sqrt( pow(Model(0,j)-Query(0,i),2.0) + pow(Model(1,j)-Query(1,i),2.0) + pow(Model(2,j)-Query(2,i),2.0) );
				//cout<<"dist = "<<dist[j]<<endl;
			}
			//クエリベクトル(t)に対して最も距離の短いモデルベクトル(t-1)を探す
			min[i] = dist[0];
			for(int j=0;j<4;j++){
				if(min[i]>=dist[j]){
					min[i] = dist[j];
					pair[i] = j;
				}
			}
			//cout<<"pair = "<<pair[i]<<endl;
		}
		
		//回転角度の算出
		for(int i=0;i<4;i++){
			double ang1 = atan2(Query(1,i) , Query(0,i)) * 180.0 / 3.14159265358979;
			double ang2 = atan2(Model(1,pair[i]) , Model(0,pair[i])) * 180.0 / 3.14159265358979;
			double yaw = ang1 - ang2;
			if(yaw < -180.0) yaw += 360.0;
			if(yaw >  180.0) yaw -= 360.0;
			
			//if(ang1<0.0) ang1 += 360.0;
			//if(ang2<0.0) ang2 += 360.0;
			
			//cout<<"yaw = "<<yaw<<"  ang1,ang2 = "<<ang1<<" ,  "<<ang2<<endl;
			//cout<<atan(Query(1,i) / Query(0,i)) * 180.0 / 3.14159265358979<<"   "<<atan(Model(1,pair[i]) / Model(0,pair[i])) * 180.0 / 3.14159265358979<<endl;	
			
			//cout<<"theta = "<<acos(Query(0,i) * Model(0,pair[i]) + Query(1,i) * Model(1,pair[i]))*180.0/3.14159265358979<<endl;
		}
		
		
		
		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
