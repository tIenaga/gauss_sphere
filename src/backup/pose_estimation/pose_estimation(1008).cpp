#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>


using namespace std;
using namespace Eigen;

bool pc_callback_flag = false;
bool DGauss_match = true;
pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pre_pc2;
nav_msgs::Odometry lcl;
nav_msgs::Odometry Dlcl;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pre_pub2;

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
	//////////////処理時間の計測//////////////
	//struct timeval b_sec, a_sec;
	//gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////
	double y = 0.0;
	
	y *= M_PI/180.0;
	Matrix3d rot;
	rot << cos(y), -sin(y), 0.0, sin(y), cos(y), 0.0, 0.0, 0.0, 1.0;
	//reg.specialFeedBack(rot);

//	cout << "callback" << endl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::fromROSMsg(*msg, *tmp);
	size_t num = tmp->points.size();
	size_t cluster_number = 0;
	double dist = 0.0;
	//主平面の代入
	for(size_t i=0;i<num;i++){
		if(fabs(tmp->points[i].z)<=0.40){
			tmp->points[i].intensity = 0;
			tmp_cloud->points.push_back(tmp->points[i]);
		}
	}
	tmp_cloud_ = tmp_cloud;
	//cout << "*************************************" << endl;
	//主平面群のクラスタリング
	if(tmp_cloud->points.size() != 0){
		for(size_t i=0;i<tmp_cloud->points.size()-1;i++){
			//cout << "1*************************************" << endl;
			//tmp_cloud_cluster->points[cluster_number].intensity = 1;
			//cout << "2*************************************" << endl;
			if(tmp_cloud->points[i].intensity != 1.0){
				cluster_number++;
				tmp->points[cluster_number].x = tmp_cloud->points[i].x;
				tmp->points[cluster_number].y = tmp_cloud->points[i].y;
				tmp->points[cluster_number].z = tmp_cloud->points[i].z;
				tmp->points[cluster_number].intensity = 1.0;
				for(size_t j=i+1;j<tmp_cloud->points.size();j++){
					if(tmp_cloud->points[j].intensity != 1.0){
						//cout << "3*************************************" << endl;
						dist = sqrt( pow( (tmp_cloud->points[i].x - tmp_cloud->points[j].x) , 2.0)
							   + pow( (tmp_cloud->points[i].y - tmp_cloud->points[j].y) , 2.0) + pow( (tmp_cloud->points[i].z - tmp_cloud->points[j].z) , 2.0) );
						if(dist <= 0.2){
							//cout << "4*************************************" << endl;
							tmp->points[cluster_number].x += tmp_cloud->points[j].x;
							tmp->points[cluster_number].y += tmp_cloud->points[j].y;
							tmp->points[cluster_number].z += tmp_cloud->points[j].z;
							tmp->points[cluster_number].intensity += 1.0;
							tmp_cloud->points[j].intensity = 1;
						}
					}
				}
				tmp->points[cluster_number].x /= tmp->points[cluster_number].intensity;
				tmp->points[cluster_number].y /= tmp->points[cluster_number].intensity;
				tmp->points[cluster_number].z /= tmp->points[cluster_number].intensity;
				tmp_cloud_cluster->points.push_back(tmp->points[cluster_number]);//クラスタした点
			}
		}
		
		
		
		
	}
	
		
	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud_cluster, pc2);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	pc2.header.frame_id = "/velodyne";//クラスタした点
	pc2.header.stamp = ros::Time::now();
	pub2.publish(pc2);
	pc2.data.clear();
	
	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();
	
	pre_pc2.header.frame_id = "/velodyne";//クラスタした点の1ステップ前のもの
	pre_pc2.header.stamp = ros::Time::now();
	pre_pub2.publish(pre_pc2);
	pre_pc2.data.clear();
	
	pre_gauss = tmp_cloud_cluster;
	//////////////処理時間の計測//////////////
	//gettimeofday(&a_sec, NULL);
	//cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	
}

void lcl_callback(nav_msgs::Odometry msg){
	lcl.pose.pose.position.x = msg.pose.pose.position.x;
	lcl.pose.pose.position.y = msg.pose.pose.position.y;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = msg.pose.pose.orientation.z;
	
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate roop(20);
	
	//ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);
	ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォルのトピック名
	/*ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	*/
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	
	ros::Publisher pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		//////////////処理時間の計測//////////////
		struct timeval b_sec, a_sec;
		gettimeofday(&b_sec, NULL);
		/////////////////////////////////////////
		MatrixXd Model(3,6);
		Model<< 6.00, 0.00, -10.0, 0.00, 10.00, 0.00, 
				0.00, 11.0, 0.00, -9.0, 0.00, 5.00,
				0.00, 0.00, 0.00,  0.00, 0.00, 0.00;
		MatrixXd Query(3,6);
		Query<< 0.00, -9.00, 0.00, 7.00, 11.00, 0.00,
				10.5, 0.00, -9.5, 0.00, 0.00, 4.50,
				0.00, 0.00, 0.00 , 0.00, 0.00, 0.00;
		
		double angle = 0.000;
		double query_x,query_y;
		double theta = angle / 180.0 * 3.14159265358979;
		double dist[6]=	{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		double min[6] =	{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		int m_pair[6] = {0, 0, 0, 0, 0, 0};//modelのprincipal componentの数分確保する
		int q_pair[6] = {0, 0, 0, 0, 0, 0};//queryのprincipal componentの数分確保する
		
		//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
		//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
		
		for(size_t i=0;i<(size_t)Model.cols();i++){
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
			if(m_pair[i] != 1){//pair[i]=1で確認済
				m_pair[i] = 1;
				
				tmp_m->points.resize (5);//変更必要
				tmp_m->points[0].x = Model(0,i);
				tmp_m->points[0].y = Model(1,i);
				tmp_m->points[0].z = Model(2,i);
				cloud_m->points.push_back(tmp_m->points[0]);//今見ている点を格納する
				cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
				
				for(size_t j=i+1;j<(size_t)Model.cols();j++){
					double dot_m = ( Model(0,i)*Model(0,j) + Model(1,i)*Model(1,j) + Model(2,i)*Model(2,j) ) / 
								 (sqrt( Model(0,i)*Model(0,i) + Model(1,i)*Model(1,i) + Model(2,i)*Model(2,i) ) * sqrt( Model(0,j)*Model(0,j) + Model(1,j)*Model(1,j) + Model(2,j)*Model(2,j) ) );
					if( fabs(dot_m) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						m_pair[j] = 1;
			
						tmp_m->points[0].x = Model(0,j);
						tmp_m->points[0].y = Model(1,j);
						tmp_m->points[0].z = Model(2,j);
						cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
						
						cloud_m->points.push_back(tmp_m->points[0]);//ある角度以内に存在する点群を格納していく
					}	
				}
				
				
				
				for(size_t j=0;j<(size_t)Query.cols();j++){
					if(q_pair[j] != 1){//pair[j]=1で確認済
				
						double dot_q = ( Model(0,i)*Query(0,j) + Model(1,i)*Query(1,j) + Model(2,i)*Query(2,j) ) / 
									 (sqrt( Model(0,i)*Model(0,i) + Model(1,i)*Model(1,i) + Model(2,i)*Model(2,i) ) * sqrt( Query(0,j)*Query(0,j) + Query(1,j)*Query(1,j) + Query(2,j)*Query(2,j) ) );
						if( fabs(dot_q) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
							q_pair[j] = 1;
							
							tmp_q->points.resize (5);//変更必要
							tmp_q->points[0].x = Query(0,j);
							tmp_q->points[0].y = Query(1,j);
							tmp_q->points[0].z = Query(2,j);
							cout<<" tmp_q : "<<tmp_q->points[0]<<endl;
						
							cloud_q->points.push_back(tmp_q->points[0]);//ある角度以内に存在する点群を格納していく
						}
					}	
				}
				
				cout<<"i = "<<i<<" cloud_m : "<<cloud_m->points.size()<<endl;
				cout<<"i = "<<i<<" cloud_q : "<<cloud_q->points.size()<<endl;
				//cout<<"i = "<<i<<" cloud_m : "<<cloud_m->points[0]<<endl;
				//cout<<"i = "<<i<<" cloud_q : "<<cloud_q->points[0]<<endl;
				//cloud_m->is_dense = false;
				//cloud_q->is_dense = false;
				if( cloud_q->points.size()>=3 && cloud_m->points.size()>=3 ){
					pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
					icp.setInputCloud(cloud_q);
					icp.setInputTarget(cloud_m);
					pcl::PointCloud<pcl::PointXYZ> Final;
					icp.align(Final);
					std::cout << "has converged:" << icp.hasConverged() << " score: " <<
					icp.getFitnessScore() << std::endl;
					Eigen::Matrix4f transformation = icp.getFinalTransformation ();//icp
					std::cout << icp.getFinalTransformation() << std::endl;
					std::cout << transformation << std::endl;
					std::cout << std::endl;
				}
				
				
			}
		}
		
		//////////////処理時間の計測//////////////
		gettimeofday(&a_sec, NULL);
		cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
		/////////////////////////////////////////
		
		if(DGauss_match){
			//////////////////////////
			//  位置算出するところ		//
			//////////////////////////
			
			pub_DGauss_pose.publish(Dlcl);
			DGauss_match = false;
		}	
		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
