#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_test.txt";
//const char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";

bool pc_callback_flag = false;
bool DGauss_match = true;
bool match = false;
bool dgauss_flag = false;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 rotated_pc2_debug;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pc2_rot;
sensor_msgs::PointCloud2 pre_pc2;
nav_msgs::Odometry lcl;
nav_msgs::Odometry lcl_ini;
nav_msgs::Odometry Dlcl;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pub2_rot;
ros::Publisher pre_pub2;
ros::Publisher pub_DGauss_pose;

const float gauss_sphere_range = 1.0;
const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]
const float D_THRESHOLD = 2.0; //[m] model gaussとの距離の閾値

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

int cnt=0;
int search_gauss_num = 0;
float dist = 0.0;
vector< vector<float> > dgauss_position;
vector< vector<double> > gmodel;

pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱
//pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

float getdyaw(float ini, float azm){
	return azm - ini;
}

float calcdist(float x1, float x2, float y1, float y2){
	return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
	
	cout << "*******************************************" << endl;
	
	if(msg->data.size()==0){//msgに主平面が含まれていない場合
		ROS_FATAL("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}
	//dgaussの配置場所と自己位置を比較して距離を算出する
	//
	//	dgauss_position[i][]　にdgaussの座標が格納されている
	//
	
	vector< vector<double> > tmp_glist,tmp_vec;
	size_t num_glist = 3000;
	size_t cnt_glist = 0;
	tmp_glist.resize(num_glist);
	tmp_vec.resize(1);
	for(size_t i=0;i<num_glist;i++){
		tmp_glist[i].resize(2);
	}
	tmp_vec[0].resize(2);
	
	for(size_t i=0;i<(size_t)dgauss_position.size();i++){
		dist = calcdist(lcl.pose.pose.position.x,dgauss_position[i][0],lcl.pose.pose.position.y,dgauss_position[i][1]);
		cout<<"dist = "<<dist<<endl;
		if(dist <= D_THRESHOLD){//D_THRESHOLD内のgauss球の主平面を読み込む
			//
			//	D_THRESHOLD内に存在するdgaussをtmp_glist[i][j]に格納していく
			//	iは単なる数字 jは0:distance 1:dgauss numberを格納しておく
			//
			
			tmp_glist[cnt_glist][0] = dist;
			tmp_glist[cnt_glist][1] = i+1;
			cnt_glist++;
			
			search_gauss_num = i+1;
			dgauss_flag=true;
		}
	}
	tmp_glist.resize(cnt_glist);//resizeしてtmp_glistを
	
	//cout<<" D_THRESHOLD "<<D_THRESHOLD<<endl;
	//cout<<"flag = "<<dgauss_flag<<endl;
	if(dgauss_flag == false){//lcl と比較して threshold以内にガウス球が無い場合
		ROS_WARN("!!!!! NO D-GAUSS NEARBY INFANT !!!!!!");
		return;
	}
	
	
	cout<<"cnt_glist : "<<cnt_glist<<endl;
	//distの値を見て並び替え
	if(cnt_glist == 1){
		cout<<"aaaa"<<endl;
	}else if(cnt_glist == 0){
		ROS_WARN("!!!!! NO D-GAUSS NEARBY INFANT ");
		return;
	}else{
		cout<<"bbbb"<<endl;
		for(size_t i=0;i<cnt_glist-1;i++){
			for(size_t j=1;j<cnt_glist;j++){
				cout<<"----"<<endl;
				if(tmp_glist[i][0] >= tmp_glist[j][0]){
					tmp_vec[0][0] = tmp_glist[i][0];
					tmp_vec[0][1] = tmp_glist[i][1];
					tmp_glist[i][0] = tmp_glist[j][0];
					tmp_glist[i][1] = tmp_glist[j][1];
					tmp_glist[j][0] = tmp_vec[0][0];
					tmp_glist[j][1] = tmp_vec[0][1];
				}
			}
		}
		cout<<"cccc"<<endl;
		search_gauss_num = tmp_glist[0][1];
		cout<<"dddd"<<endl;
	}
	cout<<"eeee"<<endl;
	
	
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////
	double yaw = (-M_PI * 0.50) + lcl_ini.pose.pose.orientation.z + getdyaw(lcl_ini.pose.pose.orientation.z,lcl.pose.pose.orientation.z);
	cout << "yaw : " << yaw << " [rad] " << yaw * 180.0 / M_PI << " [deg]" <<endl;
//	cout << "lcl.pose.pose.orientation.z = " << lcl.pose.pose.orientation.z <<endl;
	
//	cout << "callback" << endl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZI>);
//	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);

	//pcl::PointCloud<pcl::PointXYZI>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZI>);
	//pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZI>);
	//pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZI>);
	//pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZI>);

	pcl::fromROSMsg(*msg, *tmp);
	size_t num = tmp->points.size();
	//size_t cluster_number = 0;
	//double dist = 0.0;
	//主平面の代入
	//cout<<"a"<<endl;
	
	//ここの処理は不要(無駄な代入)
	for(size_t i=0;i<num;i++){
		//if(fabs(tmp->points[i].z)<=0.40){//z軸に存在する主平面を除いた
			//tmp->points[i].intensity = 0;
			tmp_cloud->points.push_back(tmp->points[i]);			
			//cout << "tmp->points[i] : " << tmp->points[i] << endl;
		//}
	}
	*tmp_rot = *tmp_cloud;

	double cos_ = cos(yaw);
	double sin_ = sin(yaw);
	
	cout << "tmp_cloud->points[i]_____"<<endl;
	
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		tmp_cloud->points[i].x = ( tmp_rot->points[i].x * cos_ ) - ( tmp_rot->points[i].y * sin_ );
		tmp_cloud->points[i].y = ( tmp_rot->points[i].x * sin_ ) + ( tmp_rot->points[i].y * cos_ );
		cout << tmp_cloud->points[i].x <<","<< tmp_cloud->points[i].y <<","<< tmp_cloud->points[i].z <<","<< tmp_cloud->points[i].intensity << endl;
	}
	
	//
	//	model の Dgauss の主平面情報の読み込み
	//
	//	search_gauss_num :　閾値以下の距離になったgauss球の番号
	//
	FILE* fp2;
	char file_name_1[100];
	//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",search_gauss_num);
	sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_test/%d.txt",search_gauss_num);
	
	//model dgaussのための箱を用意する
	//
	gmodel.resize(1000);
	for(int i=0;i<1000;i++){
		gmodel[i].resize(3);
	}
	if( (fp2 = fopen(file_name_1,"r")) == NULL){
		cout << "Dgauss の主平面情報の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}else{
		float a,b,c = 0.0;
		int gcnt = 0;
		while(fscanf(fp2, "%f,%f,%f", &a, &b, &c) != EOF) {	
			gmodel[gcnt][0] = a;
			gmodel[gcnt][1] = b;
			gmodel[gcnt][2] = c;
			//cout<<gmodel[gcnt][0]<<"  "<<gmodel[gcnt][1]<<endl;
			gcnt++;
		}
		gmodel.resize(gcnt);
	}
	fclose(fp2);
	
	if(gmodel[0][0] == 1000){// 主平面が取れていない場合，txt fileには1000が代入されているため
		ROS_FATAL("!!!!! txt FILE DO NOT HAVE INFORMATION !!!!!!");
		return;
	}
	//
	//

	MatrixXd Vector(3,1);
	Vector<<0.0,
			0.0,
			0.0;

	int m_pair[(size_t)gmodel.size()];//modelのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)gmodel.size();i++)	m_pair[i]=0;
	int q_pair[(size_t)tmp_cloud->points.size()];//queryのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++)	q_pair[i]=0;
	
	//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
	
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	for(size_t i=0;i<(size_t)gmodel.size();i++){
		////////
		//debug
		cout<<"*******"<<endl;
		cout<<"i = "<<i<<endl;
			
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
		if(m_pair[i] != 1){//pair[i]=1で確認済
			m_pair[i] = 1;
			
			tmp_m->points.resize (1);//変更必要
			tmp_m->points[0].x = gmodel[i][0];//Model(0,i);
			tmp_m->points[0].y = gmodel[i][1];//Model(1,i);
			tmp_m->points[0].z = gmodel[i][2];//Model(2,i);
			cloud_m->points.push_back(tmp_m->points[0]);//今見ている点を格納する
//			cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
			
			for(size_t j=i+1;j<(size_t)gmodel.size();j++){
				if(m_pair[j] != 1){
					double dot_m = ( gmodel[i][0]*gmodel[j][0] + gmodel[i][1]*gmodel[j][1] /*+ gmodel[i][2]*gmodel[j][2]*/ ) / 
								 (sqrt( gmodel[i][0]*gmodel[i][0] + gmodel[i][1]*gmodel[i][1] /*+ gmodel[i][2]*gmodel[i][2]*/ ) * sqrt( gmodel[j][0]*gmodel[j][0] + gmodel[j][1]*gmodel[j][1] /*+ gmodel[j][2]*gmodel[j][2]*/ ) );
					if( fabs(dot_m) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						m_pair[j] = 1;
		
						tmp_m->points[0].x = gmodel[j][0];//Model(0,j);
						tmp_m->points[0].y = gmodel[j][1];//Model(1,j);
						tmp_m->points[0].z = gmodel[j][2];//Model(2,j);
						//cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
					
						cloud_m->points.push_back(tmp_m->points[0]);//ある角度以内に存在する点群を格納していく
					}
				}
			}
			
			
			
			for(size_t j=0;j<(size_t)tmp_cloud->points.size();j++){
				if(q_pair[j] != 1){//pair[j]=1で確認済
			
					double dot_q = ( gmodel[i][0]*tmp_cloud->points[j].x + gmodel[i][1]*tmp_cloud->points[j].y /*+ gmodel[i][2]*tmp_cloud->points[j].z*/ ) / 
								 (sqrt( gmodel[i][0]*gmodel[i][0] + gmodel[i][1]*gmodel[i][1] /*+ gmodel[i][2]*gmodel[i][2]*/ ) * sqrt( tmp_cloud->points[j].x*tmp_cloud->points[j].x + tmp_cloud->points[j].y*tmp_cloud->points[j].y /*+ tmp_cloud->points[j].z*tmp_cloud->points[j].z*/ ) );
					
					if( fabs(dot_q) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						q_pair[j] = 1;
						
						tmp_q->points.resize (1);//変更必要
						tmp_q->points[0].x = tmp_cloud->points[j].x;
						tmp_q->points[0].y = tmp_cloud->points[j].y;
						tmp_q->points[0].z = tmp_cloud->points[j].z;
						//cout<<" tmp_q : "<<tmp_q->points[0]<<endl;
				
						cloud_q->points.push_back(tmp_q->points[0]);//ある角度以内に存在する点群を格納していく
					}
				}	
			}
			
			//cout << "cloud_q : " << endl;
			//for(size_t k=0;k<cloud_q->points.size();k++){
			//	cout << cloud_q->points[k].x << "," << cloud_q->points[k].y << endl;
			//}
			//cout << "cloud_m : " << endl;
			//for(size_t k=0;k<cloud_m->points.size();k++){
			//	cout << cloud_m->points[k].x << "," << cloud_m->points[k].y << endl;
			//}
			
			if( cloud_q->points.size()>=3 && cloud_m->points.size()>=3 ){//取得点数が3点以上であるなら
				cout<<"---ICP---"<<endl;
				pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
				//icp.setInputCloud(cloud_q);//pcl1.8では廃止???
				icp.setInputSource(cloud_q);
				icp.setInputTarget(cloud_m);
				pcl::PointCloud<pcl::PointXYZ> Final;
				icp.align(Final);

				Eigen::Matrix4f transformation = icp.getFinalTransformation ();//icpによって得られた変換行列をEigenに代入

				Vector(0,0) += transformation(0,3);
				Vector(1,0) += transformation(1,3);
				Vector(2,0) += transformation(2,3);
				
				cout << " x : " << Vector(0,0) << endl;
				cout << " y : " << Vector(1,0) << endl;
				
				
				match = true;
			}else if( cloud_q->points.size()==0 || cloud_m->points.size()==0 ){
				cout<<"主平面はなし"<<endl;
				
			}else{//主平面が二点以下の場合の処理を書く
			//*********************************************************************************
				cout<<"---NN---"<<endl;
				pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
				kdtree.setInputCloud (cloud_m);//model gaussの情報を入れておく

				pcl::PointXYZ searchPoint;//query gaussのための箱
										  //query gaussの点から近いmodel gaussの点を探索する
				//cout << "主平面が二点以下の場合" << endl;

				// K nearest neighbor search

				int K = 1;
				double sum_x = 0.0;
				double sum_y = 0.0;

				std::vector<int> pointIdxNKNSearch(K);
				std::vector<float> pointNKNSquaredDistance(K);
//
//				std::cout << "K nearest neighbor search at (" << searchPoint.x 
//					<< " " << searchPoint.y 
//					<< " " << searchPoint.z
//					<< ") with K=" << K << std::endl;
				for(size_t l = 0; l < cloud_q->points.size();l++){
					
					searchPoint.x = cloud_q->points[l].x;
					searchPoint.y = cloud_q->points[l].y;
					searchPoint.z = cloud_q->points[l].z;
				
					if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 ){
						sum_x += cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x;
						sum_y += cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y;
						//cout<<"pointIdx="<<pointIdxNKNSearch[0]<<endl;
					}
				}
				sum_x /= (double)cloud_q->points.size();
				sum_y /= (double)cloud_q->points.size();
				
//				cout<<sum_x<<endl;
				
				Vector(0,0) += sum_x;
				Vector(1,0) += sum_y;
				//Vector(2,0) += transformation(2,3);
				
				cout << " x : " << Vector(0,0) << endl;
				cout << " y : " << Vector(1,0) << endl;
			//*********************************************************************************
			}
			
			
		}
	}
	//if(match) printf("x, y, z = %1.3f ,%1.3f ,%1.3f ",Vector(0,0),Vector(1,0),Vector(2,0));
	cout << " x : " << Vector(0,0) << endl;
	cout << " y : " << Vector(1,0) << endl;
	cout << " z : " << Vector(2,0) << endl;
	
	if(calcdist(0.0,Vector(0,0),0.0,Vector(1,0))>=D_THRESHOLD){
		ROS_FATAL("!!!!! MATCHING RESULT SEEMS WRONG !!!!!!");
		return;
	}
	
	//	
	//	matching しなかったときの対応(flagを使う)
	//	
	//	
	
	Dlcl.pose.pose.position.x = Vector(0,0) + dgauss_position[search_gauss_num-1][0];//txtから読み込んだ際は0始まり，
	Dlcl.pose.pose.position.y = Vector(1,0) + dgauss_position[search_gauss_num-1][1];
	Dlcl.pose.pose.orientation.z = lcl.pose.pose.orientation.z;
	
	//cout<<"Dlcl.x : "<<Dlcl.pose.pose.position.x<<endl;
	//cout<<"Dlcl.y : "<<Dlcl.pose.pose.position.y<<endl;
	
	pub_DGauss_pose.publish(Dlcl);
		
//	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud, rotated_pc2_debug);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
//	pcl::toROSMsg(*tmp_rot, pc2_rot);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	rotated_pc2_debug.header.frame_id = "/velodyne";//クラスタした点
	rotated_pc2_debug.header.stamp = ros::Time::now();
	pub2.publish(rotated_pc2_debug);
	rotated_pc2_debug.data.clear();

	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();

	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
	cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	dgauss_flag=false;
}

void lcl_callback(nav_msgs::Odometry msg){
	lcl.pose.pose.position.x = msg.pose.pose.position.x;
	lcl.pose.pose.position.y = msg.pose.pose.position.y;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = msg.pose.pose.orientation.z;
//	cout<< lcl.pose.pose.orientation.z * 180 / M_PI <<endl;
	
}

void lcl_ini_callback(nav_msgs::Odometry msg){
	lcl_ini.pose.pose.position.x = msg.pose.pose.position.x;
	lcl_ini.pose.pose.position.	y = msg.pose.pose.position.y;
	lcl_ini.pose.pose.orientation.z = msg.pose.pose.orientation.z;
	
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate loop(20);
	
	//ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	//ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);
	ros::Subscriber sub2 = n.subscribe("/gauss_sphere_depth_clustered",1,pc2_callback);
	//ros::Subscriber sub2 = n.subscribe("/gauss_sphere_clustered",1,pc2_callback);
	ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	ros::Subscriber sub_lcl_ini = n.subscribe("/lcl/initial_pose",1,lcl_ini_callback);
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォルのトピック名
	/*ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	*/
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rotated_pc2_query",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	//pub2_rot = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rot",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	
	//////////////////////////////////////////////////////////////////
	//	Dgaussを Dgauss_list/ikuta.txtから読み取る
	//	txt内は
	//	
	//	x , y <- /map でのDgaussの位置を保存していく
	//	
	//
	//////////////////////////////////////////////////////////////////
	
	FILE* fp;

	dgauss_position.resize(1000);
	for(int i=0;i<1000;i++){
		dgauss_position[i].resize(2);
	}
	
	if( (fp = fopen(file_name,"r")) == NULL){
		cout << "Dgauss_list の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}else{
		float a,b = 0.0;
		while(fscanf(fp, "%f,%f", &a, &b) != EOF) {	
			dgauss_position[cnt][0] = a;
			dgauss_position[cnt][1] = b;
			cout<<dgauss_position[cnt][0]<<"  "<<dgauss_position[cnt][1]<<endl;
			
			/*
			visualization_msgs::Marker marker;
			marker.header.frame_id = "map";
			marker.header.stamp = ros::Time();
			marker.ns = "dgauss_point";
			marker.id = cnt;
			marker.type = visualization_msgs::Marker::SPHERE;
			marker.action = visualization_msgs::Marker::ADD;
			marker.pose.position.x = a;
			marker.pose.position.y = b;
			marker.pose.position.z = 0;
			marker.pose.orientation.x = 0.0;
			marker.pose.orientation.y = 0.0;
			marker.pose.orientation.z = 0.0;
			marker.pose.orientation.w = 0.0;
			marker.scale.x = 1;
			marker.scale.y = 0.1;
			marker.scale.z = 0.1;
			marker.color.a = 1.0; // Don't forget to set the alpha!
			marker.color.r = 0.0;
			marker.color.g = 1.0;
			marker.color.b = 0.0;
			vis_pub.publish( marker );
			
			
			*/
			cnt++;
		}
		dgauss_position.resize(cnt);
	}
	fclose(fp);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		
		const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_test.txt";
		
		FILE* fp2;
		char file_name_1[100];
		//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",search_gauss_num);
		sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_test/%d.txt",search_gauss_num);
	
		
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
