#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

bool pc_callback_flag = false;
bool DGauss_match = true;
bool match = false;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 rotated_pc2_debug;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pc2_rot;
sensor_msgs::PointCloud2 pre_pc2;
nav_msgs::Odometry lcl;
nav_msgs::Odometry Dlcl;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pub2_rot;
ros::Publisher pre_pub2;

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){

	if(msg->data.size()==0){
		ROS_WARN("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////
	double yaw = lcl.pose.pose.orientation.z;
	//cout << "yaw = " << yaw *180.0 /M_PI <<endl;
	cout << "yaw = " << yaw <<endl;
	//y *= M_PI/180.0;
	//Matrix4d rot;
	//rot << cos(y), -sin(y), 0.0, 0.0, sin(y), cos(y), 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0;
	
	//queryをyaw方向に回転する->これにより/velodyneを/mapへ変換
	//その後/mapにおける変位量を計算する(いらなそう)
	
	//	/velodyneでの点群を/mapにするには
	//	-90.0[deg]して+yaw[deg]すれば/mapになるはず
	//	/mapでのD-gaussで比較し/mapでの相対位置を算出する
	
	
	//reg.specialFeedBack(rot);

//	cout << "callback" << endl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZI>);
//	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);
	pcl::fromROSMsg(*msg, *tmp);
	size_t num = tmp->points.size();
	size_t cluster_number = 0;
	double dist = 0.0;
	//主平面の代入
	//cout<<"a"<<endl;
	for(size_t i=0;i<num;i++){
		if(fabs(tmp->points[i].z)<=0.40){//z軸に存在する主平面を除いた
			tmp->points[i].intensity = 0;
			tmp_cloud->points.push_back(tmp->points[i]);			
			//cout << "tmp->points[i] : " << tmp->points[i] << endl;
		}
	}
	//cout<<"b"<<endl;
	*tmp_rot = *tmp_cloud;
	
	//for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
	//	cout<<tmp_cloud->points.size()<<" , "<<tmp_rot->points.size()<<endl;
	//	cout<< "tmp_rot : " << tmp_rot->points[i].x <<" , "<< tmp_rot->points[i].y <<" , "<< tmp_rot->points[i].z  <<endl;
	//	cout<< "tmp_cloud : " << tmp_cloud->points[i].x <<" , "<< tmp_cloud->points[i].y <<" , "<< tmp_cloud->points[i].z  <<endl;
	//}
	//yaw = (M_PI/6.0);
	double cos_ = cos(yaw);
	double sin_ = sin(yaw);
	
	//cout<<"tmp_cloud->points.size() : "<<tmp_cloud->points.size()<<endl;
	//cout<<"tmp_rot->points.size() : "<<tmp_rot->points.size()<<endl;
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		//cout<<tmp_cloud->points[i].x<<"  ,  "<<tmp_cloud->points[i].y<<endl;
		//cout<<tmp_rot->points[i].x<<"  ,  "<<tmp_rot->points[i].y<<endl;
		//cout<<"************************************************"<<endl;
		//cout<<sin_<<" , "<<cos_<<endl;
		
		//cout<<"i = "<<i<<endl;
		//tmp_rot->points[i].x = 1.0;
		//tmp_rot->points[i].y = 1.0;
		//cout<<tmp_rot->points[i].x<<" , "<<tmp_rot->points[i].y<<endl;
		
		//cout<<"i : "<<i<<endl;
		tmp_cloud->points[i].x = ( tmp_rot->points[i].x * cos_ ) - ( tmp_rot->points[i].y * sin_ );
		tmp_cloud->points[i].y = ( tmp_rot->points[i].x * sin_ ) + ( tmp_rot->points[i].y * cos_ );
		//cout<<tmp_rot->points[i].x<<" , "<<tmp_rot->points[i].y<<endl;

		//tmp_cloud->points[i].x = ( tmp_rot->points[i].x * cos(yaw) ) - ( tmp_rot->points[i].y * sin(yaw) );
		//tmp_cloud->points[i].y = ( tmp_rot->points[i].x * sin(yaw) ) + ( tmp_rot->points[i].y * cos(yaw) );
		
		
	}
	//cout<<tmp_rot->points[0].x<<" , "<<tmp_rot->points[0].y<<endl;

	//
	//	tmp_cloud はz軸上に無い主平面を入れてある．
	
//	tmp_cloud_ = tmp_cloud;
	cout << "*******************************************" << endl;
	
	//
	//	処理部分
	
	MatrixXd Model(3,9);
	Model<<	0.0557555,0.0557555,2.13334,1.87732,-2.16379,-8.0591,-0.0662146,8.03021,8.1825,
			-2.30219,-2.30219,0.0490737,0.0507155,-0.0502203,-0.20073,2.91887,0.191404,0.149765,
			0.0251195,0.0251195,0.0245499,0.0199583,-0.00392761,-0.0146779,-0.0132888,0.12591,0.255362;




//	MatrixXd Model(3,9);
//	Model<< -7.9606,-0.0392641,0.0964364,-2.04837,-2.23175,-1.81299,2.23153,8.13007,-0.0457593,
//			0.189565,-1.58847,3.64077,0.0604017,-0.000200413,0.0507281,-0.0534287,-0.253602,-1.57417,
//			0.00066617,0.00844743,-0.0276444,-0.00748307,-0.022527,0.0443718,0.0199546,0.175637,0.0146214;



	MatrixXd Vector(3,1);
	Vector<<0.0,
			0.0,
			0.0;

	int m_pair[(size_t)Model.cols()];//modelのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)Model.cols();i++)m_pair[i]=0;
	int q_pair[(size_t)tmp_cloud->points.size()];//queryのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++)q_pair[i]=0;
	
	//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
	
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	for(size_t i=0;i<(size_t)Model.cols();i++){
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
		if(m_pair[i] != 1){//pair[i]=1で確認済
			m_pair[i] = 1;
			
			tmp_m->points.resize (1);//変更必要
			tmp_m->points[0].x = Model(0,i);
			tmp_m->points[0].y = Model(1,i);
			tmp_m->points[0].z = Model(2,i);
			cloud_m->points.push_back(tmp_m->points[0]);//今見ている点を格納する
//			cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
			
			for(size_t j=i+1;j<(size_t)Model.cols();j++){
				double dot_m = ( Model(0,i)*Model(0,j) + Model(1,i)*Model(1,j) + Model(2,i)*Model(2,j) ) / 
							 (sqrt( Model(0,i)*Model(0,i) + Model(1,i)*Model(1,i) + Model(2,i)*Model(2,i) ) * sqrt( Model(0,j)*Model(0,j) + Model(1,j)*Model(1,j) + Model(2,j)*Model(2,j) ) );
				if( fabs(dot_m) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
					m_pair[j] = 1;
		
					tmp_m->points[0].x = Model(0,j);
					tmp_m->points[0].y = Model(1,j);
					tmp_m->points[0].z = Model(2,j);
//					cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
					
					cloud_m->points.push_back(tmp_m->points[0]);//ある角度以内に存在する点群を格納していく
				}	
			}
			
			
			
			for(size_t j=0;j<(size_t)tmp_cloud->points.size();j++){
				if(q_pair[j] != 1){//pair[j]=1で確認済
			
					double dot_q = ( Model(0,i)*tmp_cloud->points[j].x + Model(1,i)*tmp_cloud->points[j].y + Model(2,i)*tmp_cloud->points[j].z ) / 
								 (sqrt( Model(0,i)*Model(0,i) + Model(1,i)*Model(1,i) + Model(2,i)*Model(2,i) ) * sqrt( tmp_cloud->points[j].x*tmp_cloud->points[j].x + tmp_cloud->points[j].y*tmp_cloud->points[j].y + tmp_cloud->points[j].z*tmp_cloud->points[j].z ) );
					if( fabs(dot_q) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						q_pair[j] = 1;
						
						tmp_q->points.resize (1);//変更必要
						tmp_q->points[0].x = tmp_cloud->points[j].x;
						tmp_q->points[0].y = tmp_cloud->points[j].y;
						tmp_q->points[0].z = tmp_cloud->points[j].z;
//						cout<<" tmp_q : "<<tmp_q->points[0]<<endl;
				
						cloud_q->points.push_back(tmp_q->points[0]);//ある角度以内に存在する点群を格納していく
					}
				}	
			}
			
			if( cloud_q->points.size()>=3 && cloud_m->points.size()>=3 ){//取得点数が3点以上であるなら
				pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
				//icp.setInputCloud(cloud_q);//pcl1.8では廃止???
				icp.setInputSource(cloud_q);
				icp.setInputTarget(cloud_m);
				pcl::PointCloud<pcl::PointXYZ> Final;
				icp.align(Final);
//				std::cout << "has converged:" << icp.hasConverged() << " score: " <<
//				icp.getFitnessScore() << std::endl;
				Eigen::Matrix4f transformation = icp.getFinalTransformation ();//icpによって得られた変換行列をEigenに代入
//				std::cout << icp.getFinalTransformation() << std::endl;
//				std::cout << transformation << std::endl;
//				std::cout << std::endl;

				Vector(0,0) += transformation(0,3);
				Vector(1,0) += transformation(1,3);
				Vector(2,0) += transformation(2,3);
//				cout << transformation << endl;
//				cout << transformation(0,3) << endl;
				//cout << transformation(1,3) << endl;
//				cout << transformation(2,3) << endl;
				match = true;
			}else if( cloud_q->points.size()==0 || cloud_m->points.size()==0 ){
				cout<<"主平面はなし"<<endl;
				
			}else{//主平面が二点以下の場合の処理を書く
			//*********************************************************************************
			
				pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
				kdtree.setInputCloud (cloud_m);//model gaussの情報を入れておく

				pcl::PointXYZ searchPoint;//query gaussのための箱
										  //query gaussの点から近いmodel gaussの点を探索する
				//cout << "主平面が二点以下の場合" << endl;

				// K nearest neighbor search

				int K = 1;
				double sum_x,sum_y = 0.0;

				std::vector<int> pointIdxNKNSearch(K);
				std::vector<float> pointNKNSquaredDistance(K);
//
//				std::cout << "K nearest neighbor search at (" << searchPoint.x 
//					<< " " << searchPoint.y 
//					<< " " << searchPoint.z
//					<< ") with K=" << K << std::endl;
				for(size_t l = 0; l < cloud_q->points.size();l++){
					
					searchPoint.x = cloud_q->points[l].x;
					searchPoint.y = cloud_q->points[l].y;
					searchPoint.z = cloud_q->points[l].z;
					
					if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 ){
/*						for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i){
							
							
							cout << "    Δx,Δy : "  <<   cloud_m->points[ pointIdxNKNSearch[i] ].x - cloud_q->points[l].x
									<< " " << cloud_m->points[ pointIdxNKNSearch[i] ].y - cloud_q->points[l].y
									<< " " << cloud_m->points[ pointIdxNKNSearch[i] ].z - cloud_q->points[l].z
									<< endl;
							
							cout << "    Q : "  <<   cloud_q->points[l].x 
									<< " " << cloud_q->points[l].y 
									<< " " << cloud_q->points[l].z 
									<< endl;
									
							std::cout << "    M : "  <<   cloud_m->points[ pointIdxNKNSearch[i] ].x 
									<< " " << cloud_m->points[ pointIdxNKNSearch[i] ].y 
									<< " " << cloud_m->points[ pointIdxNKNSearch[i] ].z 
									<< " (squared distance: " << pointNKNSquaredDistance[i] << ")" << std::endl;

						}
*/						
						//cout<<"1"<<endl;
						//cout<<"pointIdx="<<pointIdxNKNSearch[0]<<endl;
						//cout<<"q_size = "<<cloud_q->points.size()<<endl;
						//cout<<"l:"<<l<<endl;
						sum_x += cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x;
						sum_y += cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y;
						//cout<<"pointIdx="<<pointIdxNKNSearch[0]<<endl;
					}
				}
				sum_x /= (double)cloud_q->points.size();
				sum_y /= (double)cloud_q->points.size();
				
//				cout<<sum_x<<endl;
				
				Vector(0,0) += sum_x;
				Vector(1,0) += sum_y;
				//Vector(2,0) += transformation(2,3);
			//*********************************************************************************
			}
			
			
		}
	}
	//if(match) printf("x, y, z = %1.3f ,%1.3f ,%1.3f ",Vector(0,0),Vector(1,0),Vector(2,0));
	cout << "x, y, z = " << Vector(0,0) <<" , "<< Vector(1,0) <<" , "<< Vector(2,0) << endl;
	//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
	
	//cout<<tmp_rot->points[0].x<<" , "<<tmp_rot->points[0].y<<endl;
		
//	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud, rotated_pc2_debug);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
	pcl::toROSMsg(*tmp_rot, pc2_rot);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	rotated_pc2_debug.header.frame_id = "/velodyne";//クラスタした点
	rotated_pc2_debug.header.stamp = ros::Time::now();
	pub2.publish(rotated_pc2_debug);
	rotated_pc2_debug.data.clear();
	
	pc2_rot.header.frame_id = "/velodyne";
	pc2_rot.header.stamp = ros::Time::now();
	pub2_rot.publish(pc2_rot);
	pc2_rot.data.clear();
	
	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();
/*	
	pre_pc2.header.frame_id = "/velodyne";//クラスタした点の1ステップ前のもの
	pre_pc2.header.stamp = ros::Time::now();
	pre_pub2.publish(pre_pc2);
	pre_pc2.data.clear();
*/	
//	pre_gauss = tmp_cloud_cluster;
	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
	cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	
}

void lcl_callback(nav_msgs::Odometry msg){
	lcl.pose.pose.position.x = msg.pose.pose.position.x;
	lcl.pose.pose.position.y = msg.pose.pose.position.y;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = msg.pose.pose.orientation.z;
//	cout<< lcl.pose.pose.orientation.z * 180 / M_PI <<endl;
	
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate roop(20);
	
	//ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);
	ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォルのトピック名
	/*ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	*/
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rotated_pc2_query",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	pub2_rot = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rot",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	
	ros::Publisher pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		if(DGauss_match){
			//////////////////////////
			//  位置算出するところ		//
			//////////////////////////
			
			pub_DGauss_pose.publish(Dlcl);
			DGauss_match = false;
		}	
		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
