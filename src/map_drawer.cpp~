//This template is made for .cpp
//Author: t.hagiwara
//
//pkg       : xxx
//filename  : map_drawer.cpp
//author    : x.xxx
//created   : 2016.07.28
//lastupdate: 2016.08.25

/*
起動方法
aft.csvのある場所でrosrun


//////////////////////////////////

aft.csvにおける各node前後1スキャンを含め3スキャン分を保存するsrc

topic名(publish)
	/perfect_velodyne/normal
		（/gauss_sphere/drawed_map）
		
読み込むpcd file名
	/clouds/cloud_●●.pcd



*/	

#include <stdio.h>
#include <iostream>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointSufel> pc_input;
pcl::PointCloud<pcl::PointSufel> pc_tmp;
pcl::PointCloud<pcl::PointSufel> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const size_t skip = 1;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}


void pcl2ros(pcl::PointCloud<pcl::PointSufel> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}

pcl::PointCloud<pcl::PointSufel> transformer(const pcl::PointCloud<pcl::PointSufel> &cloud_org, Eigen::Matrix4f m){
	pcl::PointCloud<pcl::PointSufel> cloud;
	cloud.points.resize(cloud_org.points.size());
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p(0) = cloud_org.points[i].x;
		p(1) = cloud_org.points[i].y;
		p(2) = cloud_org.points[i].z;
		p(3) = 1.0;
		n(0) = cloud_org.points[i].normal_x;
		n(1) = cloud_org.points[i].normal_y;
		n(2) = cloud_org.points[i].normal_z;
		n(3) = 0.0;
		p = m * p;
		n = m * n;
		cloud.points[i].x = p(0);
		cloud.points[i].y = p(1);
		cloud.points[i].z = p(2);
		cloud.points[i].normal_x = n(0);
		cloud.points[i].normal_y = n(1);
		cloud.points[i].normal_z = n(2);
		cloud.points[i].curvature = cloud_org.points[i].curvature;
	}
	return cloud;
}

Eigen::Matrix3f quat2mat(float x,float y,float z,float w){
	Eigen::Matrix3f rsl = Eigen::Matrix3f::Identity();
	rsl << 
		1-2*y*y-2*z*z,	2*x*y+2*w*z,	2*x*z-2*w*y,
		2*x*y-2*w*z,	1-2*x*x-2*z*z,	2*y*z+2*w*x,
		2*x*z+2*w*y,	2*y*z-2*w*x,	1-2*x*x-2*y*y;
	return rsl.inverse();
}

float calcdist(float node_x,float node_y,float cloud_x,float cloud_y)
{
	float dist = 0.0;
	return dist = sqrt( (node_x - cloud_x)*(node_x - cloud_x) + (node_y - cloud_y)*(node_y - cloud_y) );	
}

int main (int argc, char** argv)
{

	ros::init(argc, argv, "map_drawer"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	
	cout<<"!!!!map_drawer START!!!!"<<endl;
	cout<<"aft.csv が存在する階層にいることを確認"<<endl;
	
	/////graphファイルの読み込み
	FILE *fp;
	fp = fopen("aft.csv","r");


	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	ros::Publisher pub_drawed_map;
	
	sensor_msgs::PointCloud2 pc;
	
	
 	pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal", 1);//for gauss sphere
 	//pub_drawed_map = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/drawed_map", 1);
///////////////////////////////////////////////////////////////////////////////////////
	
	int file_num = 1;
	
	pcl::PointCloud<pcl::PointSufel>::Ptr map_cloud (new pcl::PointCloud<pcl::PointSufel>);
	pcl::PointCloud<pcl::PointSufel>::Ptr cloud_tmp (new pcl::PointCloud<pcl::PointSufel>);

	while(fscanf(fp,"%s",s) != EOF){
		float dat[8];
		int num;
		
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			dat[7] = num;
			Eigen::Matrix4f S = Eigen::Matrix4f::Identity();
			
			char filename[100];
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			//if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud_tmp) == -1){
			if (pcl::io::loadPCDFile<pcl::PointSufel> (filename, *cloud_tmp) == -1){
				cout << "cloud_" << num << "not found." << endl;
				return 0;
			}
			
			Eigen::Matrix3f kaiten = quat2mat(dat[3],dat[4],dat[5],dat[6]);
			for(int i=0;i<3;i++){
				for(int j=0;j<3;j++){
						S(i,j) = kaiten(i,j);
				}
			}
			*map_cloud = transformer(*cloud_tmp,S);
			
			cout<<"map_cloud->points.size() : "<<map_cloud->points.size()<<endl;
			
			pcl::toROSMsg(*map_cloud, pc);
			char outfile[100];
			sprintf(outfile, "drawed_map/%d.pcd", file_num);
			pcl::io::savePCDFileBinary(outfile, *map_cloud);
			cout<<"Finish saving map_cloud to : "<<outfile<<endl;
			map_cloud->points.clear();
			cloud_tmp->points.clear();
			file_num++;
			
			
		}
////////////////////////////////////////////////////////////////////		

		pc.header.frame_id = "velodyne";
		//pc.header.frame_id = "map";
	
		pc.header.stamp = ros::Time::now();
		pub_drawed_map.publish(pc);
		
	}
	
	cout<<"Finish map drawing."<<endl;
}

