//	
//	gicp_registration_rot_test.cpp
//	
//	last update 2017 02 05
//				
//	/home/amsl/test/point_normal/%d.pcd
//	の点群を受け取り
//	/perfect_velodyne/normalを傾けたものと
//	とregistrationする
//


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;
typedef pcl::PointCloud<pcl::PointNormal> CloudN;

bool callback_flag=false;

pcl::PointCloud<pcl::PointXYZ>::Ptr pc_raw (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_tf (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> output_raw;
pcl::PointCloud<pcl::PointXYZINormal> output_transfered;
sensor_msgs::PointCloud2 ros_pc2_1;
sensor_msgs::PointCloud2 ros_pc2_2;
sensor_msgs::PointCloud2 ros_pc2_3;
int unko = 1;

//pub_3 = n.advertise<sensor_msgs::PointCloud2>("perfect_velodyne/normal",1);

CloudN shift(Eigen::Matrix4f m, CloudN cloud){
	CloudN rsl;
	//	cout << "mat=" << endl <<  m << endl << endl;
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p << cloud.points[i].x, cloud.points[i].y, cloud.points[i].z, 1.0;
		n << cloud.points[i].normal_x, cloud.points[i].normal_y, cloud.points[i].normal_z, 0.0;
		p = m*p;
		n = m*n;
		pcl::PointNormal pnt;
		pnt.x = p(0);
		pnt.y = p(1);
		pnt.z = p(2);
		pnt.normal_x = n(0);
		pnt.normal_y = n(1);
		pnt.normal_z = n(2);
		pnt.curvature = cloud.points[i].curvature;
		rsl.push_back(pnt);
	}
	return rsl;
}

void pc_callback(const sensor_msgs::PointCloud2::Ptr &msg)
{
	ros_pc2_1 = *msg;
	callback_flag = true;
} 

int main (int argc, char** argv)
{
	ros::init(argc, argv, "gicp_registration_rot");
	ros::NodeHandle n;	
	ros::Rate loop(20);
	
	ros::Subscriber sub = n.subscribe("/perfect_velodyne/normal",1,pc_callback);
		
	ros::Publisher src_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/gicp_registration/src",1);
	ros::Publisher tgt_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/gicp_registration/target",1);
	ros::Publisher fnl_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/gicp_registration/final",1);
	
	//gicp parameter
	//
	pcl::GeneralizedIterativeClosestPoint<pcl::PointNormal, pcl::PointNormal> gicp; 
    //pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZINormal, pcl::PointXYZINormal> gicp; 
	
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_src (new pcl::PointCloud<pcl::PointNormal>);
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_src_tmp (new pcl::PointCloud<pcl::PointNormal>);
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_tgt (new pcl::PointCloud<pcl::PointNormal>);
	
	pcl::PointCloud<pcl::PointNormal> cloud_fnl;
	
	gicp.setMaxCorrespondenceDistance (0.5);//0.3//#ark_memo# 対応距離の最大値
	gicp.setMaximumIterations (100);        //20//#ark_memo# ICPの最大繰り返し回数
	gicp.setTransformationEpsilon (1e-8);   //#ark_memo# 変換パラメータ値
 	gicp.setEuclideanFitnessEpsilon (1e-8); //#ark_memo# ??
	
	int file_num = 1;
	char filename[100];
	sprintf(filename,"/home/amsl/test/point_normal/%d.pcd",file_num);
	
	if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud_tgt) == -1){
		cout << file_num <<".pcd not found." << endl;
		return(0);
	}
	
	pcl::toROSMsg(*cloud_tgt, ros_pc2_2);
	
	cout << "gicp_registration START !!" << endl;
	
	
	int count = 0;
	while(ros::ok()){	

		double yyy = 0.0;
		
		//ros_pcをrviz上に表示するための部分
		//
		//pcl::toROSMsg(output_raw,ros_pc);
		ros_pc2_1.header.frame_id="/velodyne";
		ros_pc2_1.header.stamp=ros::Time::now();
		src_pc_pub.publish(ros_pc2_1);
		//output_raw.points.clear();

		//pc_tfをrviz上に表示するための部分
		//
		//pcl::toROSMsg(*pc_tf,ros_pc2_2);
		ros_pc2_2.header.frame_id="/velodyne";
		ros_pc2_2.header.stamp=ros::Time::now();
		tgt_pc_pub.publish(ros_pc2_2);
		//output_raw.points.clear();
		double yaw = 10.0 * 3.141592 / 180.0;
		Eigen::Matrix4f ROT;
		ROT<< 
			cos(yaw), 0.0, -sin(yaw),  0.0,
			0.0, 1.0, 0.0, 0.0,
			sin(yaw), 0.0, cos(yaw), 0.0,
			0.0, 0.0, 0.0, 1.0;
		
		
		//	cout << "mat=" << endl <<  m << endl << endl;
		for(size_t i=0;i<cloud_src_tmp->points.size();i++){
			Eigen::Vector4f p;
			Eigen::Vector4f n;
			p << cloud_src_tmp->points[i].x, cloud_src_tmp->points[i].y, cloud_src_tmp->points[i].z, 1.0;
			n << cloud_src_tmp->points[i].normal_x, cloud_src_tmp->points[i].normal_y, cloud_src_tmp->points[i].normal_z, 0.0;
			p = ROT*p;
			n = ROT*n;
			pcl::PointNormal pnt;
			pnt.x = p(0);
			pnt.y = p(1);
			pnt.z = p(2);
			pnt.normal_x = n(0);
			pnt.normal_y = n(1);
			pnt.normal_z = n(2);
			pnt.curvature = cloud_src_tmp->points[i].curvature;
			cloud_src->push_back(pnt);
		}
		
		
		cout<<ROT<<endl;
		//modelとqueryのpointXYZを準備、ICPの定義
		//
		pcl::fromROSMsg(ros_pc2_1, *cloud_src_tmp);
		//*cloud_src = shift(ROT, *cloud_src_tmp);
		//pcl::fromROSMsg(ros_pc2_2, *cloud_tgt);
		
		//////////////処理時間の計測//////////////
		struct timeval b_sec, a_sec;
		gettimeofday(&b_sec, NULL);
		/////////////////////////////////////////
		
		if(callback_flag){
			//gicp.setInputCloud(cloud_src);            //srcのPointCloud
														//こっちだとWARNING
			gicp.setInputSource(cloud_src); 			//setInputSourceとsetInputCloudの違いは確認してない	
			gicp.setInputTarget(cloud_tgt);             //targetのPointCloud
			pcl::PointCloud<pcl::PointNormal> Final;
		
			gicp.align(Final);                			//gicpを実行し，srcの移動結果を取得
		
			Eigen::Matrix4f F;
			F = gicp.getFinalTransformation();			//gicpによって得られた変換行列をEigenに代入

			//cout<<"test"<<endl;
			cout<<"transformation : "<<endl<<F<<endl;
			cout<<"(x,y) = "<<"( "<<F(0,3)<<" , "<<F(1,3)<<" )"<<endl;	
			//cout<<yyy<<"	"<<yyy+transformation(1,3)<<"	"<<(a_sec.tv_sec - b_sec.tv_sec) + (a_sec.tv_usec - b_sec.tv_usec)*1e-6<<endl;
			
			cloud_fnl = shift(F, *cloud_src);
			
			//pc_tfをrviz上に表示するための部分
			//
			//pcl::toROSMsg(*pc_tf,ros_pc2_2);
			pcl::toROSMsg(cloud_fnl, ros_pc2_3);
			ros_pc2_3.header.frame_id="/velodyne";
			ros_pc2_3.header.stamp=ros::Time::now();
			fnl_pc_pub.publish(ros_pc2_3);
				
			callback_flag = false;
	
		}
		
		//////////////処理時間の計測//////////////
		gettimeofday(&a_sec, NULL);
		cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
		/////////////////////////////////////////
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
