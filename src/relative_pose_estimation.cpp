//
//	pose_estimation.cpp
//
//	last update 2016 / 10 / 25
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/tsukuba.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuchale_1.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_test.txt";
//const char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/building_d.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/jrm123_map.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/IKUTA_1106.txt";

//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_0914.txt";

//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/201611.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuInte1026.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/tsukuba_0922.txt";

//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_zed_0919.txt";
const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/din_20161109.txt";
//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/din_20161112.txt";

bool pc_callback_flag = false;
bool DGauss_match = true;
bool match = false;
bool dgauss_flag = false;
bool is_first = true;

const float RATIO = 0.50;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 rotated_pc2_debug;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pc2_rot;
sensor_msgs::PointCloud2 pre_pc2;
sensor_msgs::PointCloud2 pc2_model;
sensor_msgs::PointCloud2 pc2_query;
nav_msgs::Odometry lcl;
nav_msgs::Odometry lcl_ini;
nav_msgs::Odometry Dlcl;
nav_msgs::Odometry Dlcl_;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pub2_rot;
ros::Publisher pre_pub2;
ros::Publisher pub_DGauss_pose;
ros::Publisher pub_DGauss_yaw;
ros::Publisher pub_model;
ros::Publisher pub_query;


pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_1 (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_2 (new pcl::PointCloud<pcl::PointXYZ>);

const float gauss_sphere_range = 1.0;
const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]
//const float D_THRESHOLD = 2.0; //[m] model gaussとの距離の閾値
//0825
const float D_THRESHOLD = 3.0; //[m] model gaussとの距離の閾値

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

int cnt=0;
int search_gauss_num = 0;
float dist = 0.0;
vector< vector<float> > dgauss_position;
vector< vector<double> > gmodel;

pcl::PointCloud<pcl::PointXYZ>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZ>);//前ステップにおけるガウス球のための箱
//pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

float getdyaw(float ini, float azm){
	return azm - ini;
}

float calcdist(float x1, float x2, float y1, float y2){
	return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

/*
0107
処理の手順

t-1ステップの主平面とtステップの主平面を保存する
その二つを今までの処理に利用する(地図から引っ張ってきた部分をt-1ステップのものに変更する)


*/
void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
cout<<"■ ■ ■ ■ ■"<<endl;
	/*
	msgを箱に入れて
	1step前のも入れ替えて保存しておく
	*/
	
	//一番初めのループのエラー処理
	//
	if(is_first){
		pcl::fromROSMsg(*msg, *tmp_1);
		is_first = false;
		return;
	}else{
		*tmp_2 = *tmp_1;
		pcl::fromROSMsg(*msg, *tmp_1);
	}
	
	if(tmp_1->points.size()==0 || tmp_2->points.size()==0){//msgに主平面が含まれていない場合
		ROS_FATAL("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

	
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////

	double yaw = (-M_PI * 0.50) + lcl_ini.pose.pose.orientation.z + getdyaw(lcl_ini.pose.pose.orientation.z,lcl.pose.pose.orientation.z);//座標系を揃えている(?) おそらくmap座標系で考えている
	//cout << "yaw : " << yaw << " [rad] " << yaw * 180.0 / M_PI << " [deg]" <<endl;
//	cout << "lcl.pose.pose.orientation.z = " << lcl.pose.pose.orientation.z <<endl;
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_m (new pcl::PointCloud<pcl::PointXYZ>);//モデル、クエリの可視化用
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
	
//	pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_cluster (new pcl::PointCloud<pcl::PointXYZI>);

	pcl::fromROSMsg(*msg, *tmp);
	//size_t num = tmp->points.size();
	
	
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
	
	MatrixXd Vector(3,1);
	Vector<<0.0,
			0.0,
			0.0;
	
	int m_pair[(size_t)gmodel.size()];//modelのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)gmodel.size();i++)	m_pair[i]=0;
	int q_pair[(size_t)tmp_cloud->points.size()];//queryのprincipal componentの数分確保する
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++)	q_pair[i]=0;	
	
	int gcnt = tmp_1->points.size();
	size_t cnt_pair=0;//軸方向が何面存在するかのカウンタ


	

	//*********************************************************************************
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	//	2016 08 16 変更部分ここから
	//
	//*********************************************************************************
	//	gmodel[][]
	//	0はじまりのgcnt-1まで
	//	
	//	tmp_cloud->points[]
	//	tmp_cloud->points.size()まで
	//	
	//	この二つの主平面群を利用してそれぞれペアを見つけて
	//	そのペアのなす角をそれぞれ出し、ひとまず平均を求める
	//	
	Eigen::Vector3f model_vector;
	Eigen::Vector3f query_vector;
	float dot_ij[(int)gmodel.size()][3];
	float aaa[3] = {0.000,0.000,0.000};
	float theta[tmp_cloud->points.size()];
	float dyaw = 0.0;
	float theta_count = 0.0;
	
	//tmp_1,tmp_2をgmodel[][]とtmp_cloudに代入する(簡単のため)
	//	gmodel		->	tmp_1
	//	tmp_cloud	->	tmp_2
	
	gmodel.resize(tmp_1->points.size());
	for(size_t i=0;i<tmp_1->points.size();i++){
		gmodel[i].resize(3);
	}
	
	for(size_t i=0;i<tmp_1->points.size();i++){
		gmodel[i][0] = tmp_1->points[i].x;
		gmodel[i][1] = tmp_1->points[i].y;
		gmodel[i][2] = tmp_1->points[i].z;
	}
	
	*tmp_cloud = *tmp_2;
	
	
		/*
		cout<<"tmp_1 : "<<tmp_1->points.size()<<endl;
		for(size_t i=0;i<tmp_1->points.size();i++){
			cout<<"   tmp_1.x : "<<tmp_1->points[i].x<<endl;
		}
		cout<<"tmp_2 : "<<tmp_2->points.size()<<endl;
		for(size_t i=0;i<tmp_2->points.size();i++){
			cout<<"   tmp_2.x : "<<tmp_2->points[i].x<<endl;
		}
		cout<<"tmp_cloud : "<<tmp_cloud->points.size()<<endl;
		for(size_t i=0;i<tmp_cloud->points.size();i++){
			cout<<"   tmp_cloud.x : "<<tmp_cloud->points[i].x<<endl;
		}
		*/

	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		if(calcdist(tmp_cloud->points[i].x,0.0,tmp_cloud->points[i].y,0.0)>=1.0){
			for(int j=0;j<(int)gmodel.size();j++){
			
				model_vector << (gmodel[j][0])
							   ,(gmodel[j][1])
							   ,(gmodel[j][2]);
				query_vector << (tmp_cloud->points[i].x)
							   ,(tmp_cloud->points[i].y)
							   ,(tmp_cloud->points[i].z);
			
				dot_ij[j][0] = i;
				dot_ij[j][1] = j;
				dot_ij[j][2] = model_vector.dot(query_vector) / ( model_vector.norm() * query_vector.norm() );//cosΘ
				
//cout<<"( i , j ) : ( "<<i<<" , "<<j<<" )"<<endl;
			}
			cout<<"■"<<endl;
			//並び替え(大きい順に)
			//
			for(int j=0;j<(int)gmodel.size()-1;j++){
				for(int k=1;k<(int)gmodel.size();k++){
					if(dot_ij[j][2] < dot_ij[k][2]){
						aaa[0] = dot_ij[k][0];
						aaa[1] = dot_ij[k][1];
						aaa[2] = dot_ij[k][2];
					
						dot_ij[k][0] = dot_ij[j][0];
						dot_ij[k][1] = dot_ij[j][1];
						dot_ij[k][2] = dot_ij[j][2];
					
						dot_ij[j][0] = aaa[0];
						dot_ij[j][1] = aaa[1];
						dot_ij[j][2] = aaa[2];
					}
				}
			}
			
			cout<<"(int)dot_ij[0][0] : "<<(int)dot_ij[0][0]<<endl;
			cout<<"(int)dot_ij[0][1] : "<<(int)dot_ij[0][1]<<endl;
			cout<<"(int)dot_ij[0][2] : "<<(int)dot_ij[0][2]<<endl;
			
			cout<<"dot_ij[0][0] : "<<dot_ij[0][0]<<endl;
			cout<<"dot_ij[0][1] : "<<dot_ij[0][1]<<endl;
			cout<<"dot_ij[0][2] : "<<dot_ij[0][2]<<endl;
			
			int box = dot_ij[0][1];
			
			//ここにi,jがはいっている
			//dot_ij[0][0] = i;
			//dot_ij[0][1] = j;
			cout<<"■　■"<<endl;
			float dist_mq;
			float cross = (tmp_cloud->points[dot_ij[0][0]].x * gmodel[dot_ij[0][1]][1]) - (gmodel[dot_ij[0][1]][0] * tmp_cloud->points[dot_ij[0][0]].y);
			float norm = sqrt( (tmp_cloud->points[dot_ij[0][0]].x)*(tmp_cloud->points[dot_ij[0][0]].x) + (tmp_cloud->points[dot_ij[0][0]].y)*(tmp_cloud->points[dot_ij[0][0]].y) )
							* sqrt( (gmodel[dot_ij[0][1]][0])*(gmodel[dot_ij[0][1]][0]) + (gmodel[dot_ij[0][1]][1])*(gmodel[dot_ij[0][1]][1]));
			
			//
			//
			//float cross = (tmp_cloud->points[dot_ij[0][0]].x * gmodel[dot_ij[0][1]][1]) - (gmodel[dot_ij[0][1]][0] * tmp_cloud->points[dot_ij[0][0]].y);
			//float norm = sqrt( (tmp_cloud->points[dot_ij[0][0]].x)*(tmp_cloud->points[dot_ij[0][0]].x) + (tmp_cloud->points[dot_ij[0][0]].y)*(tmp_cloud->points[dot_ij[0][0]].y) )
			//				* sqrt( (gmodel[dot_ij[0][1]][0])*(gmodel[dot_ij[0][1]][0]) + (gmodel[dot_ij[0][1]][1])*(gmodel[dot_ij[0][1]][1]));
			
			theta[i] = asin(cross/norm);
			
			cout<<"dot_ij[0][0] : "<<dot_ij[0][0]<<endl;
			cout<<"dot_ij[0][1] : "<<dot_ij[0][1]<<endl;
			cout<<"dot_ij[0][2] : "<<dot_ij[0][2]<<endl;
			
			dot_ij[0][1] = box;
			cout<<"      "<<endl;
			cout<<"dot_ij[0][0] : "<<dot_ij[0][0]<<endl;
			cout<<"dot_ij[0][1] : "<<dot_ij[0][1]<<endl;
			cout<<"dot_ij[0][2] : "<<dot_ij[0][2]<<endl;
			
			dist_mq = calcdist(tmp_cloud->points[dot_ij[0][0]].x ,gmodel[dot_ij[0][1]][0] ,tmp_cloud->points[dot_ij[0][0]].y ,gmodel[dot_ij[0][1]][1] );
			
			if( (abs(theta[i])<=0.1736) && (dot_ij[0][2]>=0.9848) && dist_mq<=3.0){//10[deg]以下なら
			//if( (abs(theta[i])<=0.261799) && (dot_ij[0][2]>=0.9659258) && dist_mq<=3.0){//15[deg]以下なら
				dyaw += theta[i];
				theta_count++;

			}
			
		}
		
	}
	//dyaw /= tmp_cloud->points.size();
	dyaw /= theta_count;//前ステップと今のステップでの主平面の対応点の数
	if(theta_count == 0.0 || dyaw == 0.000){
		cout<<"theta_count : 0"<<endl;
		Dlcl_.pose.pose.orientation.w = 0.0;//flagとして使った
	}else{
		Dlcl_.pose.pose.orientation.w = theta_count;
	}
	Dlcl_.pose.pose.orientation.z = dyaw;// - 0.03338629;
	pub_DGauss_yaw.publish(Dlcl_);
	
	if(isnan(dyaw)){
//0905		ROS_FATAL("■■■■■ NAN IS INCLUDED IN THE VALUE ■■■■■");
	}else{
		
	
	}
	
	cout<<" dyaw[rad] : "<<dyaw<<endl;
	cout<<" dyaw[deg] : "<<dyaw * 180.0 / M_PI<<endl;
	return;
	//*********************************************************************************
	//
	//20170115 search_gauss_numの定義の部分を消しているのでここは有効ではない
/*	
	for(size_t i=0;i<(size_t)gmodel.size();i++){
			
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
		if(m_pair[i] != 1){//pair[i]=1で確認済
			m_pair[i] = 1;
			
			tmp_m->points.resize (1);//変更必要
			tmp_m->points[0].x = gmodel[i][0];//Model(0,i);
			tmp_m->points[0].y = gmodel[i][1];//Model(1,i);
			tmp_m->points[0].z = gmodel[i][2];//Model(2,i);
			cloud_m->points.push_back(tmp_m->points[0]);//今見ている点を格納する
//			cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
			
			for(size_t j=i+1;j<(size_t)gmodel.size();j++){
				if(m_pair[j] != 1){
					double dot_m = ( gmodel[i][0]*gmodel[j][0] + gmodel[i][1]*gmodel[j][1] //+ gmodel[i][2]*gmodel[j][2]
					 ) / 
								 (sqrt( gmodel[i][0]*gmodel[i][0] + gmodel[i][1]*gmodel[i][1] //+ gmodel[i][2]*gmodel[i][2]
					 ) * sqrt( gmodel[j][0]*gmodel[j][0] + gmodel[j][1]*gmodel[j][1] //+ gmodel[j][2]*gmodel[j][2]
					  ) );
					if( fabs(dot_m) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						m_pair[j] = 1;
		
						tmp_m->points[0].x = gmodel[j][0];//Model(0,j);
						tmp_m->points[0].y = gmodel[j][1];//Model(1,j);
						tmp_m->points[0].z = gmodel[j][2];//Model(2,j);
						//cout<<"tmp_m : "<<tmp_m->points[0]<<endl;
					
						cloud_m->points.push_back(tmp_m->points[0]);//ある角度以内に存在する点群を格納していく
					}
				}
			}
			
			for(size_t j=0;j<(size_t)tmp_cloud->points.size();j++){
				if(q_pair[j] != 1){//pair[j]=1で確認済
			
					double dot_q = ( gmodel[i][0]*tmp_cloud->points[j].x + gmodel[i][1]*tmp_cloud->points[j].y //+ gmodel[i][2]*tmp_cloud->points[j].z
					 ) / 
								 (sqrt( gmodel[i][0]*gmodel[i][0] + gmodel[i][1]*gmodel[i][1] //+ gmodel[i][2]*gmodel[i][2]
					 ) * sqrt( tmp_cloud->points[j].x*tmp_cloud->points[j].x + tmp_cloud->points[j].y*tmp_cloud->points[j].y //+ tmp_cloud->points[j].z*tmp_cloud->points[j].z
					  ) );
					
					//if( fabs(dot_q) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
					if( fabs(dot_q) >= 0.95 ){//≒18[deg]成す角が18deg以内であるならば
						q_pair[j] = 1;
						
						tmp_q->points.resize (1);//変更必要
						tmp_q->points[0].x = tmp_cloud->points[j].x;
						tmp_q->points[0].y = tmp_cloud->points[j].y;
						tmp_q->points[0].z = tmp_cloud->points[j].z;
						//cout<<" tmp_q : "<<tmp_q->points[0]<<endl;
						
						//	ロボット座標の原点1m以内に存在する主平面を除く
						//
						if(calcdist(tmp_q->points[0].x,0.0,tmp_q->points[0].y,0.0)>=1.0){
							cloud_q->points.push_back(tmp_q->points[0]);//ある角度以内に存在する点群を格納していく
						}
					}
				}	
			}
			//	ここまででcloud_m,cloud_qにはモデルとクエリの点群が入っているので
			//	片方を平行移動させる
			
			//自己位置 - gauss球配置位置
			//lcl.pose.pose.position.x,lcl.pose.pose.position.y
			// -
			//dgauss_position[search_gauss_num-1][0]
			
			Eigen::Vector3f vector_mq;//model -> query のベクトル
			Eigen::Vector3f vector_x;
			Eigen::Vector3f shift;
cout<<"■ ■ ■ ■ ■"<<endl;
//search_gauss_numが有効になってないため注意
			vector_mq << lcl.pose.pose.position.x - dgauss_position[search_gauss_num-1][0]
						,lcl.pose.pose.position.x - dgauss_position[search_gauss_num-1][0]
						,0.0;
			for(int h=0;h<(int)cloud_m->points.size();h++){
				vector_x << cloud_m->points[h].x , cloud_m->points[h].y , 0.0;
				float dot = vector_mq.dot(vector_x) / ( vector_mq.norm() * vector_x.norm() );//cosΘ;
				if(dot>0.0){
					shift = vector_mq.norm() * dot * vector_x.normalized();
				}
			}
			for(int h=0;h<(int)cloud_q->points.size();h++){
				pub_cloud_q->points.push_back(cloud_q->points[h]);
			}
			if( cloud_q->points.size()>=3 && cloud_m->points.size()>=3 ){//取得点数が3点以上であるなら
				cout<<"---ICP---"<<endl;
				pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
				//icp.setInputCloud(cloud_q);//pcl1.8では廃止???
				icp.setInputSource(cloud_q);
				icp.setInputTarget(cloud_m);
				pcl::PointCloud<pcl::PointXYZ> Final;
				icp.align(Final);

				Eigen::Matrix4f transformation = icp.getFinalTransformation ();//icpによって得られた変換行列をEigenに代入

				Vector(0,0) += transformation(0,3);
				Vector(1,0) += transformation(1,3);
				Vector(2,0) += transformation(2,3);
				
				cout << "	x : " << transformation(0,3) << endl;
				cout << "	y : " << transformation(1,3) << endl;
				cnt_pair++;
				match = true;
			}else if( cloud_q->points.size()==0 || cloud_m->points.size()==0 ){
				//if(cloud_m->points.size()==0){
				if(gcnt==0){
					cout<<"modelの主平面なし"<<endl;
				}else if(cloud_q->points.size()==0){
					cout<<"queryの主平面なし"<<endl;
				}
				return;
			}else if( cloud_q->points.size() <= cloud_m->points.size()){//モデルの主平面が多い場合
				
				
				cout<<"---NN---  m>q"<<endl;
				pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
				kdtree.setInputCloud (cloud_m);//model gaussの情報を入れておく
				
				pcl::PointXYZ searchPoint;//query gaussのための箱
										  //query gaussの点から近いmodel gaussの点を探索する
				//cout << "主平面が二点以下の場合" << endl;
				
				// K nearest neighbor search
				
				int K = 1;
				int count_q = 0;//近傍点と対応付けして求めた変位ベクトルがある値以下であればカウントする
				double sum_x = 0.0;
				double sum_y = 0.0;
				double theta_d = 0.0;//modelのgaussの主平面のmodel接地点からのベクトルと変位ベクトルのなす角
				double dist_1,dist_2 = 0.0;	//dist_1 : modelのgaussの主平面のmodel接地点からのベクトルの大きさ
											//dist_2 : 変位ベクトルの大きさ
				
				std::vector<int> pointIdxNKNSearch(K);
				std::vector<float> pointNKNSquaredDistance(K);
//				
//				std::cout << "K nearest neighbor search at (" << searchPoint.x 
//					<< " " << searchPoint.y 
//					<< " " << searchPoint.z
//					<< ") with K=" << K << std::endl;
				for(size_t l = 0; l < cloud_q->points.size();l++){
					
					searchPoint.x = cloud_q->points[l].x;
					searchPoint.y = cloud_q->points[l].y;
					searchPoint.z = cloud_q->points[l].z;
				
					if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 ){
						
						//1104変更***
						//
						//d[] = cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x;
						//なので
						//dprincipalとthetaを算出する
						//その後でdprincipal vector上の変位量を計算しsum_x,sum_yにそれぞれ代入する
						//
						//dprincipal[0] = cloud_m->points[ pointIdxNKNSearch[0] ].x
						//dprincipal[1] = cloud_m->points[ pointIdxNKNSearch[0] ].y
						dist_1 = sqrt( (cloud_m->points[ pointIdxNKNSearch[0] ].x * cloud_m->points[ pointIdxNKNSearch[0] ].x) + (cloud_m->points[ pointIdxNKNSearch[0] ].y * cloud_m->points[ pointIdxNKNSearch[0] ].y) );
						dist_2 = sqrt( 
										(cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x)*(cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x)
									  + (cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y)*(cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y)
								 );
						//theta_d = ( cloud_m->points[ pointIdxNKNSearch[0] ].x*cloud_q->points[l].x + cloud_m->points[ pointIdxNKNSearch[0] ].y*cloud_q->points[l].y ) / ( dist_1 * dist_2 );
						theta_d = acos( ( cloud_m->points[ pointIdxNKNSearch[0] ].x*(cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x) + cloud_m->points[ pointIdxNKNSearch[0] ].y*(cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y) ) / ( dist_1 * dist_2 ) );
						
						cout<<"dist1 : "<<dist_1<<" dist2 : "<<dist_2<<" theta_d : "<<theta_d<<endl;
						
						
						if(dist_2 <= (D_THRESHOLD*0.5)){
							
							
							sum_x += (cloud_m->points[ pointIdxNKNSearch[0] ].x / dist_1) * dist_2 * cos(theta_d);
							sum_y += (cloud_m->points[ pointIdxNKNSearch[0] ].y / dist_1) * dist_2 * cos(theta_d);
							cout<<"sum_x : "<<sum_x<<"sum_y : "<<sum_y<<endl;
							
								//theta_d = acos( cloud_q->points[l].x / calcdist(cloud_q->points[l].x,0.0,cloud_q->points[l].y,0.0) );
							
								//sum_x += (cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x) * cos(theta_d);//1105comment out
								//sum_y += (cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y) * cos(theta_d);//1105comment out
							
							
							//sum_x += cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x;//1105comment out
							//sum_y += cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y;//1105comment out
							
							
							count_q ++;
						}
						//
						//
						//
						//1104変更ここまで***
						
						//sum_x += cloud_m->points[ pointIdxNKNSearch[0] ].x - cloud_q->points[l].x;//1105comment out
						//sum_y += cloud_m->points[ pointIdxNKNSearch[0] ].y - cloud_q->points[l].y;//1105comment out
						//cout<<"pointIdx="<<pointIdxNKNSearch[0]<<endl;
					}
				}
				
				cout<<"count_q : "<<count_q<<endl;
				//sum_x /= (double)cloud_q->points.size();
				//sum_y /= (double)cloud_q->points.size();
				if(count_q == 0){
					cout<<"RETURN"<<endl;
					return;
				}
					
				sum_x /= count_q;
				sum_y /= count_q;
				
				Vector(0,0) += sum_x;
				Vector(1,0) += sum_y;
				//Vector(2,0) += transformation(2,3);
				
				//cout << " x : " << Vector(0,0) << endl;
				//cout << " y : " << Vector(1,0) << endl;
				cout << "	x : " << sum_x << endl;
				cout << "	y : " << sum_y << endl;
				cnt_pair++;
				
				
			// *********************************************************************************
			}else if( cloud_q->points.size() >= cloud_m->points.size()){//クエリの主平面が多い場合
				
				cout<<"---NN---  q>m"<<endl;
				pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
				kdtree.setInputCloud (cloud_q);//query modelの情報を入れておく
				
				pcl::PointXYZ searchPoint;//model gaussのための箱
										  //model gaussの点から近いquery gaussの点を探索する
				// K nearest neighbor search
				
				int K = 1;
				int count_q = 0;//近傍点と対応付けして求めた変位ベクトルがある値以下であればカウントする
				double sum_x = 0.0;
				double sum_y = 0.0;
				double theta_d = 0.0;//modelのgaussの主平面のmodel接地点からのベクトルと変位ベクトルのなす角
				double dist_1,dist_2 = 0.0;	//dist_1 : modelのgaussの主平面のmodel接地点からのベクトルの大きさ
											//dist_2 : 変位ベクトルの大きさ
				
				std::vector<int> pointIdxNKNSearch(K);
				std::vector<float> pointNKNSquaredDistance(K);

				for(size_t l = 0; l < cloud_m->points.size();l++){
					
					searchPoint.x = cloud_m->points[l].x;
					searchPoint.y = cloud_m->points[l].y;
					searchPoint.z = cloud_m->points[l].z;
				
					if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 ){
						
						dist_1 = sqrt( (cloud_q->points[ pointIdxNKNSearch[0] ].x * cloud_q->points[ pointIdxNKNSearch[0] ].x) + (cloud_q->points[ pointIdxNKNSearch[0] ].y * cloud_q->points[ pointIdxNKNSearch[0] ].y) );
						dist_2 = sqrt( 
										(cloud_q->points[ pointIdxNKNSearch[0] ].x - cloud_m->points[l].x)*(cloud_q->points[ pointIdxNKNSearch[0] ].x - cloud_m->points[l].x)
									  + (cloud_q->points[ pointIdxNKNSearch[0] ].y - cloud_m->points[l].y)*(cloud_q->points[ pointIdxNKNSearch[0] ].y - cloud_m->points[l].y)
								 );
						//theta_d = ( cloud_m->points[ pointIdxNKNSearch[0] ].x*cloud_q->points[l].x + cloud_m->points[ pointIdxNKNSearch[0] ].y*cloud_q->points[l].y ) / ( dist_1 * dist_2 );
						theta_d = acos( ( cloud_q->points[ pointIdxNKNSearch[0] ].x*(cloud_q->points[ pointIdxNKNSearch[0] ].x - cloud_m->points[l].x) + cloud_q->points[ pointIdxNKNSearch[0] ].y*(cloud_q->points[ pointIdxNKNSearch[0] ].y - cloud_m->points[l].y) ) / ( dist_1 * dist_2 ) );
						
						if(dist_2 <= (D_THRESHOLD*0.5)){
							
							sum_x += (cloud_q->points[ pointIdxNKNSearch[0] ].x / dist_1) * dist_2 * cos(theta_d);
							sum_y += (cloud_q->points[ pointIdxNKNSearch[0] ].y / dist_1) * dist_2 * cos(theta_d);
							count_q ++;
						}
					}
				}
				sum_x /= count_q;
				sum_y /= count_q;
				
				Vector(0,0) -= sum_x;
				Vector(1,0) -= sum_y;
				//Vector(2,0) += transformation(2,3);
				
				//cout << " x : " << Vector(0,0) << endl;
				//cout << " y : " << Vector(1,0) << endl;
				cout << "	x : " << sum_x << endl;
				cout << "	y : " << sum_y << endl;
				cnt_pair++;
				
			}
			
		}
	}
*/
cout<<"■"<<endl;	
	//	debug用にモデルとクエリを可視化する
	//
	
	pcl::toROSMsg(*pub_cloud_m, pc2_model);
	pcl::toROSMsg(*pub_cloud_q, pc2_query);
cout<<"■■"<<endl;		
	pc2_query.header.frame_id = "/velodyne";
	pc2_query.header.stamp = ros::Time::now();
	pub_query.publish(pc2_query);
	pc2_query.data.clear();
cout<<"■■■"<<endl;		
	pc2_model.header.frame_id = "/velodyne";
	pc2_model.header.stamp = ros::Time::now();
	pub_model.publish(pc2_model);
	pc2_model.data.clear();
cout<<"■■■■"<<endl;		
	//cout<<"cnt_pair : "<<cnt_pair<<endl;


	if(cnt_pair == 0){
		//cout<<"--  NO PAIR  --"<<endl;
		return;
	}else if(cnt_pair == 1){
cout<<"■■■■■"<<endl;	
		//cout<<" !!!!! cnt_pair = 1 !!!!! "<<endl;
		double R[2],M[2],r[2],d[2],theta,abs_d,abs_r;
		//R : subscribeした自己位置の座標(/map上)
		R[0] = lcl_ini.pose.pose.position.x;
		R[1] = lcl_ini.pose.pose.position.y;
//0905		cout<<"R[0] : "<<R[0]<<"  R[1] : "<<R[1]<<endl;
		//M : model gaussの設置位置(/map上)
cout<<"■■■■■■"<<endl;
		cout<<"search_gauss_num : "<<search_gauss_num<<endl;
		M[0] = dgauss_position[search_gauss_num-1][0];
		M[1] = dgauss_position[search_gauss_num-1][1];
cout<<"■■■■■■■"<<endl;	
//0905		cout<<"M[0] : "<<M[0]<<"  M[1] : "<<M[1]<<endl;
		//r = R - M
		r[0] = R[0] - M[0];
		r[1] = R[1] - M[1];
		//d = Vec
		d[0] = Vector(0,0);
		d[1] = Vector(1,0);
		
		abs_d = sqrt( d[0]*d[0] + d[1]*d[1] );
		abs_r = sqrt( r[0]*r[0] + r[1]*r[1] );
		theta = acos( ( d[0]*r[0] + d[1]*r[1] ) / ( sqrt( abs_d ) * sqrt( abs_r ) ) );
		
		Vector(0,0) = r[0] + ( d[0] * ( 1.0 - ( (abs_r / abs_d) * cos(theta) ) ) );
		Vector(1,0) = r[1] + ( d[1] * ( 1.0 - ( (abs_r / abs_d) * cos(theta) ) ) );
		cout<<"Vector(0,0) : "<<Vector(0,0)<<" Vector(1,0) : "<<Vector(1,0)<<endl;
		return;//1010
	}

cout<<"■■■■■■■■"<<endl;		
	
	//if(match) printf("x, y, z = %1.3f ,%1.3f ,%1.3f ",Vector(0,0),Vector(1,0),Vector(2,0));
//0905	cout<<"■■■■■■■■■■■■■■■■■■■■"<<endl;
	cout << " x : " << Vector(0,0) << endl;
	cout << " y : " << Vector(1,0) << endl;
//0905	cout << " z : " << Vector(2,0) << endl;
	
	if(calcdist(0.0,Vector(0,0),0.0,Vector(1,0))>=D_THRESHOLD){
		ROS_FATAL("!!!!! MATCHING RESULT SEEMS WRONG !!!!!!");
		return;
	}
	
	if(isnan(Vector(0,0)) || isnan(Vector(1,0)) ){
		ROS_FATAL("■■■■■ NAN IS INCLUDED IN THE VALUE ■■■■■");
		return;
	}else{
				}
	//	
	//	matching しなかったときの対応(flagを使う)
	//	
	
	//0825
	//Dlcl.pose.pose.position.x = Vector(0,0);//localization_testにおいて変位量だけを受け取りたいため//0927
	//Dlcl.pose.pose.position.y = Vector(1,0);
	
	/*
	double estimated_pose_x = 0.0;
	double estimated_pose_y = 0.0;
	estimated_pose_x = Vector(0,0) + dgauss_position[search_gauss_num-1][0];
	estimated_pose_y = Vector(1,0) + dgauss_position[search_gauss_num-1][1];
	Dlcl.pose.pose.position.x = estimated_pose_x - lcl.pose.pose.position.x;//localization_testにおいて変位量だけを受け取りたいため//0927
	Dlcl.pose.pose.position.y = estimated_pose_y - lcl.pose.pose.position.y;
	*/
cout<<"■■■■■■■■■"<<endl;		
	Dlcl.pose.pose.position.x = Vector(0,0) + dgauss_position[search_gauss_num-1][0];//txtから読み込んだ際は0始まり，
	Dlcl.pose.pose.position.y = Vector(1,0) + dgauss_position[search_gauss_num-1][1];
	//Dlcl.pose.pose.orientation.z = dyaw;
	
	cout<<"Dlcl.x : "<<Dlcl.pose.pose.position.x<<endl;
	cout<<"Dlcl.y : "<<Dlcl.pose.pose.position.y<<endl;
	
	pub_DGauss_pose.publish(Dlcl);
	
//	cout<<cluster_number<<endl;
	pcl::toROSMsg(*tmp_cloud, rotated_pc2_debug);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
//	pcl::toROSMsg(*tmp_rot, pc2_rot);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	rotated_pc2_debug.header.frame_id = "/velodyne";//クラスタした点
	rotated_pc2_debug.header.stamp = ros::Time::now();
	pub2.publish(rotated_pc2_debug);
	rotated_pc2_debug.data.clear();

	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();

	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
//0905	cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	dgauss_flag=false;
}

void lcl_callback(nav_msgs::Odometry msg){
	lcl.pose.pose.position.x = msg.pose.pose.position.x;
	lcl.pose.pose.position.y = msg.pose.pose.position.y;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = msg.pose.pose.orientation.z;
//	cout<< lcl.pose.pose.orientation.z * 180 / M_PI <<endl;
	
}

void lcl_ini_callback(nav_msgs::Odometry msg){
	lcl_ini.pose.pose.position.x = msg.pose.pose.position.x;
	lcl_ini.pose.pose.position.	y = msg.pose.pose.position.y;
	lcl_ini.pose.pose.orientation.z = msg.pose.pose.orientation.z;
	
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "relative_pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate loop(20);
	
	//ros::Subscriber sub = n.subscribe("/perfect_velodyne/principal_components",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);//for perfect velodyne pose_estimation//1105
	//ros::Subscriber sub2 = n.subscribe("/gauss_sphere_depth_clustered",1,pc2_callback);//for velodyne_2times & normal_estimation pkg
	//ros::Subscriber sub2 = n.subscribe("/gauss_sphere_clustered",1,pc2_callback);
	
	ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	//ros::Subscriber sub_lcl = n.subscribe("/ekf_DgaussAndNDT",1,lcl_callback);//1013comment out
	
	ros::Subscriber sub_lcl_ini = n.subscribe("/lcl/initial_pose",1,lcl_ini_callback);
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォルのトピック名
	/*ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	*/
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rotated_pc2_query",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	//pub2_rot = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rot",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	pub_DGauss_yaw = n.advertise<nav_msgs::Odometry>("/gauss_sphere/yaw", 10);
	
	pub_model = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/model",1);
	pub_query = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/query",1);
	
	//////////////////////////////////////////////////////////////////
	//	Dgaussを Dgauss_list/ikuta.txtから読み取る
	//	txt内は
	//	
	//	x , y <- /map でのDgaussの位置を保存していく
	//	
	//
	//////////////////////////////////////////////////////////////////
	
	FILE* fp;

	dgauss_position.resize(10000);
	for(int i=0;i<10000;i++){
		dgauss_position[i].resize(2);
	}
	
	if( (fp = fopen(file_name,"r")) == NULL){
		cout << "Dgauss_list の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}else{
		float a,b = 0.0;
		while(fscanf(fp, "%f,%f", &a, &b) != EOF) {	
			dgauss_position[cnt][0] = a;
			dgauss_position[cnt][1] = b;
			cout<<dgauss_position[cnt][0]<<"  "<<dgauss_position[cnt][1]<<endl;
			
			/*
			visualization_msgs::Marker marker;
			marker.header.frame_id = "map";
			marker.header.stamp = ros::Time();
			marker.ns = "dgauss_point";
			marker.id = cnt;
			marker.type = visualization_msgs::Marker::SPHERE;
			marker.action = visualization_msgs::Marker::ADD;
			marker.pose.position.x = a;
			marker.pose.position.y = b;
			marker.pose.position.z = 0;
			marker.pose.orientation.x = 0.0;
			marker.pose.orientation.y = 0.0;
			marker.pose.orientation.z = 0.0;
			marker.pose.orientation.w = 0.0;
			marker.scale.x = 1;
			marker.scale.y = 0.1;
			marker.scale.z = 0.1;
			marker.color.a = 1.0; // Don't forget to set the alpha!
			marker.color.r = 0.0;
			marker.color.g = 1.0;
			marker.color.b = 0.0;
			vis_pub.publish( marker );
			
			
			*/
			cnt++;
		}
		dgauss_position.resize(cnt);
	}
	fclose(fp);
	
	cout << "Pose Estimation START !!" << endl;
	
	while(ros::ok()){
		
		//const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_test.txt";
		
		//FILE* fp2;
		//char file_name_1[100];
		//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",search_gauss_num);
		//sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_test/%d.txt",search_gauss_num);
	
		
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
