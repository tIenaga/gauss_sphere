//
//	pose_estimation.cpp
//
//	last update 2016 / 10 / 25
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

vector< vector<float> > dgauss_position;
vector< vector<double> > g_model;

nav_msgs::Odometry lcl;
nav_msgs::Odometry lcl_init;

int cnt = 1;

int n = 0;
float last_angle;

// const char* file_name = "/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_zed_0919.txt";
// const char* file_name_list = "/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_robosym2016.txt";
const char* file_name_list = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta_robosym2016_kari_new.txt";

ros::Publisher pub_Dgauss_yaw;

ros::Publisher pub_query_mk_a;
ros::Publisher pub_model_mk_a;
ros::Publisher pub_vis_query;
ros::Publisher pub_vis_query2;
ros::Publisher pub_vis_model;
ros::Publisher pub_vis_model2;

void visualization_for_pairing(pcl::PointCloud<pcl::PointXYZ>::Ptr query, pcl::PointCloud<pcl::PointXYZ>::Ptr model){
	visualization_msgs::MarkerArray query_mk_a;	
	visualization_msgs::MarkerArray model_mk_a;
	
	sensor_msgs::PointCloud2 vis_query;
	pcl::toROSMsg(*query, vis_query);
	vis_query.header.frame_id = "/map";
	vis_query.header.stamp = ros::Time::now();
	pub_vis_query.publish(vis_query);
	
	sensor_msgs::PointCloud2 vis_model;
	pcl::toROSMsg(*model, vis_model);
	vis_model.header.frame_id = "/map";
	vis_model.header.stamp = ros::Time::now();
	pub_vis_model.publish(vis_model);


	query_mk_a.markers.resize(query->points.size());
	for(size_t i = 0; i < query->points.size(); i++){
		query_mk_a.markers[i].ns = "namespace";
		query_mk_a.markers[i].id = i;
		query_mk_a.markers[i].type = visualization_msgs::Marker::TEXT_VIEW_FACING;
		query_mk_a.markers[i].action = visualization_msgs::Marker::ADD;
		query_mk_a.markers[i].color.a = 1.0;
		query_mk_a.markers[i].color.r = 0.0;
		query_mk_a.markers[i].color.g = 1.0;
		query_mk_a.markers[i].color.b = 1.0;
		query_mk_a.markers[i].scale.z = 0.75;
		// query_mk_a.markers[i].lifetime = ros::Duration(0.1);
		query_mk_a.markers[i].header.frame_id = "/map";
		
		char index[100];
		sprintf(index, "%3d", (int) i);
		query_mk_a.markers[i].text = index;

		query_mk_a.markers[i].pose.position.x = query->points[i].x;
		query_mk_a.markers[i].pose.position.y = query->points[i].y;
		query_mk_a.markers[i].pose.position.z = query->points[i].z + 0.5;
	}

	pub_query_mk_a.publish(query_mk_a);

	
	model_mk_a.markers.resize(model->points.size());
	for(size_t i = 0; i < model->points.size(); i++){
		model_mk_a.markers[i].ns = "namespace";
		model_mk_a.markers[i].id = i;
		model_mk_a.markers[i].type = visualization_msgs::Marker::TEXT_VIEW_FACING;
		model_mk_a.markers[i].action = visualization_msgs::Marker::ADD;
		model_mk_a.markers[i].color.a = 1.0;
		model_mk_a.markers[i].color.r = 1.0;
		model_mk_a.markers[i].color.g = 1.0;
		model_mk_a.markers[i].color.b = 0.0;
		model_mk_a.markers[i].scale.z = 0.75;
		// model_mk_a.markers[i].lifetime = ros::Duration(0.1);
		model_mk_a.markers[i].header.frame_id = "/map";
		
		char index[100];
		sprintf(index, "%3d", (int) i);
		model_mk_a.markers[i].text = index;

		model_mk_a.markers[i].pose.position.x = model->points[i].x;
		model_mk_a.markers[i].pose.position.y = model->points[i].y;
		model_mk_a.markers[i].pose.position.z = model->points[i].z + 0.5;
	}
	
	pub_model_mk_a.publish(model_mk_a);

}

float calc_dist(float x1, float x2, float y1, float y2){
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1- y2));
}

int search_nearest_model_num(nav_msgs::Odometry lcl_){
	int index = 0;
	float dist;
	float temp_min = dist = calc_dist(lcl_.pose.pose.position.x, dgauss_position[0][0], lcl_.pose.pose.position.y, dgauss_position[0][1]);
	
	// cout<<"lcl_.pose.pose.position.x : "<<lcl_.pose.pose.position.x<<endl;
	// cout<<"lcl_.pose.pose.position.y : "<<lcl_.pose.pose.position.y<<endl;
	// cout<<"lcl_.pose.pose.position.z : "<<lcl_.pose.pose.position.z<<endl;
	// cout<<"lcl_.pose.pose.orientation.z : "<<lcl_.pose.pose.orientation.z<<endl;

	size_t dgauss_position_size = dgauss_position.size();
	for(size_t i = 0; i < dgauss_position_size;i++){
		dist = calc_dist(lcl_.pose.pose.position.x, dgauss_position[i][0], lcl_.pose.pose.position.y, dgauss_position[i][1]);
		
		if(dist < temp_min){
			temp_min = dist;
			index = i;
		}
	}

	return index;
}

void get_model(int search_gauss_num, pcl::PointCloud<pcl::PointXYZ>::Ptr model_cloud){

	FILE* fp2;
	char file_name_pose[1024];
	// sprintf(file_name_pose, "/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_robosym2016/%d.txt", search_gauss_num);
	sprintf(file_name_pose, "/home/amsl/infant_AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta_robosym2016_kari/%d.txt", search_gauss_num);
	cout<<"file_name_pose : "<<file_name_pose<<endl;
	
	if((fp2=fopen(file_name_pose,"r")) == NULL){
		cout << "Dgauss の主平面情報の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		return;
		// exit(1);
	}
	else{
		float a,b,c = 0.0;
		while(fscanf(fp2, "%f, %f, %f", &a, &b, &c) != EOF){
			pcl::PointXYZ temp_point;
			temp_point.x = a;
			temp_point.y = b;
			temp_point.z = c;

			model_cloud->points.push_back(temp_point);
		}
	}
	fclose(fp2);
	// cout<<"model_cloud->points.size() : "<<model_cloud->points.size()<<endl;
}

float calc_simirality(pcl::PointXYZ query_point, pcl::PointXYZ model_point){
	float dot = query_point.x * model_point.x + query_point.y * model_point.y + query_point.z * model_point.z;
	// float query_norm = sqrt(query_point.x * query_point.x + query_point.y * query_point.y + query_point.z * query_point.z);
	float query_norm = calc_dist(query_point.x, 0.0, query_point.y, 0.0);
	// float model_norm = sqrt(model_point.x * model_point.x + model_point.y * model_point.y + model_point.z * model_point.z);
	float model_norm = calc_dist(model_point.x, 0.0, model_point.y, 0.0);

	return dot / (query_norm * model_norm);
}

void transform(pcl::PointCloud<pcl::PointXYZ>::Ptr pc_in, pcl::PointCloud<pcl::PointXYZ>::Ptr pc_out, float x, float y, float z, float theta){
	// cout<<"theta : "<<theta<<endl;
	
	Eigen::Translation<float, 3> trans(x, y, z);	// 平行移動

	Eigen::Matrix3f rot;
	rot = AngleAxisf(theta, Vector3f::UnitZ());		// Vector3f::UnitZ()軸周りにtheta回転(半時計回り正)

	// cout<<rot<<endl;
	
	Eigen::Affine3f affine = trans * rot;

	// cout<<affine.matrix()<<endl;

	size_t cloud_size = pc_in->points.size();
	for(size_t i = 0; i < cloud_size; i++){
		Eigen::Vector3f point_before(pc_in->points[i].x, pc_in->points[i].y, pc_in->points[i].z);
		
		Eigen::Vector3f point_after;
		point_after = affine * point_before;

		pcl::PointXYZ point_after_transform;
		point_after_transform.x = point_after[0];
		point_after_transform.y = point_after[1];
		point_after_transform.z = point_after[2];

		pc_out->points.push_back(point_after_transform);
	}

	// for(size_t i = 0; i < cloud_size; i++){
		// cout<<"pc_in->points["<<i<<"] : "<<pc_in->points[i]<<endl;
		// cout<<"pc_out->points["<<i<<"] : "<<pc_out->points[i]<<endl;
	// }
}

vector< vector<int> > check_pairing(pcl::PointCloud<pcl::PointXYZ>::Ptr query_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr model_cloud, nav_msgs::Odometry lcl_, int search_gauss_num){
	size_t query_cloud_size = query_cloud->points.size();
	size_t model_cloud_size = model_cloud->points.size();
	
	vector< vector<int> > pair_candidate;
	
	if(query_cloud_size <= model_cloud_size){
		for(size_t i = 0; i < query_cloud_size; i++){
			for(size_t j = 0; j < model_cloud_size; j++){
				// cout<<"i : "<<i<<", j : "<<j<<", "<<calc_dist(query_cloud->points[i].x, model_cloud->points[j].x, query_cloud->points[i].y, model_cloud->points[j].y)<<endl;
				if(calc_dist(query_cloud->points[i].x, model_cloud->points[j].x, query_cloud->points[i].y, model_cloud->points[j].y) <= 1.0){
					vector<int> temp(2);
					temp[0] = i;
					temp[1] = j;
					pair_candidate.push_back(temp);
				}
			}
		}
	}
	else{
		for(size_t i = 0; i < model_cloud_size; i++){
			for(size_t j = 0; j < query_cloud_size; j++){
				// cout<<"i_ : "<<i<<", j_ : "<<j<<", "<<calc_dist(query_cloud->points[j].x, model_cloud->points[i].x, query_cloud->points[j].y, model_cloud->points[i].y)<<endl;
				if(calc_dist(query_cloud->points[j].x, model_cloud->points[i].x, query_cloud->points[j].y, model_cloud->points[i].y) <= 1.0){
					vector<int> temp(2);
					temp[0] = i;
					temp[1] = j;
					pair_candidate.push_back(temp);
				}
			}
		}
	}
	
	for(size_t i = 0; i < pair_candidate.size(); i++){
		cout<<"pair_candidate["<<i<<"][0] : "<<pair_candidate[i][0]<<", pair_candidate["<<i<<"][1] : "<<pair_candidate[i][1]<<endl;	
	}

	return pair_candidate;
}

double SafeAcos (double x){
	if (x < -1.0){ 
		x = -1.0;
	}
	else if (x > 1.0){
		 x = 1.0;
	}
	return acos (x) ;
}

bool check_sign(pcl::PointXYZ query, pcl::PointXYZ model){
	float s = query.x * model.y - query.y * model.x;
	if(s >= 0.0){
		return true;
	}
	else{
		return false;
	}
}

float expand_angle(float now_angle){
	float angle_ = now_angle;
	if((now_angle * last_angle) < 0.0){
		if((now_angle - last_angle) < -M_PI*1.8){
			n++;
		}
		else if((now_angle - last_angle) > M_PI*1.8){
			n--;
		}
	}
	// cout<<"now_angle : "<<now_angle<<endl;
	// cout<<"last_angle : "<<last_angle<<endl;
	// cout<<"n : "<<n<<endl;
	angle_ += 2.0 * n * M_PI;
	// cout<<"angle_ : "<<angle_<<endl;
	last_angle = now_angle;
	return angle_;
}

float PI2PI(float angle){
	while(angle >= M_PI){
		angle -= 2.0 * M_PI;
	}
	while(angle<=-M_PI){
		angle += 2.0 * M_PI;
	}
	return angle;
}

float predict_azimus(pcl::PointCloud<pcl::PointXYZ>::Ptr query, pcl::PointCloud<pcl::PointXYZ>::Ptr model, vector< vector<int> > pair, nav_msgs::Odometry lcl_, int search_gauss_num){
	vector<float> azimus;
	size_t query_size = query->points.size();
	size_t model_size = model->points.size();

	size_t pair_size = pair.size();
	bool sign_flag = false;

	for(size_t i = 0; i < pair_size; i++){
		if(query_size <= model_size){
			azimus.push_back(SafeAcos(calc_simirality(query->points[pair[i][0]], model->points[pair[i][1]])));
			// cout<<"(q)azimus["<<i<<"] : "<<azimus[i]<<endl;
			sign_flag = check_sign(query->points[pair[i][0]], model->points[pair[i][1]]);
			// cout<<"(q)check_sign : "<<check_sign(query->points[pair[i][0]], model->points[pair[i][1]])<<endl;
		}
		else{
			azimus.push_back(SafeAcos(calc_simirality(query->points[pair[i][1]], model->points[pair[i][0]])));
			// cout<<"(m)azimus["<<i<<"] : "<<azimus[i]<<endl;
			sign_flag = check_sign(query->points[pair[i][1]], model->points[pair[i][0]]);
			// cout<<"(m)check_sign : "<<check_sign(query->points[pair[i][1]], model->points[pair[i][0]])<<endl;
		}
	}
	// cout<<"azimus.size() : "<<azimus.size()<<endl;

	float average_azimus;
	if(azimus.size() != 0){
		average_azimus = accumulate(azimus.begin(), azimus.end(), 0.0) / azimus.size();

		cout<<"average_azimus : "<<average_azimus<<endl;
		
		cout<<"dgauss_position["<<search_gauss_num<<"][2] : "<<dgauss_position[search_gauss_num][2]<<endl;

		float yaw;
		if(sign_flag){
			yaw = dgauss_position[search_gauss_num][2] + average_azimus;
			// cout<<"+ "<<average_azimus<<endl;
		}
		else{
			yaw = dgauss_position[search_gauss_num][2] - average_azimus;
			// cout<<"- "<<average_azimus<<endl;
		}
		
		yaw = PI2PI(yaw);
		cout<<"yaw(-PI~PI) : "<<yaw<<endl;
		
		yaw = expand_angle(yaw);
		cout<<"yaw(-2nPI~2nPI) : "<<yaw<<endl;

		cout<<"lcl_.pose.pose.orientation.z : "<<lcl_.pose.pose.orientation.z<<endl;
		cout<<"yaw : "<<yaw + 0.5 * M_PI<<", yaw(deg) : "<<(yaw + 0.5 * M_PI) / M_PI*180.0<<endl;

		return	yaw + 0.5 * M_PI;
	}
	else{
		cout<<"!!!!!!!!!!!!No Pair(距離のせい)!!!!!!!!!!!!"<<endl;
		return lcl_.pose.pose.orientation.z;
	}
}

void predict_position(pcl::PointCloud<pcl::PointXYZ>::Ptr query, pcl::PointCloud<pcl::PointXYZ>::Ptr model, vector< vector<int> > pair, nav_msgs::Odometry lcl_, int search_gauss_num, float yaw){
	cout<<"ASDSDAD"<<endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr model_after_transform (new pcl::PointCloud<pcl::PointXYZ>);
	
	transform(model, model_after_transform, lcl_.pose.pose.position.x, lcl_.pose.pose.position.y, lcl_.pose.pose.position.z, yaw);

	sensor_msgs::PointCloud2 vis_model_output;

}

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
	cout<<"■■■■■■■■■■■■■■■■■■■■"<<endl;

	if(msg->data.size()==0){//msgに主平面が含まれていない場合
		ROS_FATAL("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}

	pcl::PointCloud<pcl::PointXYZ>::Ptr query_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr query_cloud_after_transform (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr model_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr model_cloud_after_transform (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromROSMsg(*msg, *query_cloud);
	// cout<<"query_cloud->points.size() : "<<query_cloud->points.size()<<endl;
	// cout<<"msg->header.frame_id  : "<<msg->header.frame_id<<endl;

	int search_gauss_num = search_nearest_model_num(lcl);
	cout<<"search_gauss_num : "<<search_gauss_num<<endl;

	get_model(search_gauss_num, model_cloud);
	


	transform(query_cloud, query_cloud_after_transform, lcl.pose.pose.position.x, lcl.pose.pose.position.y, lcl.pose.pose.position.z, lcl.pose.pose.orientation.z - 0.5 * M_PI);
	transform(model_cloud, model_cloud_after_transform, dgauss_position[search_gauss_num][0], dgauss_position[search_gauss_num][1], 0.0, dgauss_position[search_gauss_num][2]);
	
	
	
	vector< vector<int> > pair;
	pair = check_pairing(query_cloud_after_transform, model_cloud_after_transform, lcl, search_gauss_num);


	float yaw;
	visualization_for_pairing(query_cloud_after_transform, model_cloud_after_transform);
	yaw = predict_azimus(query_cloud, model_cloud, pair, lcl, search_gauss_num);

	nav_msgs::Odometry Dgauss_yaw;
	Dgauss_yaw.pose.pose.orientation.z = yaw;
	pub_Dgauss_yaw.publish(Dgauss_yaw);


	predict_position(query_cloud, model_cloud, pair, lcl, search_gauss_num, yaw);

}

void lcl_callback(nav_msgs::Odometry msg){
	lcl = msg;
	// cout<<"lcl.pose.pose.position.x : "<<lcl.pose.pose.position.x<<endl;
	// cout<<"lcl.pose.pose.position.y : "<<lcl.pose.pose.position.y<<endl;
	// cout<<"lcl.pose.pose.position.z : "<<lcl.pose.pose.position.z<<endl;

	// cout<<"lcl.pose.pose.orientation.z : "<<lcl.pose.pose.orientation.z<<endl;
}

void lcl_init_callback(nav_msgs::Odometry msg){
	lcl_init = msg;
	// // cout<<"lcl_init.pose.pose.position.x : "<<lcl_init.pose.pose.position.x<<endl;
	// // cout<<"lcl_init.pose.pose.position.y : "<<lcl_init.pose.pose.position.y<<endl;
	// // cout<<"lcl_init.pose.pose.position.z : "<<lcl_init.pose.pose.position.z<<endl;

	// // cout<<"lcl_init.pose.pose.orientation.z : "<<lcl_init.pose.pose.orientation.z<<endl;
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation_aoyan");
	ros::NodeHandle n;
	
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2", 1, pc2_callback);//for perfect velodyne pose_estimation//1105
	
	// ros::Subscriber sub_lcl = n.subscribe("/lcl",1,lcl_callback);
	ros::Subscriber sub_lcl = n.subscribe("/ekf_DgaussAndNDT",1,lcl_callback);
	
	ros::Subscriber sub_lcl_ini = n.subscribe("/lcl/initial_pose",1,lcl_init_callback);
	
	pub_Dgauss_yaw = n. advertise<nav_msgs::Odometry>("/Dgauss_sphere/yaw", 1);

	pub_vis_query = n.advertise<sensor_msgs::PointCloud2>("/Dgauss_sphere/query_points", 1);
	pub_vis_query2 = n.advertise<sensor_msgs::PointCloud2>("/Dgauss_sphere/query_points2", 1);
	pub_vis_model = n.advertise<sensor_msgs::PointCloud2>("/Dgauss_sphere/model_points", 1);
	pub_vis_model2 = n.advertise<sensor_msgs::PointCloud2>("/Dgauss_sphere/model_points2", 1);

	pub_query_mk_a = n.advertise<visualization_msgs::MarkerArray>("/Dgauss_sphere/query_nums", 1);
	pub_model_mk_a = n.advertise<visualization_msgs::MarkerArray>("/Dgauss_sphere/model_nums", 1);
	
	
	FILE* fp;

	dgauss_position.resize(10000);
	for(int i=0;i<10000;i++){
		dgauss_position[i].resize(3);
	}
	
	if( (fp = fopen(file_name_list,"r")) == NULL){
		cout << "Dgauss_list の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}
	else{
		float a, b, c = 0.0;
		while(fscanf(fp, "%f, %f, %f", &a, &b, &c) != EOF) {	
			dgauss_position[cnt][0] = a;
			dgauss_position[cnt][1] = b;
			dgauss_position[cnt][2] = c;
			cout<<dgauss_position[cnt][0]<<"  "<<dgauss_position[cnt][1]<<"   "<<dgauss_position[cnt][2]<<endl;

			cnt++;
			
		}
		dgauss_position.resize(cnt);
	}
	fclose(fp);
	
	cout << "Pose Estimation START !!" << endl;
	
	last_angle = lcl_init.pose.pose.orientation.z;

	ros::spin();
	return 0;
}
