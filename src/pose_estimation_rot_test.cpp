//
//	pose_estimation_rot_test.cpp
//
//	last update 2017/02/06
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;


const char* file_name = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/rot_test.txt";

bool pc_callback_flag = false;
bool DGauss_match = true;
bool match = false;
bool dgauss_flag = false;
bool start_calq = true;

const float RATIO = 0.50;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc_;
sensor_msgs::PointCloud2 pc2;
sensor_msgs::PointCloud2 rotated_pc2_debug;
sensor_msgs::PointCloud2 pc2_;
sensor_msgs::PointCloud2 pc2_rot;
sensor_msgs::PointCloud2 pre_pc2;
sensor_msgs::PointCloud2 pc2_model;
sensor_msgs::PointCloud2 pc2_query;
nav_msgs::Odometry lcl;
nav_msgs::Odometry lcl_ini;
nav_msgs::Odometry Dlcl;
nav_msgs::Odometry Dlcl_;
ros::Publisher pub;
ros::Publisher pub_;
ros::Publisher pub2;
ros::Publisher pub2_;
ros::Publisher pub2_rot;
ros::Publisher pre_pub2;
ros::Publisher pub_DGauss_pose;
ros::Publisher pub_DGauss_yaw;
ros::Publisher pub_model;
ros::Publisher pub_query;


const float gauss_sphere_range = 1.0;
const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]
//const float D_THRESHOLD = 2.0; //[m] model gaussとの距離の閾値
//0825
const float D_THRESHOLD = 3.0; //[m] model gaussとの距離の閾値

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

int cnt=0;
int search_gauss_num = 0;
float dist = 0.0;

double deg = 0.0;
double yaw = 0.0;

vector< vector<float> > dgauss_position;
vector< vector<double> > gmodel;

pcl::PointCloud<pcl::PointXYZ>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZ>);//前ステップにおけるガウス球のための箱
//pcl::PointCloud<pcl::PointXYZI>::Ptr pre_gauss (new pcl::PointCloud<pcl::PointXYZI>);//前ステップにおけるガウス球のための箱

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

float getdyaw(float ini, float azm){
	return azm - ini;
}

float calcdist(float x1, float x2, float y1, float y2){
	return sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

void pc2_callback(sensor_msgs::PointCloud2ConstPtr msg){
	//cout<<"■■■■■■■■■■■■■■■■■■■■"<<endl;
	
	if(msg->data.size()==0){//msgに主平面が含まれていない場合
		ROS_FATAL("SUBSCRIBE MSG HAS NO CLOUDS");
		return;
	}
	
	vector< vector<double> > tmp_glist,tmp_vec;
	size_t num_glist = 3000;
	size_t cnt_glist = 0;
	tmp_glist.resize(num_glist);
	tmp_vec.resize(1);
	for(size_t i=0;i<num_glist;i++){
		tmp_glist[i].resize(2);
	}
	tmp_vec[0].resize(2);//最大値を入れ替える用の箱
	
	//自己位置からD_THRESHOLD内に存在するdgaussの距離と番号を保存していく
	//
	for(size_t i=0;i<(size_t)dgauss_position.size();i++){
		dist = calcdist(lcl.pose.pose.position.x,dgauss_position[i][0],lcl.pose.pose.position.y,dgauss_position[i][1]);

		if(dist <= D_THRESHOLD){//D_THRESHOLD内のgauss球の主平面を読み込む
			//
			//	D_THRESHOLD内に存在するdgaussをtmp_glist[i][j]に格納していく
			//	iは単なる数字 jは0:distance 1:dgauss numberを格納しておく
			//
			tmp_glist[cnt_glist][0] = dist;
			tmp_glist[cnt_glist][1] = i+1;
			cnt_glist++;
			
			search_gauss_num = i+1;
			dgauss_flag=true;
			
		}
	}
	tmp_glist.resize(cnt_glist);//resizeしてtmp_glistをcnt_glistと同じサイズにする
	
	if(dgauss_flag == false){//lcl と比較してD_THRESHOLD内にガウス球が無い場合
		ROS_WARN("!!!!! NO D-GAUSS NEARBY INFANT !!!!!!");
		return;
	}
	
	//distの値を見て並び替え
	//
	if(cnt_glist == 1){

	}else if(cnt_glist == 0){
		ROS_WARN("!!!!! NO D-GAUSS NEARBY INFANT ");
		return;
	}else{
		for(size_t i=0;i<cnt_glist-1;i++){
			for(size_t j=1;j<cnt_glist;j++){
				if(tmp_glist[i][0] >= tmp_glist[j][0]){//[i][0]に入っているdistを比較して小さい順に並び替える
					tmp_vec[0][0] = tmp_glist[i][0];
					tmp_vec[0][1] = tmp_glist[i][1];
					tmp_glist[i][0] = tmp_glist[j][0];
					tmp_glist[i][1] = tmp_glist[j][1];
					tmp_glist[j][0] = tmp_vec[0][0];
					tmp_glist[j][1] = tmp_vec[0][1];
				}
			}
		}
		search_gauss_num = tmp_glist[0][1];//最もdistが小さいdgaussの番号呼び出し
	}
	
	
	
if(start_calq){
	start_calq = false;
	
	deg += 1.0;
	yaw = deg * 3.141592 / 180.0;
if(deg<=45.0){	
	//////////////処理時間の計測//////////////
	struct timeval b_sec, a_sec;
	gettimeofday(&b_sec, NULL);
    /////////////////////////////////////////

	//double yaw = 0.0;//(-M_PI * 0.50) + lcl_ini.pose.pose.orientation.z + getdyaw(lcl_ini.pose.pose.orientation.z,lcl.pose.pose.orientation.z);//座標系を揃えている(?) おそらくmap座標系で考えている
	//cout << "yaw : " << yaw << " [rad] " << yaw * 180.0 / M_PI << " [deg]" <<endl;
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_rot (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud_ (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_m (new pcl::PointCloud<pcl::PointXYZ>);//モデル、クエリの可視化用
	pcl::PointCloud<pcl::PointXYZ>::Ptr pub_cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
	

	pcl::fromROSMsg(*msg, *tmp);
	size_t num = tmp->points.size();

	for(size_t i=0;i<num;i++){
		tmp_cloud->points.push_back(tmp->points[i]);			
	}
	*tmp_rot = *tmp_cloud;

	double cos_ = cos(yaw);
	double sin_ = sin(yaw);
	
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		tmp_cloud->points[i].x = ( tmp_rot->points[i].x * cos_ ) - ( tmp_rot->points[i].y * sin_ );
		tmp_cloud->points[i].y = ( tmp_rot->points[i].x * sin_ ) + ( tmp_rot->points[i].y * cos_ );
	}
	
	FILE* fp2;
	char file_name_1[100];
	
	sprintf(file_name_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/rot_test/%d.txt",search_gauss_num);

	gmodel.resize(10000);
	for(int i=0;i<10000;i++){
		gmodel[i].resize(3);
	}
	int gcnt = 0;
	
	if( (fp2 = fopen(file_name_1,"r")) == NULL){
		
		cout << "Dgauss の主平面情報の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		fclose(fp2);
		return;
		//exit(1);
	}else{
		float a,b,c = 0.0;
		
		while(fscanf(fp2, "%f,%f,%f", &a, &b, &c) != EOF) {	
			gmodel[gcnt][0] = a;
			gmodel[gcnt][1] = b;
			gmodel[gcnt][2] = c;
			gcnt++;
		}
		gmodel.resize(gcnt);
		pub_cloud_m->points.resize(gcnt);
		for(int i=0;i<gcnt;i++){
			pub_cloud_m->points[i].x = gmodel[i][0];
			pub_cloud_m->points[i].y = gmodel[i][1];
			pub_cloud_m->points[i].z = gmodel[i][2];
		}
		
		
	}
	if(gmodel[0][0] == 1000){// 主平面が取れていない場合，txt fileには1000が代入されているため
		ROS_FATAL("!!!!! txt FILE DO NOT HAVE INFORMATION !!!!!!");
		fclose(fp2);
		return;
	}
	fclose(fp2);

	MatrixXd Vector(3,1);
	Vector<<0.0,
			0.0,
			0.0;
	
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_m (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_q (new pcl::PointCloud<pcl::PointXYZ>);
	
	Eigen::Vector3f model_vector;
	Eigen::Vector3f query_vector;
	float dot_ij[(int)gmodel.size()][3];
	float aaa[3];
	float theta[tmp_cloud->points.size()];
	float dyaw = 0.0;
	float theta_count = 0.0;
	
	for(size_t i=0;i<(size_t)tmp_cloud->points.size();i++){
		if(calcdist(tmp_cloud->points[i].x,0.0,tmp_cloud->points[i].y,0.0)>=1.0){
			for(int j=0;j<(int)gmodel.size();j++){
			
				model_vector << (gmodel[j][0])
							   ,(gmodel[j][1])
							   ,(gmodel[j][2]);
				query_vector << (tmp_cloud->points[i].x)
							   ,(tmp_cloud->points[i].y)
							   ,(tmp_cloud->points[i].z);
			
				dot_ij[j][0] = i;
				dot_ij[j][1] = j;
				dot_ij[j][2] = model_vector.dot(query_vector) / ( model_vector.norm() * query_vector.norm() );//cosΘ
			}
		
			for(int j=0;j<(int)gmodel.size()-1;j++){
				for(int k=1;k<(int)gmodel.size();k++){
					if(dot_ij[j][2] < dot_ij[k][2]){
						aaa[0] = dot_ij[k][0];
						aaa[1] = dot_ij[k][1];
						aaa[2] = dot_ij[k][2];
					
						dot_ij[k][0] = dot_ij[j][0];
						dot_ij[k][1] = dot_ij[j][1];
						dot_ij[k][2] = dot_ij[j][2];
					
						dot_ij[j][0] = aaa[0];
						dot_ij[j][1] = aaa[1];
						dot_ij[j][2] = aaa[2];
					}
				}
			}

			float cross = (tmp_cloud->points[dot_ij[0][0]].x * gmodel[dot_ij[0][1]][1]) - (gmodel[dot_ij[0][1]][0] * tmp_cloud->points[dot_ij[0][0]].y);
			float norm = sqrt( (tmp_cloud->points[dot_ij[0][0]].x)*(tmp_cloud->points[dot_ij[0][0]].x) + (tmp_cloud->points[dot_ij[0][0]].y)*(tmp_cloud->points[dot_ij[0][0]].y) )
							* sqrt( (gmodel[dot_ij[0][1]][0])*(gmodel[dot_ij[0][1]][0]) + (gmodel[dot_ij[0][1]][1])*(gmodel[dot_ij[0][1]][1]));
		
			theta[i] = asin(cross/norm);
			
			dyaw += theta[i];
			theta_count++;
		}
	}
	
	dyaw /= theta_count;
	
	//////////////処理時間の計測//////////////
	gettimeofday(&a_sec, NULL);
	//cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
	/////////////////////////////////////////
	
	cout<<deg<<" "<<dyaw * 180.0 / 3.141592<<" "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<endl;
	
	//cout<<"dyaw : "<<dyaw * 180.0 / M_PI <<"[deg]"<<endl;
	
	pub_DGauss_pose.publish(Dlcl);
	pcl::toROSMsg(*tmp_cloud, rotated_pc2_debug);//クラスタした点
	pcl::toROSMsg(*tmp_cloud_, pc2_);
	pcl::toROSMsg(*pre_gauss, pre_pc2);//クラスタした点の1ステップ前のもの
	
	rotated_pc2_debug.header.frame_id = "/velodyne";//クラスタした点
	rotated_pc2_debug.header.stamp = ros::Time::now();
	pub2.publish(rotated_pc2_debug);
	rotated_pc2_debug.data.clear();

	pc2_.header.frame_id = "/velodyne";
	pc2_.header.stamp = ros::Time::now();
	pub2_.publish(pc2_);
	pc2_.data.clear();
}
	start_calq = true;
}
	
	dgauss_flag=false;
}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "pose_estimation");
	ros::NodeHandle n;
	
	ros::Rate loop(20);
	
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/principal_components2",1,pc2_callback);//for perfect velodyne pose_estimation//1105

	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed",1);
	pub_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components_removed_",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/rotated_pc2_query",1);			//クラスタした点
	pub2_ = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_",1);
	pre_pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2_removed_pre",1);	//クラスタした点の1ステップ前のもの
	pub_DGauss_pose = n.advertise<nav_msgs::Odometry>("/gauss_sphere/pose", 10);
	pub_DGauss_yaw = n.advertise<nav_msgs::Odometry>("/gauss_sphere/yaw", 10);
	
	pub_model = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/model",1);
	pub_query = n.advertise<sensor_msgs::PointCloud2>("/gauss_sphere/query",1);
	
	lcl.pose.pose.position.x = 0.0;
	lcl.pose.pose.position.y = 0.0;
	lcl.pose.pose.position.z = 0.0;
	lcl.pose.pose.orientation.z = 0.0;
	
	lcl_ini.pose.pose.position.x = 0.0;
	lcl_ini.pose.pose.position.	y = 0.0;
	lcl_ini.pose.pose.orientation.z = 0.0;
		
	FILE* fp;

	dgauss_position.resize(10000);
	for(int i=0;i<10000;i++){
		dgauss_position[i].resize(2);
	}
	
	if( (fp = fopen(file_name,"r")) == NULL){
		cout << "Dgauss_list の読み込みに失敗しました" << endl << "プログラムを停止します" << endl;
		exit(1);
	}else{
		float a,b = 0.0;
		while(fscanf(fp, "%f,%f", &a, &b) != EOF) {	
			dgauss_position[cnt][0] = a;
			dgauss_position[cnt][1] = b;
			cout<<dgauss_position[cnt][0]<<"  "<<dgauss_position[cnt][1]<<endl;
			
			cnt++;
		}
		dgauss_position.resize(cnt);
	}
	fclose(fp);
	
	cout << "Pose Estimation START !!" << endl;
	cout<<"↓の数字は，回転角[deg]，推定角度[deg]，計算時間[ms]←(1/1000倍すること)　の順"<<endl;
	
	while(ros::ok()){
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
