#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>

#include <unistd.h>

void imu_callback(sensor_msgs::Imu msg){
	//imu_msg
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "imu_test");
	ros::NodeHandle n;

	//ros::Publisher navioimu_pub = n.advertise<sensor_msgs::Imu>("/imu/data_raw", 10);
	
	ros::Rate loop_rate(10);
	ros::Subscriber imu_sub = n.subscribe("/imu/rpy/filtered",1,imu_callback);
	
	sensor_msgs::Imu imu_msg;
	sensor_msgs::MagneticField mag_msg;

	FILE *fp1;
	fp1 = fopen("./rpy.csv","w");
	fprintf(fp1, "roll(x),pitch(y),yaw(z)\n");

    while(ros::ok()) {
        
		//fprintf(fp1, "%lf,%lf,%lf\n",);

		/*
		sensor->update();
        sensor->read_accelerometer(&ax, &ay, &az);
        sensor->read_gyroscope(&gx, &gy, &gz);
        sensor->read_magnetometer(&mx, &my, &mz);
        printf("Acc: %+7.3f %+7.3f %+7.3f  ", ax, ay, az);
        printf("Gyr: %+8.3f %+8.3f %+8.3f  ", gx, gy, gz);
        printf("Mag: %+7.3f %+7.3f %+7.3f\n", mx, my, mz);
		imu_msg.header.stamp = ros::Time::now();
		imu_msg.header.frame_id = "base_link";
		mag_msg.header.stamp = ros::Time::now();
		mag_msg.header.frame_id = "base_link";
		imu_msg.angular_velocity.x = gx;
		imu_msg.angular_velocity.y = gy;
		imu_msg.angular_velocity.z = gz;
		imu_msg.linear_acceleration.x = ax;
		imu_msg.linear_acceleration.y = ay;
		imu_msg.linear_acceleration.z = az;
		mag_msg.magnetic_field.x = mx;
		mag_msg.magnetic_field.y = my;
		mag_msg.magnetic_field.z = mz;

		navioimu_pub.publish(imu_msg);
		naviomag_pub.publish(mag_msg);
		*/
		ros::spinOnce();
		loop_rate.sleep();

	}
	return 0;
}
