//2016 11 01

#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

using namespace std;
using namespace Eigen;

typedef pcl::PointCloud<pcl::PointXYZ> CloudXYZ;

pcl::PointCloud<pcl::PointXYZ> vel_input;
pcl::PointCloud<pcl::PointXYZINormal> normal_input;
pcl::PointCloud<pcl::PointNormal>::Ptr cloud_n (new pcl::PointCloud<pcl::PointNormal>);
CloudXYZ cloud_write;

int main (int argc, char** argv)
{
	ros::init(argc, argv, "pcd_saver");
	ros::NodeHandle n;
	
	int file_num = 78;
	char filename[100];
	sprintf(filename,"/home/amsl/3Dmap/din_20161109/drawed_map/%d.pcd",file_num);
	
	if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud_n) == -1){
		cout << file_num <<".pcd not found." << endl;
		return(0);
	}
	
	for(int i=0;i<cloud_n->points.size();i++){
		pcl::PointXYZ pnt;
		pnt.x = cloud_n->points[i].x;
		pnt.y = cloud_n->points[i].y;
		pnt.z = cloud_n->points[i].z;

		cloud_write.push_back(pnt);
	}
	
	char filename1[100];
	sprintf(filename1,"/home/amsl/%d.pcd",file_num);
	pcl::io::savePCDFileBinary(filename1, cloud_write);
	
	cout << "End!!" << endl;
	
	ros::spin();
	return 0;
}
