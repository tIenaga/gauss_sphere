//2016 11 01

#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>

using namespace std;
using namespace Eigen;

int file_num1 = 1;
int file_num2 = 1;

pcl::PointCloud<pcl::PointXYZ> vel_input;
pcl::PointCloud<pcl::PointXYZINormal> normal_input;

void pc1_callback(sensor_msgs::PointCloud2::ConstPtr msg){//PointXYZ
	pcl::fromROSMsg(*msg , vel_input);
	
	char filename1[100];
	sprintf(filename1,"/home/amsl/test/vel_points/%d.pcd",file_num1);
	pcl::io::savePCDFileBinary(filename1, vel_input);
	file_num1++;
	
	cout<<"VEL_POINTS!"<<endl;
}


void pc2_callback(sensor_msgs::PointCloud2::ConstPtr msg){//PointXYZINormal
	pcl::fromROSMsg(*msg , normal_input);
	
	char filename2[100];
	sprintf(filename2,"/home/amsl/test/point_normal/%d.pcd",file_num2);
	pcl::io::savePCDFileBinary(filename2, normal_input);
	file_num2++;
	
	cout<<"	NORMAL!"<<endl;
}

int main (int argc, char** argv)
{
	ros::init(argc, argv, "pcd_saver");
	ros::NodeHandle n;
	
	ros::Subscriber sub1 = n.subscribe("/velodyne_points",1,pc1_callback);
	ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/normal",1,pc2_callback);
	
	cout << "Here We Go !!!" << endl;
	
	ros::spin();
	return 0;
}
