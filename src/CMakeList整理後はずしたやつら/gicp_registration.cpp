//	
//	gicp_registration.cpp
//	
//	last update 2016 / 06 / 17
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

bool callback_flag=false;

pcl::PointCloud<pcl::PointXYZ>::Ptr pc_raw (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_tf (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> output_raw;
pcl::PointCloud<pcl::PointXYZINormal> output_transfered;
sensor_msgs::PointCloud2 ros_pc2_1;
sensor_msgs::PointCloud2 ros_pc2_2;
int unko = 1;

//pub_3 = n.advertise<sensor_msgs::PointCloud2>("perfect_velodyne/normal",1);

void pc_callback(const sensor_msgs::PointCloud2::Ptr &msg)
{
	
	if(unko == 1){
		cout<<" !!! 11111111 !!!"<<endl;
		ros_pc2_1 = *msg;
		unko ++;
	}else if(unko ==2){
		cout<<" !!! 22222222 !!!"<<endl;
		ros_pc2_2 = *msg;
		unko ++;
		
		callback_flag = true;
	}
} 

//	********DONE*******
//	subscribeはnormal estimationから受け取る
//	pcl::pointnormalで二つ用意しておく
//	
//	callback内で
//	この二つがsrcとtargetとして保持しておく
//	フラッグかなんかでひとまず連続する二つを保存しておく
//
//
//１	main文で
//	gicpを初期条件含め定義する
//	gicpの関数にセットする
//	実際に計算させてみて結果をcout
//
//	******TODO******
//
//2	50cmずらしたりなどしてきちんと機能しているかの確認をする
//		
//	F = gicp.getFinalTransformation()の行列Rの部分がクォータニオンなのか回転行列なのかでsrcが変わるので
//	そこを確認しましょう
//	
//	
//	
//	

int main (int argc, char** argv)
{
	ros::init(argc, argv, "icp_registration");
	ros::NodeHandle n;	
	ros::Rate loop(20);
	
	//ros::Subscriber sub = n.subscribe("/velodyne_points",1,pc_callback);
	ros::Subscriber sub = n.subscribe("/perfect_velodyne/normal",1,pc_callback);
		
	ros::Publisher raw_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/gicp_registration/raw",1);
	ros::Publisher tf_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/gicp_registration/transfered",1);
	
//gicp parameter
//
	pcl::GeneralizedIterativeClosestPoint<pcl::PointNormal, pcl::PointNormal> gicp; 
    //pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZINormal, pcl::PointXYZINormal> gicp; 
	
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_src (new pcl::PointCloud<pcl::PointNormal>);
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_tgt (new pcl::PointCloud<pcl::PointNormal>);
	
	gicp.setMaxCorrespondenceDistance (0.5);//0.3//#ark_memo# 対応距離の最大値
	gicp.setMaximumIterations (100);        //20//#ark_memo# ICPの最大繰り返し回数
	gicp.setTransformationEpsilon (1e-8);   //#ark_memo# 変換パラメータ値
 	gicp.setEuclideanFitnessEpsilon (1e-8); //#ark_memo# ??
	
	
	
	cout << "gicp_registration START !!" << endl;
	
	
	int count = 0;
	while(ros::ok()){	

		double yyy = 0.0;
		
		//ros_pcをrviz上に表示するための部分
		//
		//pcl::toROSMsg(output_raw,ros_pc);
		ros_pc2_1.header.frame_id="/velodyne";
		ros_pc2_1.header.stamp=ros::Time::now();
		raw_pc_pub.publish(ros_pc2_1);
		//output_raw.points.clear();

		//pc_tfをrviz上に表示するための部分
		//
		//pcl::toROSMsg(*pc_tf,ros_pc2_2);
		ros_pc2_2.header.frame_id="/velodyne";
		ros_pc2_2.header.stamp=ros::Time::now();
		tf_pc_pub.publish(ros_pc2_2);
		//output_raw.points.clear();
		
		//modelとqueryのpointXYZを準備、ICPの定義
		//
		pcl::fromROSMsg(ros_pc2_1, *cloud_src);
		pcl::fromROSMsg(ros_pc2_2, *cloud_tgt);
		
		//////////////処理時間の計測//////////////
		struct timeval b_sec, a_sec;
		gettimeofday(&b_sec, NULL);
		/////////////////////////////////////////
		
		if(callback_flag){
			//gicp.setInputCloud(cloud_src);            //srcのPointCloud
														//こっちだとWARNING
			gicp.setInputSource(cloud_src); 			//setInputSourceとsetInputCloudの違いは確認してない	
			gicp.setInputTarget(cloud_tgt);             //targetのPointCloud
			pcl::PointCloud<pcl::PointNormal> Final;
		
			gicp.align(Final);                			//gicpを実行し，srcの移動結果を取得
		
			Eigen::Matrix4f F;
			F = gicp.getFinalTransformation();			//gicpによって得られた変換行列をEigenに代入

			//cout<<"test"<<endl;
			cout<<"transformation : "<<endl<<F<<endl;
			cout<<"(x,y) = "<<"( "<<F(0,3)<<" , "<<F(1,3)<<" )"<<endl;	
			//cout<<yyy<<"	"<<yyy+transformation(1,3)<<"	"<<(a_sec.tv_sec - b_sec.tv_sec) + (a_sec.tv_usec - b_sec.tv_usec)*1e-6<<endl;
				
			callback_flag = false;
	
		}
		
		//////////////処理時間の計測//////////////
		gettimeofday(&a_sec, NULL);
		cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
		/////////////////////////////////////////
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
