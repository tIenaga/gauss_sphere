#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
// #include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/statistical_outlier_removal.h>//
#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>


using namespace std;
using namespace Eigen;

typedef pcl::PointNormal PointN;
typedef pcl::PointCloud<PointN>  CloudN;
typedef pcl::PointCloud<PointN>::Ptr  CloudNPtr;
typedef pcl::PointXYZINormal PointA;
typedef pcl::PointCloud<PointA>  CloudA;
typedef pcl::PointCloud<PointA>::Ptr  CloudAPtr;

ros::Publisher pub_debug;
pcl::PointCloud<pcl::PointXYZINormal> pc_input;

const float gauss_sphere_range = 1.0;

const int color_num = 360;
const int color_res = 10;

const size_t skip = 10;

CloudN shift(Eigen::Matrix4f m, CloudN cloud){
	CloudN rsl;
	//	cout << "mat=" << endl <<  m << endl << endl;
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p << cloud.points[i].x, cloud.points[i].y, cloud.points[i].z, 1.0;
		n << cloud.points[i].normal_x, cloud.points[i].normal_y, cloud.points[i].normal_z, 0.0;
		p = m*p;
		n = m*n;
		pcl::PointNormal pnt;
		pnt.x = p(0);
		pnt.y = p(1);
		pnt.z = p(2);
		pnt.normal_x = n(0);
		pnt.normal_y = n(1);
		pnt.normal_z = n(2);
		pnt.curvature = cloud.points[i].curvature;
		rsl.push_back(pnt);
	}
	return rsl;
}

Eigen::Matrix4f trans(float dat[7]){
	Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
	Eigen::Matrix3f r;
	/*
	   m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	   m(0,1) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	   m(0,2) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	   m(1,0) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	   m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	   m(1,2) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	   m(2,0) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	   m(2,1) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	   m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	 */////////////
	m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	m(1,0) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	m(2,0) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	m(0,1) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	m(2,1) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	m(0,2) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	m(1,2) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	////////////////
	m(0,3) = dat[0];
	m(1,3) = dat[1];
	m(2,3) = dat[2];

	return m;
	//return m.inverse();
}

CloudAPtr ptrTransform(CloudA pcl_in){
	CloudAPtr output(new CloudA);

	// output->width=1;
	// output->height=pcl_in.points.size();
	output->points.resize(pcl_in.points.size());
	for(size_t i=0;i<pcl_in.points.size();i++)
	{
		output->points[i].x=pcl_in.points[i].x;
		output->points[i].y=pcl_in.points[i].y;
		output->points[i].z=pcl_in.points[i].z;
		output->points[i].intensity=pcl_in.points[i].intensity;
		output->points[i].normal_x=pcl_in.points[i].normal_x;
		output->points[i].normal_y=pcl_in.points[i].normal_y;
		output->points[i].normal_z=pcl_in.points[i].normal_z;
		output->points[i].curvature=pcl_in.points[i].curvature;
	}
	return output;
}

void NormalToXYZINormal(CloudN& pc_tmp,CloudA& pc_tmp_i)
{
	for (size_t i=0; i<pc_tmp.points.size(); ++i){
		pc_tmp_i.points[i].x=pc_tmp.points[i].x;
		pc_tmp_i.points[i].y=pc_tmp.points[i].y;
		pc_tmp_i.points[i].z=pc_tmp.points[i].z;
		pc_tmp_i.points[i].normal_x=pc_tmp.points[i].normal_x;
		pc_tmp_i.points[i].normal_y=pc_tmp.points[i].normal_y;
		pc_tmp_i.points[i].normal_z=pc_tmp.points[i].normal_z;
		pc_tmp_i.points[i].intensity=0;
	}
}

void createDepthSphere(CloudA& pc,CloudA &pc_output2){
	
	CloudA pc_output;

	pc_output.points.resize(pc.points.size());
	// pc_output2.points.resize(pc.points.size());
	for(size_t i=0;i<pc.points.size();i++){
		pc_output.points[i].x = gauss_sphere_range * pc.points[i].normal_x;
		pc_output.points[i].y = gauss_sphere_range * pc.points[i].normal_y;
		pc_output.points[i].z = gauss_sphere_range * pc.points[i].normal_z;
		pc_output.points[i].normal_x = pc.points[i].normal_x;
		pc_output.points[i].normal_y = pc.points[i].normal_y;
		pc_output.points[i].normal_z = pc.points[i].normal_z;
		pc_output.points[i].curvature = pc.points[i].curvature;

		float color = 0.0;

		//find max and min of normal element
		float xyz[3] = {0.0};
		xyz[0] = pc_output.points[i].x;
		xyz[1] = pc_output.points[i].y;
		xyz[2] = pc_output.points[i].z;
		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
		}

		if((fabs(pc_output.points[i].x)>fabs(pc_output.points[i].y))
					&& (fabs(pc_output.points[i].x)>fabs(pc_output.points[i].z))){
			if(pc_output.points[i].x<0)
				color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min)+180.0;
			else color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min);
		}else if((fabs(pc_output.points[i].y)>fabs(pc_output.points[i].z))
					&& (fabs(pc_output.points[i].y)>fabs(pc_output.points[i].x))){
			if(pc_output.points[i].y<0)
				color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0;
		}else if((fabs(pc_output.points[i].z)>fabs(pc_output.points[i].x))
					&& (fabs(pc_output.points[i].z)>fabs(pc_output.points[i].y))){
			if(pc_output.points[i].z<0)
				color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0;
		}else{
		}
		// limit
		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		pc_output.points[i].intensity = color;

		float range = fabs(-pc.points[i].normal_x*pc.points[i].x
						-pc.points[i].normal_y*pc.points[i].y
						-pc.points[i].normal_z*pc.points[i].z);
		if (fabs(pc.points[i].normal_z)<0.9 && pc.points[i].z>0.2){//0.7
			PointA tmp;
			tmp.x = range * pc_output.points[i].normal_x;
			tmp.y = range * pc_output.points[i].normal_y;
			tmp.z = range * pc_output.points[i].normal_z;
			tmp.normal_x=pc.points[i].normal_x;
			tmp.normal_y=pc.points[i].normal_y;
			tmp.normal_z=pc.points[i].normal_z;
			tmp.intensity = color;
			tmp.intensity = color;
			pc_output2.points.push_back(tmp);
		}
		
	}
	
  	// Create the filtering object
	CloudAPtr pc_output2_ptr;
	CloudAPtr pc_output2_fil_ptr(new CloudA);
	pc_output2_ptr=ptrTransform(pc_output2);
  	pcl::StatisticalOutlierRemoval<pcl::PointXYZINormal> sor;
  	sor.setInputCloud (pc_output2_ptr);
  	sor.setMeanK (20);
  	sor.setStddevMulThresh (0.1);
	sor.filter (*pc_output2_fil_ptr);
	pc_output2.points.resize(pc_output2_fil_ptr->points.size());
	for (size_t i=0; i<pc_output2_fil_ptr->points.size(); ++i){
		pc_output2.points[i].x=pc_output2_fil_ptr->points[i].x;
		pc_output2.points[i].y=pc_output2_fil_ptr->points[i].y;
		pc_output2.points[i].z=pc_output2_fil_ptr->points[i].z;
		pc_output2.points[i].normal_x=pc_output2_fil_ptr->points[i].normal_x;
		pc_output2.points[i].normal_y=pc_output2_fil_ptr->points[i].normal_y;
		pc_output2.points[i].normal_z=pc_output2_fil_ptr->points[i].normal_z;
	}

}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "gauss_sphere");
	ros::NodeHandle n;
	
	cout << "Here We Go !!!" << endl;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////graphファイルの読み込み
	FILE *fp;
	fp = fopen("for_model.csv","r");
	//fp = fopen("aft.csv","r");
	//fp = fopen("bfr.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	///////////////////////////////////////////////
	FILE *fp2;
	char filename_2[100];
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	
	
	size_t max_index = 5000;
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );

			
			dat[0] = 0.0;
			dat[1] = 0.0;
			dat[2] = 0.0;
			
			Eigen::Matrix4f node;
			node = trans(dat);

			cout << "num = " << num << endl;
			char filename[100];
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			pcl::PointCloud<pcl::PointNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointNormal>);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud) == -1){
				cout << "cloud_" << num << "not found." << endl;
				break;
			}
            
            ////////////////////////////////////////////////////////
            char outfile[100];
			//addCloud( shift(node, *cloud) );
			CloudA pc_tmp;
	        pcl::PointCloud<pcl::PointNormal> cloud_fnl;
            cloud_fnl = shift(node, *cloud);
            
            pc_tmp.points.resize( cloud_fnl.points.size() );
            for(size_t i=0;i<cloud_fnl.points.size();i++){
            	pc_tmp.points[i].x = cloud_fnl.points[i].x;
            	pc_tmp.points[i].y = cloud_fnl.points[i].y;
            	pc_tmp.points[i].z = cloud_fnl.points[i].z;
            	pc_tmp.points[i].normal_x = cloud_fnl.points[i].normal_x;
            	pc_tmp.points[i].normal_y = cloud_fnl.points[i].normal_y;
            	pc_tmp.points[i].normal_z = cloud_fnl.points[i].normal_z;
				pc_tmp.points[i].intensity=0;
            }
            
            ////////////////////////////////////////////////////////
            //回転後のpcdが cloud_fnlに入っている
	/////////create depth gauss sphere and clustering////////////////////////////////////////////////////////////////////////////////////////
			//CloudA pc_tmp_i, d_gauss_pt;
			//NormalToXYZINormal(pc_tmp, pc_tmp_i);
			CloudA pc_dgsc;
			createDepthSphere(pc_tmp, pc_dgsc);	
            //process();
            
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            for(size_t i=0;i<pc_dgsc.points.size();i++){
				cout<<pc_dgsc.points[i].x<<endl;
				cout<<pc_dgsc.points[i].y<<endl;
			}
            FILE *fp1;
            char filename_1[100];
            //char* file_name_1 = "/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/0.txt";
            sprintf(filename_1,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss/ikuta/%d.txt",num);//ファイルの作成
            
		        
		     
		    if((fp1 = fopen(filename_1, "a+")) == NULL){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(EXIT_FAILURE);
			}
			
			
		    // 処理（ここでは、下記文字をファイルに書き込む）
			for(size_t i=0;i<pc_dgsc.points.size();i++){
				fprintf(fp1, "%f,%f,%f\n", pc_dgsc.points[i].x,pc_dgsc.points[i].y,pc_dgsc.points[i].z);
			}
			// ファイルを閉じる 
			fclose(fp1);
			
            
            
            ///////////////////
            //pc_dgsc の初期化
            pc_dgsc.points.clear();
            
            
            ////////////////////////////////////////////////////////
            sprintf(outfile, "clouds_cnvrt/%d.pcd", num);
            pcl::io::savePCDFileBinary(outfile, cloud_fnl);
			/////////////////////////////////////////////////////////
            
            cout << cnt << " / " << max_index << endl << endl;
			if(cnt == max_index)break;
			cnt += skip;
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);
	return 0;
}
