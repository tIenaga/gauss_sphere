/*********************************************************************

 src: split_waypoint.cpp
 
 last_update: '15.09.25
 memo: 
    rviz上でクリックした点の座標(x, y)を格納したファイル(txt)を読込み， 
    -->指定した長さで分割し，補間 ---> 細かいwaypointsをtxtに保存!!

***********************************************************************/
#include <ros/ros.h>
#include <iostream>
#include <string>

#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

using namespace std;
bool callback_flag = false;
bool is_first = true;
double tmp = 0.0;
int nn=1;

FILE *fp_r;//waypoint読込みファイル
FILE *fp_rr;//waypoint読込みファイル
FILE *fp_w;//waypoint書込みファイル

//geometry_msgs::PoseStamped click_pt;
sensor_msgs::PointCloud way_split_pp;
//sensor_msgs::PointCloud w_pt;

ros::Publisher pub_p;
//ros::Publisher pub_way_split_pp;
ros::Publisher pub_label;

/*
void ClickedPointCallback(const geometry_msgs::PoseStamped::Ptr& msg)
{
	click_pt = *msg;

   // if(fabs(tmp - click_pt.pose.position.x)>0.001){	
	//    tmp = click_pt.pose.position.x;
        callback_flag = true;
        cout<<"point_num = "<<nn<<endl;
        nn++;
    //}
}*/
/*
void Way_points(void)
{
    static int cnt = 1;//for not use pushback
    pt.points.resize(cnt);
    pt.points[cnt-1].x = click_pt.pose.position.x;     
    pt.points[cnt-1].y = click_pt.pose.position.y;     
    pt.points[cnt-1].z = click_pt.pose.position.z;
    
    fprintf(fp, "%f %f\n", pt.points[cnt-1].x, pt.points[cnt-1].y);//X座標，y座標
    printf("  x: %.5f\n  y: %.5f\n", pt.points[cnt-1].x, pt.points[cnt-1].y);

    cnt++;
    callback_flag = false;
}
*/
struct PointXY{
    float x;
    float y;
};


void splitWaypoint(float split)
{
    fp_w = fopen("split_waypoints.txt", "w");
    
cout<<"333"<<endl; 
    if(fp_w == NULL){
        cout<<" Error: Can not open file!!"<<endl;
        exit (1); 
    }
    
    int cnt=1;
    vector<PointXY> way_p; 
    way_p.resize(cnt);

    float tmp_x, tmp_y;

    while(fscanf(fp_r, "%f %f",&tmp_x, &tmp_y)==2){
        way_p[cnt-1].x = tmp_x;
        way_p[cnt-1].y = tmp_y;
//cout<<"x       ==" << way_p[cnt-1].x<<"y  == "<<way_p[cnt-1].y<<endl;
//cout<<"x       ==" << tmp_x<<"y  == "<<tmp_y<<endl;
        cnt++;
        way_p.resize(cnt);
    }
    
    int end_i = way_p.size();
//cout<<end_i<<endl;    
    float Length, ratio;
    float XX, YY;
    float x0, y0;
    float color = 0.00001;
    int counter = 0;
    for(int i=0;i<end_i-1;i++){
        Length = sqrt( pow((double)(way_p[i+1].x - way_p[i].x), 2.0) + pow((double)(way_p[i+1].y - way_p[i].y), 2.0) );
//cout<<"length"<<Length<<endl;       
        ratio = split/ Length; // 5[m]/ 20[m] => 1/4 
        
        //vt = (way_p[i+1].y - way_p[i].y)/ (way_p[i+1].x - way_p[i].x);

        x0 = way_p[i].x;
        y0 = way_p[i].y;
//cout<<"x0------------------"<<x0<<"y0----------------"<<y0<<endl;
        XX = way_p[i+1].x - way_p[i].x;
        YY = way_p[i+1].y - way_p[i].y;

        XX *= ratio;
        YY *= ratio;
//cout<<"XX------------------"<<XX<<"YY----------------"<<YY<<endl;
        
        fprintf(fp_w, "%f %f\n", x0, y0);
//        printf(" x: %.5f y: %.5f\n", x0, y0);

        geometry_msgs::Point32 pp_temp;
        //if(is_first){
            pp_temp.x = x0;
            pp_temp.y = y0;
            way_split_pp.points.push_back(pp_temp);
        //    is_first = false;
        //}
        if(split<=0.0) continue;
        while(sqrt(pow((double)(way_p[i+1].y-y0), 2.0) + pow((double)(way_p[i+1].x-x0), 2.0)) > split){
            x0 = x0 + XX;
            y0 = y0 + YY;
  //   cout<<"!!!!!!"<<endl;        
            fprintf(fp_w, "%f %f\n", x0, y0);
            printf(" x: %.5f y: %.5f\n", x0, y0);
            counter++;
            pp_temp.x = x0;
            pp_temp.y = y0;
            pp_temp.z = counter*color;
            way_split_pp.points.push_back(pp_temp);
        }
    }
}

/*void Marker_publish(sensor_msgs::PointCloud in, visualization_msgs::Marker way)
{
    size_t size = pt.points.size();
    way.points.resize(size);
    
    for(size_t i=0;i<size;i++){
        way.points[i].x = pt.points[i].x; 
        way.points[i].y = pt.points[i].y;
        way.points[i].z = -1.35;
    }
    
    pt.header.frame_id  ="/map";
    way.header.frame_id="/map";
    //pub_p.header.stamp=ros::Time::now();
    //pub_way.header.stamp=ros::Time::now();
    pub_p.publish(pt);
    pub_way.publish(way);
}*/


int main(int argc, char** argv)
{
   	cout<<"start!!"<<endl;
    ros::init(argc, argv, "split_waypoint");
	ros::NodeHandle n;
  
    char str[100]={0};
    float split;

    //waypoint 格納ファイル入力
    cout << "input waypoint file ==> ";
    cin >> str;
    strcat(str, ".txt");

    //分割[m]
    cout << "split[m]?? ==> ";
    cin >> split;
	//ros::Subscriber sub = n.subscribe("/move_base_simple/goal", 1, ClickedPointCallback);//rviz上座標
    ros::Publisher pub_split_way = n.advertise<sensor_msgs::PointCloud>("split_way_point", 1);
    ros::Publisher pub_way_label = n.advertise<visualization_msgs::MarkerArray>("way_label", 1);
	ros::Publisher pub_way_pair  = n.advertise<visualization_msgs::Marker>("way_pair",1);
    
    
    //#w_pt.header.frame_id = "map";
    //#mk.header.frame_id = "map";
    //#mk2.header.frame_id = "map";
cout<<"111"<<endl; 
    fp_r = fopen(str, "r");
    if(fp_r == NULL){
        cout<<" Error: Can not open file!!"<<endl;
        return (1); 
    }
    
cout<<"222"<<endl; 
    
    //分割関数
    splitWaypoint(split);
      
    fclose(fp_r);
    fclose(fp_w);
    
    visualization_msgs::MarkerArray mk_a;
    visualization_msgs::Marker mk;
    
    size_t WAY_NUM = way_split_pp.points.size(); 
    mk_a.markers.resize(WAY_NUM);
    //mk.points.resize(WAY_NUM*2);
    float height = 0.6; 
    
    mk.ns = "namespace";
    mk.id = 0;
    mk.type = visualization_msgs::Marker::LINE_LIST;
    mk.action = visualization_msgs::Marker::ADD;
    mk.scale.x = 0.70;//0.15
    mk.scale.y = 0.70;//0.1
    mk.scale.z = 0.70;//0.1
    mk.color.a = 1.0;
    mk.color.r = 1.0;
    mk.color.g = 0.0;
    mk.color.b = 1.0;
    mk.header.frame_id = "/map";

    size_t j=0;
    for(size_t i=0;i<WAY_NUM;i++){
        mk_a.markers[i].ns = "namespace";
        mk_a.markers[i].id = i;
        mk_a.markers[i].type = visualization_msgs::Marker::TEXT_VIEW_FACING;
        mk_a.markers[i].action = visualization_msgs::Marker::ADD;
        mk_a.markers[i].color.a = 1.0;
        mk_a.markers[i].color.r = 0.0;
        mk_a.markers[i].color.g = 1.0;
        mk_a.markers[i].color.b = 0.0;
        mk_a.markers[i].scale.z = 1.0;
        mk_a.markers[i].header.frame_id = "/map";
        
        char hoge[100];
        sprintf(hoge, "%5d", (int)i+1);
        mk_a.markers[i].text = hoge;
        mk_a.markers[i].pose.position.x = way_split_pp.points[i].x;
        mk_a.markers[i].pose.position.y = way_split_pp.points[i].y;
        mk_a.markers[i].pose.position.z = way_split_pp.points[i].z + height;
   } 
   for(size_t i=0;i<WAY_NUM-1;i++){
        geometry_msgs::Point p1, p2;
        p1.x = way_split_pp.points[i].x;
        p1.y = way_split_pp.points[i].y;
        p1.z = 0;
        p2.x = way_split_pp.points[i+1].x;
        p2.y = way_split_pp.points[i+1].y;
        p2.z = 0;
        mk.points.push_back(p1);
        mk.points.push_back(p2);

        /*mk.points[j].x = way_split_pp.points[i].x;
        mk.points[j].y = way_split_pp.points[i].y;
        mk.points[j].z = 0;
        mk.points[j+1].x = way_split_pp.points[i+1].x;
        mk.points[j+1].y = way_split_pp.points[i+1].y;
        mk.points[j+1].z = 0;*/
	}

  
    ros::Rate loop_rate(1);
	
    while(ros::ok()){
        {
        way_split_pp.header.frame_id="/map";
        way_split_pp.header.stamp=ros::Time::now();
        pub_split_way.publish(way_split_pp);
        
        pub_way_label.publish(mk_a);
        pub_way_pair.publish(mk);
        }
        ros::spinOnce();
		loop_rate.sleep();
	}
    
    cout<<endl<<"saved waypoint file!!"<<endl;
	return 0;
}

