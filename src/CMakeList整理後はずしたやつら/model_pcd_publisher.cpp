//This template is made for .cpp
//Author: s.shimizu
//
//pkg       : xxx
//filename  : xxx.cpp
//author    : x.xxx
//created   : 20xx.xx.xx
//lastupdate: 20xx.xx.xx


#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <memory.h>
#include <ros/ros.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <std_msgs/Bool.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
//#include <sensor_msgs/Imu.h>

//nav_msgs
//#include <nav_msgs/Odometry.h>

//geometry_msgs

//user defined
//#include <ceres_msgs/AMU_data.h>
//#include<>


//namespaces
using namespace std;	
typedef pcl::PointCloud<pcl::PointNormal> CloudN;
typedef pcl::PointCloud<pcl::PointNormal>::Ptr CloudNPtr;
typedef vector<Eigen::Matrix4f> Nodes;
typedef vector<pcl::PointCloud<pcl::PointNormal> > CloudNs;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> pc_tmp;
pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
bool pc_callback_flag = false;
bool flag_pcd = false;

const float gauss_sphere_range = 1.0;
const size_t skip = 10;
const string OUTPUT_PATH = "map/map_hagi.pcd";
//const string OUTPUT_PATH = "refined/map_0.pcd";

CloudN whole_map;

float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}

CloudN shift(Eigen::Matrix4f m, CloudN cloud){
	CloudN rsl;
	//	cout << "mat=" << endl <<  m << endl << endl;
	for(size_t i=0;i<cloud.points.size();i++){
		Eigen::Vector4f p;
		Eigen::Vector4f n;
		p << cloud.points[i].x, cloud.points[i].y, cloud.points[i].z, 1.0;
		n << cloud.points[i].normal_x, cloud.points[i].normal_y, cloud.points[i].normal_z, 0.0;
		p = m*p;
		n = m*n;
		pcl::PointNormal pnt;
		pnt.x = p(0);
		pnt.y = p(1);
		pnt.z = p(2);
		pnt.normal_x = n(0);
		pnt.normal_y = n(1);
		pnt.normal_z = n(2);
		pnt.curvature = cloud.points[i].curvature;
		rsl.push_back(pnt);
	}
	return rsl;
}

void addCloud(const CloudN &cloud){
	for(size_t i=0;i<cloud.points.size();i++){
		whole_map.push_back(cloud.points[i]);
	}
}

void merge(const Nodes &nodes, const CloudNs &clouds){
	for(size_t i=0;i<nodes.size();i++){
		addCloud( shift(nodes[i],clouds[i]) );
	}
}

Eigen::Matrix4f trans(float dat[7]){
	Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
	Eigen::Matrix3f r;
	/*
	   m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	   m(0,1) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	   m(0,2) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	   m(1,0) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	   m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	   m(1,2) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	   m(2,0) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	   m(2,1) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	   m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	 */////////////
	m(0,0) = 1.0f - 2.0f * dat[4] * dat[4] - 2.0f * dat[5] * dat[5];
	m(1,0) = 2.0f * dat[3] * dat[4] + 2.0f * dat[6] * dat[5];
	m(2,0) = 2.0f * dat[3] * dat[5] - 2.0f * dat[6] * dat[4];

	m(0,1) = 2.0f * dat[3] * dat[4] - 2.0f * dat[6] * dat[5];
	m(1,1) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[5] * dat[5];
	m(2,1) = 2.0f * dat[4] * dat[5] + 2.0f * dat[6] * dat[3];

	m(0,2) = 2.0f * dat[3] * dat[5] + 2.0f * dat[6] * dat[4];
	m(1,2) = 2.0f * dat[4] * dat[5] - 2.0f * dat[6] * dat[3];
	m(2,2) = 1.0f - 2.0f * dat[3] * dat[3] - 2.0f * dat[4] * dat[4];
	////////////////
	m(0,3) = dat[0];
	m(1,3) = dat[1];
	m(2,3) = dat[2];

	return m;
	//return m.inverse();
}

void callback(std_msgs::Bool::ConstPtr msg){
	flag_pcd = msg;
}

void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}


int main (int argc, char** argv)
{
	cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
	cout<<"model gauss保存先のフォルダが空であることを確認すること"<<endl;
	
	
	cout << "output_path = " << OUTPUT_PATH << endl;
	size_t max_index = 5000;
	cout << "max index = ";
	//$$$cin >> max_index ;
	cout << "skip = ";
	size_t skip = 0;
	cin >> skip;

	ros::init(argc, argv, "model_pcd_publisher"); //define node name.
	ros::NodeHandle n;
	ros::Rate roop(10);          //Set Rate[Hz].
	ros::Subscriber sub = n.subscribe("/flag_pcd",1,callback);
	ros::Publisher pub_pcd = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/normal",1);
	//	ros::Publisher pub = n.advertise<sensor_msgs::PointCloud>(topic,1);
	//	ros::Subscriber sub = n.subscribe("/sub_topic",1,callback);
	//	sensor_msgs::PointCloud pc;
	//	pc.header.frame_id = frame;


	/////graphファイルの読み込み
	FILE *fp;
	//fp = fopen("for_model.csv","r");
	fp = fopen("aft.csv","r");
	//fp = fopen("bfr.csv","r");
	if(fp == NULL){
		cerr << "could not load aft.csv" << endl;
		return 1;
	}
	char s[100];
	size_t cnt = 0;
	
	///////////////////////////////////////////////
	FILE *fp2;
	char filename_2[100];
	sprintf(filename_2,"/home/amsl/AMSL_ros_pkg/Dgauss_sphere/Dgauss_list/ikuta.txt");//ファイルの作成
	if((fp2 = fopen(filename_2, "a+")) == NULL){
		fprintf(stderr, "%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	
	
	while(fscanf(fp,"%s",s) != EOF){
		float dat[7];
		int num;
		if (strcmp(s, "VERTEX_SE3:QUAT") == 0){
			cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<endl;
			
			if(fscanf(fp,"%d %f %f %f %f %f %f %f",&num, &dat[0], &dat[1], &dat[2], &dat[3], &dat[4], &dat[5], &dat[6]) != 8)break;
			if(num == 0)continue;
			if(num % skip)continue;
			
			
			///////////////////////////////
			// 処理（ここでは、下記文字をファイルに書き込む）
			fprintf(fp2, "%f,%f\n",dat[0],dat[1] );

			
			dat[0] = 0.0;
			dat[1] = 0.0;
			dat[2] = 0.0;
			
			Eigen::Matrix4f node;
			node = trans(dat);

			cout << "num = " << num << endl;
			char filename[100];
			sprintf(filename,"clouds/cloud_%d.pcd",num);
			pcl::PointCloud<pcl::PointNormal>::Ptr cloud (new pcl::PointCloud<pcl::PointNormal>);
			if (pcl::io::loadPCDFile<pcl::PointNormal> (filename, *cloud) == -1){
				cout << "cloud_" << num << "not found." << endl;
				break;
			}
            
            ////////////////////////////////////////////////////////
            char outfile[100];
			//addCloud( shift(node, *cloud) );
	        pcl::PointCloud<pcl::PointNormal> cloud_fnl;
            cloud_fnl = shift(node, *cloud);
            
            pc_tmp.points.resize( cloud_fnl.points.size() );
            for(size_t i=0;i<cloud_fnl.points.size();i++){
            	pc_tmp.points[i].x = cloud_fnl.points[i].x;
            	pc_tmp.points[i].y = cloud_fnl.points[i].y;
            	pc_tmp.points[i].z = cloud_fnl.points[i].z;
            	pc_tmp.points[i].normal_x = cloud_fnl.points[i].normal_x;
            	pc_tmp.points[i].normal_y = cloud_fnl.points[i].normal_y;
            	pc_tmp.points[i].normal_z = cloud_fnl.points[i].normal_z;
            }
            
            
            ////////////////////////////////////////////////////////
            //回転後のpcdが cloud_fnlに入っている
            
            sensor_msgs::PointCloud2 ros_dgauss_sphere;
            pcl::toROSMsg(pc_tmp, ros_dgauss_sphere);
            
            //flag_pcd  == false でpublishを行う
            flag_pcd =false;
            int count_pub = 0;
            //unsigned int microseconds　= 100;
            while( flag_pcd == false ){
	            if(count_pub == 0){
	            	pub_pcd.publish(ros_dgauss_sphere);
	            	//cout<<ros_dgauss_sphere<<endl;
	     	    	count_pub++;
	     	    	cout<<"IN THE WHILE"<<endl;
	     	    }
	     	    
	     	}
            
            ///////////////////
            //pc_dgsc の初期化
            pc_dgsc.points.clear();
            
            cout << cnt << " / " << max_index << endl << endl;
			if(cnt == max_index)break;
			cnt += skip;
		}
	}
	
	// ファイルを閉じる 
	fclose(fp2);
	
	
	//$$$pcl::io::savePCDFileBinary(OUTPUT_PATH, whole_map);
	cout << "num = " << cnt << endl;
	return 0;
}

