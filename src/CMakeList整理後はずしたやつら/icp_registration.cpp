//
//	icp_registration.cpp
//
//	last update 2015 / 12 / 03
//				


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <vector>
#include <ctime>

using namespace std;
using namespace Eigen;

bool callback_flag=false;

pcl::PointCloud<pcl::PointXYZ>::Ptr pc_raw (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr pc_tf (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZINormal> pc_input;
pcl::PointCloud<pcl::PointXYZINormal> output_raw;
pcl::PointCloud<pcl::PointXYZINormal> output_transfered;
sensor_msgs::PointCloud2 ros_pc;
sensor_msgs::PointCloud2 ros_pc2;

void pc_callback(const sensor_msgs::PointCloud2::Ptr &msg)
{
	if(callback_flag==false){
		cout<<" !!! callback !!!"<<endl;
		ros_pc = *msg;
		//pcl::fromROSMsg(*msg, *pc_tf);
		callback_flag = true;
	}
}

int main (int argc, char** argv)
{
	ros::init(argc, argv, "icp_registration");
	ros::NodeHandle n;	
	ros::Rate loop(20);
	
	ros::Subscriber sub = n.subscribe("/velodyne_points",1,pc_callback);
	
	ros::Publisher raw_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/icp_registration/raw",1);
	ros::Publisher tf_pc_pub = n.advertise<sensor_msgs::PointCloud2>("/icp_registration/transfered",1);

	
	cout << "icp_registration START !!" << endl;
	
	
	int count = 0;
	while(ros::ok()){
		
		
		//
		//	ここに10cmずつずらしてicpを行う箇所を書く
		//	変位量と計算時間を表示する
		//	3mまででいいかな?
		//
		
		if(callback_flag){
			if(count == 0){
				double yyy = 0.0;
				for(yyy=0.0;yyy<5.1;yyy+=0.1){
					//cout<<" yyy = "<<yyy<<endl;
					//pcl::toROSMsg(output_raw,ros_pc);
					ros_pc.header.frame_id="/velodyne";
					ros_pc.header.stamp=ros::Time::now();
					raw_pc_pub.publish(ros_pc);
					//output_raw.points.clear();
					pcl::fromROSMsg(ros_pc, *pc_raw);
					pcl::fromROSMsg(ros_pc, *pc_tf);
					for(size_t i=0;i<pc_tf->points.size();i++){
						pc_tf->points[i].y += yyy;
					}
					pcl::toROSMsg(*pc_tf,ros_pc2);
					ros_pc2.header.frame_id="/velodyne";
					ros_pc2.header.stamp=ros::Time::now();
					tf_pc_pub.publish(ros_pc2);
					//output_raw.points.clear();
					
					pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_m (new pcl::PointCloud<pcl::PointXYZ>);
					pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q (new pcl::PointCloud<pcl::PointXYZ>);
					pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
					for(size_t i=0;i<pc_tf->points.size();i++){
						cloud_q->points.push_back(pc_tf->points[i]);
						cloud_m->points.push_back(pc_raw->points[i]);
					}
					
					//////////////処理時間の計測//////////////
					struct timeval b_sec, a_sec;
					gettimeofday(&b_sec, NULL);
					/////////////////////////////////////////
					
					//icp.setInputCloud(cloud_q);//pcl1.8では廃止???
					icp.setInputSource(cloud_q);
					icp.setInputTarget(cloud_m);
					pcl::PointCloud<pcl::PointXYZ> Final;
					icp.align(Final);
					Eigen::Matrix4f transformation = icp.getFinalTransformation ();//icpによって得られた変換行列をEigenに代入
					
					//////////////処理時間の計測//////////////
					gettimeofday(&a_sec, NULL);
					//cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
					/////////////////////////////////////////
					
					cout<<yyy<<"	"<<yyy+transformation(1,3)<<"	"<<(a_sec.tv_sec - b_sec.tv_sec) + (a_sec.tv_usec - b_sec.tv_usec)*1e-6<<endl;
					
				}
				//callback_flag = false;
			}
			count++;
		}
		
		ros::spinOnce();
		loop.sleep();
	}
	return 0;
}
