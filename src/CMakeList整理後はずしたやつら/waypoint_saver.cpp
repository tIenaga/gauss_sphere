/**********************************************

 src: waypoint_saver.cpp
 
 last_update: '15.06.22
 memo: 
    rviz上でクリックした点の座標(x, y)を格納 
    -->waypoint格納txt作成, pathplanningに役立てよう!!

************************************************/
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>


using namespace std;
bool callback_flag = false;
double tmp = 0.0;
int nn=1;

FILE *fp;//waypoint格納用

geometry_msgs::PoseStamped click_pt;
sensor_msgs::PointCloud pt;
sensor_msgs::PointCloud w_pt;

ros::Publisher pub_p;
ros::Publisher pub_way;
ros::Publisher pub_label;


void ClickedPointCallback(const geometry_msgs::PoseStamped::Ptr& msg)
{
	click_pt = *msg;

   // if(fabs(tmp - click_pt.pose.position.x)>0.001){	
	//    tmp = click_pt.pose.position.x;
        callback_flag = true;
        cout<<"point_num = "<<nn<<endl;
        nn++;
    //}
}

void Way_points(void)
{
    static int cnt = 1;//for not use pushback
    pt.points.resize(cnt);
    pt.points[cnt-1].x = click_pt.pose.position.x;     
    pt.points[cnt-1].y = click_pt.pose.position.y;     
    pt.points[cnt-1].z = click_pt.pose.position.z;
    
    fprintf(fp, "%f %f\n", pt.points[cnt-1].x, pt.points[cnt-1].y);//X座標，y座標
    printf("  x: %.5f\n  y: %.5f\n", pt.points[cnt-1].x, pt.points[cnt-1].y);

    cnt++;
    callback_flag = false;
}

void Marker_publish(/*sensor_msgs::PointCloud in, */visualization_msgs::Marker way)
{
    size_t size = pt.points.size();
    way.points.resize(size);
    
    for(size_t i=0;i<size;i++){
        way.points[i].x = pt.points[i].x; 
        way.points[i].y = pt.points[i].y;
        way.points[i].z = 0.0;
cout<<"way point: x"<<way.points[i].x<<endl; 
    }
    pt.header.frame_id  ="/map";
    way.header.frame_id="/map";
    pt.header.stamp=ros::Time::now();
    way.header.stamp=ros::Time::now();
    pub_p.publish(pt);
    pub_way.publish(way);
}


int main(int argc, char** argv)
{
   	cout<<"start!!"<<endl;
    ros::init(argc, argv, "waypoint_saver");
	ros::NodeHandle n;
  
	ros::Subscriber sub = n.subscribe("/move_base_simple/goal", 1, ClickedPointCallback);//rviz上座標
	pub_p = n.advertise<sensor_msgs::PointCloud>("way_point", 1);
	pub_way = n.advertise<visualization_msgs::Marker>("way", 1);
	pub_label = n.advertise<visualization_msgs::Marker>("label", 1);
    

    visualization_msgs::Marker mk;
    //visualization_msgs::Marker mk2;
    //w_pt.header.frame_id = "world";
    //mk.header.frame_id = "world";
    //mk2.header.frame_id = "world";
    
    mk.ns = "ns";
    mk.id = 0;
    mk.type = visualization_msgs::Marker::CYLINDER;
    mk.action = visualization_msgs::Marker::ADD;
    mk.scale.x = 0.5;
    mk.scale.y = 0.5;
    mk.scale.z = 0.5;
    mk.color.a = 1.0;
    mk.color.r = 0.0;
    mk.color.g = 1.0;
    mk.color.b = 1.0;
    
  /*  mk2.ns = "ns";
    mk2.id = 0;
    mk2.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    mk2.action = visualization_msgs::Marker::ADD;
    mk2.scale.x = 0.5;
    mk2.scale.y = 0.5;
    mk2.scale.z = 0.5;
    mk2.color.a = 1.0;
    mk2.color.r = 1.0;
    mk2.color.g = 1.0;
    mk2.color.b = 1.0;
  */  

    fp = fopen("waypoint_saver.txt", "w");
    if(fp == NULL){
        cout<<" Error: Can not open file!!"<<endl;
        return (1); 
    }

	ros::Rate loop_rate(5);
	
    while(ros::ok()){
        if(callback_flag){
            Way_points();
        }
        Marker_publish(mk); 
        
        ros::spinOnce();
		loop_rate.sleep();
	}
    
    fclose(fp);
    cout<<endl<<"saved waypoint file!!"<<endl;
	return 0;
}

