#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d.h>
#include <pcl_conversions/pcl_conversions.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <clustering.h>

#define N_pub 10

using namespace std;
using namespace Eigen;

sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc2;
ros::Publisher pub;
ros::Publisher pub2;
int min_n = 0;

void pc_callback(sensor_msgs::PointCloud2ConstPtr msg){
	Clustering clustering;
	clustering.setMinMaxNum(min_n,INT_MAX);
	//clustering.setThreshold(0.6, 0.1);	//angle and distance
	clustering.setThreshold(0.80, 0.2);	//angle and distance
	clustering.putTank(msg);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc);
	pc.header.frame_id = "/velodyne";	//principal_components
	pc.header.stamp = ros::Time::now();	//principal_components
	pub.publish(pc);

	//cout<<"gauss principal components pc num : "<<pc.height * pc.width<<endl;
}

void pc_callback2(sensor_msgs::PointCloud2ConstPtr msg){
	Clustering clustering;
	//clustering.setMinMaxNum(min_n,INT_MAX);
	clustering.setMinMaxNum(300,INT_MAX);
	//clustering.setThreshold(0.6, 0.1);	//angle and distance
	clustering.setThreshold(0.80, 0.5);	//angle and distance
	clustering.putTank(msg);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc2);

	pcl::PointCloud<pcl::PointXYZINormal> tmp_clusters;
	pcl::PointCloud<pcl::PointXYZINormal> tmp_pub;
	pcl::fromROSMsg(pc2, tmp_clusters);
	size_t num=pc2.height*pc2.width;
	//if (!num){
	if (num!=0){
		//get the number of member and weight
		tmp_clusters.points.resize(num);
		for (size_t i=0; i<num; ++i){
//			tmp_clusters.points[i].curvature=clustering.getNMember(i);
			tmp_clusters.points[i].intensity=clustering.getWeight(i);
			if(clustering.getWeight(i) <= 1.0){
				tmp_pub.push_back(tmp_clusters.points[i]);
			}
		}
	}
	else{ //to delete the remained points
		tmp_clusters.points.resize(1);
		tmp_clusters.points[0].x=1000;
		tmp_clusters.points[0].y=1000;
		tmp_clusters.points[0].z=1000;
	}
	sensor_msgs::PointCloud2 pc2_w;
	pcl::toROSMsg(tmp_pub, pc2_w);
	pc2_w.header.frame_id="velodyne";
	pc2_w.header.stamp=ros::Time::now();
	pub2.publish(pc2_w);
}

int main (int argc, char** argv)
{
	min_n = 100;
	cout << "Here We Go!!!" << endl;
	ros::init(argc, argv, "PrincipalComponentsExtractor");
  	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("gauss_sphere",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("gauss_sphere_depth",1,pc_callback2);
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2",1);
	
	ros::spin();
	
	return 0;
}
