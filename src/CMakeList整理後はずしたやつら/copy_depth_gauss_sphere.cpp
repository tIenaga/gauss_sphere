#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
// #include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/statistical_outlier_removal.h>//
#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>


using namespace std;
using namespace Eigen;

typedef pcl::PointXYZINormal PointA;
typedef pcl::PointCloud<PointA>  CloudA;
typedef pcl::PointCloud<PointA>::Ptr  CloudAPtr;

ros::Publisher pub_debug;
bool pc_callback_flag = false;
pcl::PointCloud<pcl::PointXYZINormal> pc_input;

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%
const float thresh_dist = 3.0;

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

CloudAPtr ptrTransform(CloudA pcl_in){
	CloudAPtr output(new CloudA);

	// output->width=1;
	// output->height=pcl_in.points.size();
	output->points.resize(pcl_in.points.size());
	for(size_t i=0;i<pcl_in.points.size();i++)
	{
		output->points[i].x=pcl_in.points[i].x;
		output->points[i].y=pcl_in.points[i].y;
		output->points[i].z=pcl_in.points[i].z;
		output->points[i].intensity=pcl_in.points[i].intensity;
		output->points[i].normal_x=pcl_in.points[i].normal_x;
		output->points[i].normal_y=pcl_in.points[i].normal_y;
		output->points[i].normal_z=pcl_in.points[i].normal_z;
		output->points[i].curvature=pcl_in.points[i].curvature;
	}
	return output;
}

void pc_callback(sensor_msgs::PointCloud2::ConstPtr msg){
	pc_callback_flag = true;
	pcl::fromROSMsg(*msg , pc_input);
}

void MAX(int in, int *max){
	if(in > *max) *max =in;
	else{
	}
}

void filtering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output){

	pcl::PointCloud<pcl::PointXYZINormal> pc, pc_out1;
	pc = pc_in;

	int color_cluster_num = int(color_num/color_res);
	hist color[color_cluster_num];
	//init
	for(int i=0;i<color_cluster_num;i++){
		color[i].num = 0;
		color[i].normal_num = 0.0;
		color[i].x = 0.0;
		color[i].y = 0.0;
		color[i].z = 0.0;
		color[i].nx = 0.0;
		color[i].ny = 0.0;
		color[i].nz = 0.0;
		color[i].curv = 0.0;
		color[i].inten = 0.0;
	}

	//make_color_hist
	for(size_t i=0;i<pc.points.size();i++){
		int c_num = int(pc.points[i].intensity/color_res);
		color[c_num].num++;
		color[c_num].x += pc.points[i].x;
		color[c_num].y += pc.points[i].y;
		color[c_num].z += pc.points[i].z;
		color[c_num].nx += pc.points[i].normal_x;
		color[c_num].ny += pc.points[i].normal_y;
		color[c_num].nz += pc.points[i].normal_z;
		color[c_num].curv += pc.points[i].curvature;
		color[c_num].inten += pc.points[i].intensity;
	}

	int max = 0;
	for(int i=0;i<color_cluster_num;i++) MAX(color[i].num, &max);
	for(int i=0;i<color_cluster_num;i++){
		color[i].normal_num = color[i].num/float(max);
		color[i].x /= color[i].num;
		color[i].y /= color[i].num;
		color[i].z /= color[i].num;
		color[i].nx /= color[i].num;
		color[i].ny /= color[i].num;
		color[i].nz /= color[i].num;
		color[i].curv /= color[i].num;
		color[i].inten /= color[i].num;
		if(color[i].normal_num > efficient){
			pcl::PointXYZINormal p;
			p.x = color[i].x;
			p.y = color[i].y;
			p.z = color[i].z;
			p.normal_x = color[i].nx;
			p.normal_y = color[i].ny;
			p.normal_z = color[i].nz;
			p.curvature = color[i].curv;
			p.intensity = color[i].inten;
			pc_out1.points.push_back(p);
		}else{
		}
	}

	*pc_output = pc_out1;
}



void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal>& pc, 
pcl::PointCloud<pcl::PointXYZINormal> &pc_output, pcl::PointCloud<pcl::PointXYZINormal> &pc_output_rm, 
pcl::PointCloud<pcl::PointXYZINormal> &pc_output2, pcl::PointCloud<pcl::PointXYZINormal> &pc_output2_rm){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out_rm, pc_out2, pc_out2_rm;
	pcl::PointCloud<pcl::PointXYZINormal> pc_out_c;

	pc_output.points.resize(pc.points.size());
	// pc_output2.points.resize(pc.points.size());
	for(size_t i=0;i<pc.points.size();i++){
		pc_output.points[i].x = gauss_sphere_range * pc.points[i].normal_x;
		pc_output.points[i].y = gauss_sphere_range * pc.points[i].normal_y;
		pc_output.points[i].z = gauss_sphere_range * pc.points[i].normal_z;
		pc_output.points[i].normal_x = pc.points[i].normal_x;
		pc_output.points[i].normal_y = pc.points[i].normal_y;
		pc_output.points[i].normal_z = pc.points[i].normal_z;
		pc_output.points[i].curvature = pc.points[i].curvature;

		float color = 0.0;

		//find max and min of normal element
		float xyz[3] = {0.0};
		xyz[0] = pc_output.points[i].x;
		xyz[1] = pc_output.points[i].y;
		xyz[2] = pc_output.points[i].z;
		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
		}

		if((fabs(pc_output.points[i].x)>fabs(pc_output.points[i].y))
					&& (fabs(pc_output.points[i].x)>fabs(pc_output.points[i].z))){
			if(pc_output.points[i].x<0)
				color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min)+180.0;
			else color = 30*(fabs(pc_output.points[i].y)-fabs(pc_output.points[i].z))/(max-min);
		}else if((fabs(pc_output.points[i].y)>fabs(pc_output.points[i].z))
					&& (fabs(pc_output.points[i].y)>fabs(pc_output.points[i].x))){
			if(pc_output.points[i].y<0)
				color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(pc_output.points[i].z)-fabs(pc_output.points[i].x))/(max-min) +60.0;
		}else if((fabs(pc_output.points[i].z)>fabs(pc_output.points[i].x))
					&& (fabs(pc_output.points[i].z)>fabs(pc_output.points[i].y))){
			if(pc_output.points[i].z<0)
				color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(pc_output.points[i].x)-fabs(pc_output.points[i].y))/(max-min) +120.0;
		}else{
		}
		// limit
		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		pc_output.points[i].intensity = color;

		// float range = (pc_output2.points[i].normal_x*pc_output2.points[i].x
						// +pc_output2.points[i].normal_y*pc_output2.points[i].y
						// +pc_output2.points[i].normal_z*pc_output2.points[i].z);
		float range = fabs(-pc.points[i].normal_x*pc.points[i].x
						-pc.points[i].normal_y*pc.points[i].y
						-pc.points[i].normal_z*pc.points[i].z);
		// float tmp_range = fabs(pc.points[i].x*pc.points[i].x
						// +pc.points[i].y*pc.points[i].y
						// +pc.points[i].z*pc.points[i].z);
		// if (tmp_range<range) cout<<"over"<<endl;
		if (fabs(pc.points[i].normal_z)<0.9 && pc.points[i].z>0.2){//0.7
			PointA tmp;
			tmp.x = range * pc_output.points[i].normal_x;
			tmp.y = range * pc_output.points[i].normal_y;
			tmp.z = range * pc_output.points[i].normal_z;
			tmp.normal_x=pc.points[i].normal_x;
			tmp.normal_y=pc.points[i].normal_y;
			tmp.normal_z=pc.points[i].normal_z;
			tmp.intensity = color;
			tmp.intensity = color;
			pc_output2.points.push_back(tmp);
			// pc_output2.points[i].x = range * pc_output.points[i].normal_x;
			// pc_output2.points[i].y = range * pc_output.points[i].normal_y;
			// pc_output2.points[i].z = range * pc_output.points[i].normal_z;
			// pc_output2.points[i].normal_x=pc.points[i].normal_x;
			// pc_output2.points[i].normal_y=pc.points[i].normal_y;
			// pc_output2.points[i].normal_z=pc.points[i].normal_z;
			// pc_output2.points[i].intensity = color;
			// pc_output2.points[i].intensity = color;
		}
		
	}
	
  	// Create the filtering object
	CloudAPtr pc_output2_ptr;
	CloudAPtr pc_output2_fil_ptr(new CloudA);
	pc_output2_ptr=ptrTransform(pc_output2);
  	pcl::StatisticalOutlierRemoval<pcl::PointXYZINormal> sor;
  	sor.setInputCloud (pc_output2_ptr);
  	sor.setMeanK (20);
  	sor.setStddevMulThresh (0.1);
	sor.filter (*pc_output2_fil_ptr);
	pc_output2.points.resize(pc_output2_fil_ptr->points.size());
	for (size_t i=0; i<pc_output2_fil_ptr->points.size(); ++i){
		pc_output2.points[i].x=pc_output2_fil_ptr->points[i].x;
		pc_output2.points[i].y=pc_output2_fil_ptr->points[i].y;
		pc_output2.points[i].z=pc_output2_fil_ptr->points[i].z;
		pc_output2.points[i].normal_x=pc_output2_fil_ptr->points[i].normal_x;
		pc_output2.points[i].normal_y=pc_output2_fil_ptr->points[i].normal_y;
		pc_output2.points[i].normal_z=pc_output2_fil_ptr->points[i].normal_z;
	}
	// CloudAPtr pc_output2_ptr;
	// pc_output2_ptr=ptrTransform(pc_output2);

	// pcl::search::KdTree<PointA>::Ptr tree (new pcl::search::KdTree<PointA>);
	// tree->setInputCloud (pc_output2_ptr);
	// std::vector<pcl::PointIndices> cluster_indices;
	// pcl::EuclideanClusterExtraction<PointA> ec;
	// ec.setClusterTolerance (0.3); 
	// ec.setMinClusterSize (300);
	// ec.setMaxClusterSize (10000);
	// ec.setSearchMethod (tree);
	// ec.setInputCloud(pc_output2_ptr);
	// ec.extract (cluster_indices);
  
    // //get cluster information//
	// size_t num=0;
	// pc_output2.points.clear();
	// for(size_t iii=0;iii<cluster_indices.size();iii++){
		// //cluster points
		// pc_output2.points.resize(cluster_indices[iii].indices.size()+num);
		// for(size_t jjj=0;jjj<cluster_indices[iii].indices.size();jjj++){
			// int p_num=cluster_indices[iii].indices[jjj];
			// pc_output2.points[num+jjj]=pc_output2_ptr->points[p_num];
		// }
		// num=pc_output2.points.size();//save previous size
	// }

	//filtering(pc_out, &pc_out_c);
	// pcl::PointCloud<pcl::PointXYZINormal> debug_pcl;
	// for (size_t i=0; i<pc_output2.points.size(); ++i){
		// if (pc_output2[i].z>4.0){
			// debug_pcl.points.push_back(pc.points[i]);
			// cout<<pc.points[i].x<<","<<pc.points[i].y<<","<<pc.points[i].z<<endl;
			// cout<<"ang_xy:"<<atan2(pc.points[i].normal_y,pc.points[i].normal_x)*180.0/M_PI<<endl;
			// cout<<"ang_yz:"<<atan2(pc.points[i].normal_z,pc.points[i].normal_y)*180.0/M_PI<<endl;
			// cout<<"ang_zx:"<<atan2(pc.points[i].normal_x,pc.points[i].normal_z)*180.0/M_PI<<endl<<endl;
		// }
		// else {
			// pc_output2[i].x=0;
			// pc_output2[i].y=0;
			// pc_output2[i].z=0;
		// }
	// }
	// cout<<"----------------------------------"<<endl;
	// sensor_msgs::PointCloud2 debug_ros;
	// pcl::toROSMsg(debug_pcl,debug_ros);
	// debug_ros.header.frame_id="velodyne";
	// debug_ros.header.stamp=ros::Time::now();
	// pub_debug.publish(debug_ros);

	// pc_output = pc_out;
	//*pc_output_rm = pc_out_rm;
	pc_output_rm = pc_out_c;
	// pc_output2 = pc_out2;
	pc_output2_rm = pc_out2_rm;
	pc_out.points.clear();
	pc_out_rm.points.clear();
	pc_out2.points.clear();
	pc_out2_rm.points.clear();

}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "gauss_sphere");
	ros::NodeHandle n;
	
	ros::Rate roop(10);
	
	ros::Subscriber sub = n.subscribe("/perfect_velodyne/normal",1,pc_callback);
	// ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
   	pub_debug = n.advertise<sensor_msgs::PointCloud2>("debug_pt",1);
	cout << "Here We Go !!!" << endl;
	
	while(ros::ok()){

		bool pc_flag = false;
		pcl::PointCloud<pcl::PointXYZINormal> pc_in;
		if(pc_callback_flag && pc_input.points.size()>0){
			pc_callback_flag = false;
			pc_in = pc_input;
			pc_input.points.clear();
			pc_flag = true;
		}else{
		}

		pcl::PointCloud<pcl::PointXYZINormal> pc_gauss_sphere, pc_gauss_sphere_depth, pc_gauss_sphere_f, pc_gauss_sphere_depth_f;
		if(pc_flag) make_gauss_sphere(pc_in, pc_gauss_sphere, pc_gauss_sphere_f, pc_gauss_sphere_depth, pc_gauss_sphere_depth_f);
		else{
		}

		sensor_msgs::PointCloud2 ros_gauss_sphere, ros_gauss_sphere_f, ros_gauss_sphere_depth, ros_gauss_sphere_depth_f;
		pcl::toROSMsg(pc_gauss_sphere, ros_gauss_sphere);
		pcl::toROSMsg(pc_gauss_sphere_f, ros_gauss_sphere_f);
		pcl::toROSMsg(pc_gauss_sphere_depth, ros_gauss_sphere_depth);
		pcl::toROSMsg(pc_gauss_sphere_depth_f, ros_gauss_sphere_depth_f);
		ros_gauss_sphere.header.frame_id = "/velodyne";
		ros_gauss_sphere_f.header.frame_id = "/velodyne";
		ros_gauss_sphere_depth.header.frame_id = "/velodyne";
		ros_gauss_sphere_depth_f.header.frame_id = "/velodyne";
		ros_gauss_sphere.header.stamp = ros::Time::now();
		ros_gauss_sphere_f.header.stamp = ros::Time::now();
		ros_gauss_sphere_depth.header.stamp = ros::Time::now();
		ros_gauss_sphere_depth_f.header.stamp = ros::Time::now();

		pub_gauss_sphere.publish(ros_gauss_sphere);
		pub_gauss_sphere_f.publish(ros_gauss_sphere_f);
		pub_gauss_sphere_depth.publish(ros_gauss_sphere_depth);
		pub_gauss_sphere_depth_f.publish(ros_gauss_sphere_depth_f);

		pc_in.points.clear();
		pc_gauss_sphere.points.clear();
		pc_gauss_sphere_f.points.clear();
		pc_gauss_sphere_depth.points.clear();
		pc_gauss_sphere_depth_f.points.clear();
		ros_gauss_sphere.data.clear();
		ros_gauss_sphere_f.data.clear();
		ros_gauss_sphere_depth.data.clear();
		ros_gauss_sphere_depth_f.data.clear();

		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
