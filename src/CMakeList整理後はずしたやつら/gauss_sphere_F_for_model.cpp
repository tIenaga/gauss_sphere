#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>


using namespace std;
using namespace Eigen;

ros::Publisher pub_gs;
ros::Publisher pub_dgs;
ros::Publisher pub_gsc;
ros::Publisher pub_dgsc;

pcl::PointCloud<pcl::PointXYZINormal> pc_input;
bool pc_callback_flag = false;

const float gauss_sphere_range = 1.0;
const size_t skip = 10;


float MAX(float a, float b){
	if(a>b) return a;
	else return b;
}

float MIN(float a, float b){
	if(a<b) return a;
	else return b;
}

float color(float *xyz){
	float max = 0.0;
	float min = 1.0;
	for(int j=0;j<3;j++){
		max = MAX(max, fabs(xyz[j]));
		min = MIN(min, fabs(xyz[j]));
	}

	float c = 0.0;
	if(max==fabs(xyz[0])){
		if(xyz[0]<0) c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min)+180.0;
		else c = 30*(fabs(xyz[1])-fabs(xyz[2]))/(max-min);
	}else if(max==fabs(xyz[1])){
		if(xyz[1]<0) c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0+180.0;
		else c = 30*(fabs(xyz[2])-fabs(xyz[0]))/(max-min) +60.0;
	}else{
		if(xyz[2]<0)c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0+180.0;
		else c = 30*(fabs(xyz[0])-fabs(xyz[1]))/(max-min) +120.0;
	}

	if(c<0.0) c = 0.0;
	else if(c>360.0) c = 360.0;

	return c;
}

void clustering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output, bool dgs){
	pc_output->points.clear();
	pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud (new pcl::PointCloud<pcl::PointXYZ>);
	for(size_t i=0;i<pc_in.points.size();i+=skip){
		pcl::PointXYZ p;
		p.x = pc_in.points[i].x;
		p.y = pc_in.points[i].y;
		p.z = pc_in.points[i].z;
		input_cloud->points.push_back(p);
	}
	pc_in.points.clear();

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (input_cloud);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

	if(!dgs){
		ec.setClusterTolerance (0.01); 
		ec.setMinClusterSize (50);
		ec.setMaxClusterSize (1000);
	}else{
		ec.setClusterTolerance (0.15); 
		ec.setMinClusterSize (150);
		ec.setMaxClusterSize (1000);
	}	
	ec.setSearchMethod (tree);
	ec.setInputCloud (input_cloud);
	ec.extract (cluster_indices);

	std::vector<pcl::PointIndices>::const_iterator it;
	std::vector<int>::const_iterator pit;
	
	pcl::PointCloud<pcl::PointXYZINormal>pc_out;
	int cluster_num = 0;
	for(it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

		cluster_num++;
		int pc_num = 0; 
		float cx = 0.0;
		float cy = 0.0;
		float cz = 0.0;

		for(pit = it->indices.begin(); pit != it->indices.end(); pit++) {
			cloud_cluster->points.push_back(input_cloud->points[*pit]); 
			cx += input_cloud->points[*pit].x;
			cy += input_cloud->points[*pit].y;
			cz *= input_cloud->points[*pit].z;
			pc_num++;
		}
		
		pcl::PointXYZINormal p2;
		p2.x = cx/pc_num;
		p2.y = cy/pc_num;
		p2.z = cz/pc_num;
		p2.curvature = pc_num;	//point cloud density
		float r = sqrt(pow(p2.x,2)+pow(p2.y,2)+pow(p2.z,2));
		
		float xyz[3] = {0.0};
		xyz[0] = p2.x;
		xyz[1] = p2.y;
		xyz[2] = p2.z;
		p2.intensity = color(xyz);
		
		if(!dgs) pc_out.points.push_back(p2);
		else if(r>1.0) pc_out.points.push_back(p2);
		else{
		}
	}

	if(pc_out.points.size()==0){
		pc_out.points.resize(1);
		pc_out.points[0].x = 1000.0;
		pc_out.points[0].y = 1000.0;
		pc_out.points[0].z = 1000.0;
	}
	*pc_output = pc_out;
}

void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal> pc, pcl::PointCloud<pcl::PointXYZINormal> *pc_output,pcl::PointCloud<pcl::PointXYZINormal> *pc_output2){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out2;

	for(size_t i=0;i<pc.points.size();i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;

		//for localization
		if(fabs(p.normal_z) > 0.8) continue;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;
		float c = color(xyz);

		p.intensity = c;
		pc_out.points.push_back(p);

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = c;

		float range = fabs(pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		//天井デバッグ
/*		if(c>75 && c<85 && sqrt(pp.x*pp.x+pp.y*pp.y+pp.z*pp.z)>5){
			cout<<pc.points[i].x<<" "<<pc.points[i].y<<" "<<pc.points[i].z<<endl;
			pc_out2.points.push_back(pp);
		}
*/
		int r = int(sqrt(pow(pp.x,2)+pow(pp.y,2)+pow(pp.z,2)));
		for(int j=0;j<r;j++) pc_out2.points.push_back(pp);
	}

	*pc_output = pc_out;
	*pc_output2 = pc_out2;
	pc_out.points.clear();
	pc_out2.points.clear();
}

void pcl2ros(pcl::PointCloud<pcl::PointXYZINormal> *pcl_pc, sensor_msgs::PointCloud2 *ros_pc){
	ros_pc->data.clear();
	pcl::toROSMsg(*pcl_pc, *ros_pc);
	ros_pc->header.frame_id = "/velodyne";
	ros_pc->header.stamp = ros::Time::now();
	pcl_pc->points.clear();
}

void process(){
	bool pc_flag = false;
	pcl::PointCloud<pcl::PointXYZINormal> pc_in;
	if(pc_callback_flag && pc_input.points.size()>0){
		pc_callback_flag = false;
		pc_in = pc_input;
		pc_input.points.clear();
		pc_flag = true;
	}else{
		cout<<"subscribe no points."<<endl;
	}

	pcl::PointCloud<pcl::PointXYZINormal> pc_gs, pc_dgs, pc_gsc, pc_dgsc;
	if(pc_flag){
		make_gauss_sphere(pc_in, &pc_gs, &pc_dgs);
		pc_in.points.clear();
		clustering(pc_gs, &pc_gsc, false);
		clustering(pc_dgs, &pc_dgsc, true);
	}else{
	}

	sensor_msgs::PointCloud2 ros_gs, ros_dgs, ros_gsc, ros_dgsc;
	pcl2ros(&pc_gs, &ros_gs);
	pcl2ros(&pc_dgs, &ros_dgs);
	pcl2ros(&pc_gsc, &ros_gsc);
	pcl2ros(&pc_dgsc, &ros_dgsc);
	
	pub_gs.publish(ros_gs);
	pub_dgs.publish(ros_dgs);
	pub_gsc.publish(ros_gsc);
	pub_dgsc.publish(ros_dgsc);
}

void pc_callback(sensor_msgs::PointCloud2::ConstPtr msg){
	pc_callback_flag = true;
	pcl::fromROSMsg(*msg , pc_input);
	process();
}

int main (int argc, char** argv)
{
	ros::init(argc, argv, "gauss_sphere");
	ros::NodeHandle n;
	
	//ros::Subscriber sub = n.subscribe("pcd_file_all",1,pc_callback);//1026hagi
	ros::Subscriber sub = n.subscribe("normal",1,pc_callback);//1026hagi
	//ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);
    pub_gs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	pub_gsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_clustered",1);
   	pub_dgs = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	pub_dgsc = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_clustered",1);

	cout << "Here We Go !!!" << endl;
	
	ros::spin();
	return 0;
}
