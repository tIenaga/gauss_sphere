#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d.h>
#include <pcl_conversions/pcl_conversions.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <clustering.h>
#include <ctime>

#define N_pub 10

using namespace std;
using namespace Eigen;

bool dgauss = true;

sensor_msgs::PointCloud2 pc;
sensor_msgs::PointCloud2 pc2;
ros::Publisher pub;
ros::Publisher pub2;
int min_n = 100;

void pc_callback(sensor_msgs::PointCloud2ConstPtr msg){
	Clustering clustering;
	clustering.setMinMaxNum(min_n,INT_MAX);
	clustering.setThreshold(0.6, 0.1);	//angle and distance
	//clustering.setThreshold(0.80, 0.1);	//angle and distance 1022
	//clustering.setThreshold(0.80, 0.05);	//angle and distance
	clustering.putTank(msg);
	clustering.process();
	clustering.calcWeight();
	clustering.showClusterSize();
	clustering.showClusters();
	clustering.getClusters(pc);
	pc.header.frame_id = "/velodyne";	//principal_components
	pc.header.stamp = ros::Time::now();	//principal_components
	pub.publish(pc);

	cout<<"gauss principal components pc num : "<<pc.height * pc.width<<endl;
}

void pc_callback2(sensor_msgs::PointCloud2ConstPtr msg){

	if(dgauss){
		
		//////////////処理時間の計測//////////////
		struct timeval b_sec, a_sec;
		gettimeofday(&b_sec, NULL);
	    /////////////////////////////////////////
		
		dgauss = false;
		
		Clustering clustering;
		clustering.setMinMaxNum(min_n,INT_MAX);
		//clustering.setThreshold(0.6, 0.1);	//angle and distance
		clustering.setThreshold(0.80, 0.1);	//0803 元のやつ
		//clustering.setThreshold(0.85, 0.1);	//0826 3スキャン分でいろいろやってたときのパラメタ
		//clustering.setThreshold(0.20, 0.025);	//angle and distance
		//clustering.setThreshold(0.90, 0.1);
		clustering.putTank(msg);
		clustering.process();
		clustering.calcWeight();
		clustering.showClusterSize();
		clustering.showClusters();
		clustering.getClusters(pc2);
		pc2.header.frame_id = "/velodyne";	//principal_components
		pc2.header.stamp = ros::Time::now();	//principal_components
		pub2.publish(pc2);
	
		cout<<"d-gauss principal components pc num : "<<pc2.height * pc2.width<<endl;
		
		//////////////処理時間の計測//////////////
		gettimeofday(&a_sec, NULL);
		cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
		/////////////////////////////////////////
		
		dgauss = true;
	}
}

int main (int argc, char** argv)
{
	//min_n = 100;
	cout << "Here We Go!!!" << endl;
	ros::init(argc, argv, "PrincipalComponentsExtractor_for_DGauss");
  	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("gauss_sphere",1,pc_callback);
	ros::Subscriber sub2 = n.subscribe("gauss_sphere_depth",1,pc_callback2);
	//ros::Subscriber sub2 = n.subscribe("/perfect_velodyne/normal",1,pc_callback2);
	pub = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components",1);
	pub2 = n.advertise<sensor_msgs::PointCloud2>("/perfect_velodyne/principal_components2",1);
	
	ros::spin();
	
	return 0;
}
