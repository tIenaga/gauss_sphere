//
//	last update 1105
//


#include <iostream>
#include <memory.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Core>
#include <Eigen/SVD>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <velodyne_msgs/VelodyneScan.h>
#include <omp.h>
#include <ctime>


using namespace std;
using namespace Eigen;

bool pc_callback_flag = false;
pcl::PointCloud<pcl::PointXYZINormal> pc_input;

const float gauss_sphere_range = 1.0;

const float L = gauss_sphere_range;   //[m]
const float W = gauss_sphere_range;   //[m]
const float H = gauss_sphere_range;   //[m]
const float voxel_res = 0.20;    //[m]

const int color_num = 360;
const int color_res = 10;
//const float efficient = 0.1; // = 10%
const float efficient = 0.05; // = 5%

typedef struct{
	int num;
	float normal_num;
	float x;
	float y;
	float z;
	float nx;
	float ny;
	float nz;
	float curv;
	float inten;
}hist;

void pc_callback(sensor_msgs::PointCloud2::ConstPtr msg){
	pc_callback_flag = true;
	pcl::fromROSMsg(*msg , pc_input);
}

void MAX(int in, int *max){
	if(in > *max) *max =in;
	else{
	}
}

void filtering(pcl::PointCloud<pcl::PointXYZINormal> pc_in, pcl::PointCloud<pcl::PointXYZINormal> *pc_output){

	pcl::PointCloud<pcl::PointXYZINormal> pc, pc_out1;
	pc = pc_in;

	int color_cluster_num = int(color_num/color_res);
	hist color[color_cluster_num];
	//init
	for(int i=0;i<color_cluster_num;i++){
		color[i].num = 0;
		color[i].normal_num = 0.0;
		color[i].x = 0.0;
		color[i].y = 0.0;
		color[i].z = 0.0;
		color[i].nx = 0.0;
		color[i].ny = 0.0;
		color[i].nz = 0.0;
		color[i].curv = 0.0;
		color[i].inten = 0.0;
	}

	//make_color_hist
	for(size_t i=0;i<pc.points.size();i++){
		int c_num = int(pc.points[i].intensity/color_res);
		color[c_num].num++;
		color[c_num].x += pc.points[i].x;
		color[c_num].y += pc.points[i].y;
		color[c_num].z += pc.points[i].z;
		color[c_num].nx += pc.points[i].normal_x;
		color[c_num].ny += pc.points[i].normal_y;
		color[c_num].nz += pc.points[i].normal_z;
		color[c_num].curv += pc.points[i].curvature;
		color[c_num].inten += pc.points[i].intensity;
	}

	int max = 0;
	for(int i=0;i<color_cluster_num;i++) MAX(color[i].num, &max);
	for(int i=0;i<color_cluster_num;i++){
		color[i].normal_num = color[i].num/float(max);
		color[i].x /= color[i].num;
		color[i].y /= color[i].num;
		color[i].z /= color[i].num;
		color[i].nx /= color[i].num;
		color[i].ny /= color[i].num;
		color[i].nz /= color[i].num;
		color[i].curv /= color[i].num;
		color[i].inten /= color[i].num;
		if(color[i].normal_num > efficient){
			pcl::PointXYZINormal p;
			p.x = color[i].x;
			p.y = color[i].y;
			p.z = color[i].z;
			p.normal_x = color[i].nx;
			p.normal_y = color[i].ny;
			p.normal_z = color[i].nz;
			p.curvature = color[i].curv;
			p.intensity = color[i].inten;
			pc_out1.points.push_back(p);
		}else{
		}
	}

	*pc_output = pc_out1;
}



void make_gauss_sphere(pcl::PointCloud<pcl::PointXYZINormal> pc, 
pcl::PointCloud<pcl::PointXYZINormal> *pc_output, pcl::PointCloud<pcl::PointXYZINormal> *pc_output_rm, 
pcl::PointCloud<pcl::PointXYZINormal> *pc_output2, pcl::PointCloud<pcl::PointXYZINormal> *pc_output2_rm){
	
	pcl::PointCloud<pcl::PointXYZINormal> pc_out, pc_out_rm, pc_out2, pc_out2_rm;
	pcl::PointCloud<pcl::PointXYZINormal> pc_out_c;
	size_t size_pc_points = pc.points.size();
	for(size_t i=0;i<size_pc_points;i++){
		pcl::PointXYZINormal p;
		p.x = gauss_sphere_range * pc.points[i].normal_x;
		p.y = gauss_sphere_range * pc.points[i].normal_y;
		p.z = gauss_sphere_range * pc.points[i].normal_z;
		p.normal_x = pc.points[i].normal_x;
		p.normal_y = pc.points[i].normal_y;
		p.normal_z = pc.points[i].normal_z;
		p.curvature = pc.points[i].curvature;
		
		//for localization
		if(fabs(p.normal_z) > 0.8) continue;
		if(p.z <= -0.5) continue;
		
		float color = 0.0;

		float xyz[3] = {0.0};
		xyz[0] = p.x;
		xyz[1] = p.y;
		xyz[2] = p.z;

		float max = 0.0;
		float min = 1.0;
		for(int j=0;j<3;j++){
			if(max < fabs(xyz[j])) max = fabs(xyz[j]);
			else{
			}

			if(min > fabs(xyz[j])) min = fabs(xyz[j]);
			else{
			}
		}

		if((fabs(p.x)>fabs(p.y)) && (fabs(p.x)>fabs(p.z))){
			if(p.x<0) color = 30*(fabs(p.y)-fabs(p.z))/(max-min)+180.0;
			else color = 30*(fabs(p.y)-fabs(p.z))/(max-min);
		}else if((fabs(p.y)>fabs(p.z)) && (fabs(p.y)>fabs(p.x))){
			if(p.y<0) color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0+180.0;
			else color = 30*(fabs(p.z)-fabs(p.x))/(max-min) +60.0;
		}else if((fabs(p.z)>fabs(p.x)) && (fabs(p.z)>fabs(p.y))){
			if(p.z<0)color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0+180.0;
			else color = 30*(fabs(p.x)-fabs(p.y))/(max-min) +120.0;
		}else{
		}

		if(color<0.0) color = 0.0;
		else if(color>360.0) color = 360.0;

		p.intensity = color;
		pc_out.points.push_back(p);

		pcl::PointXYZINormal pp;
		pp.x = pc.points[i].x;
		pp.y = pc.points[i].y;
		pp.z = pc.points[i].z;
		pp.normal_x = pc.points[i].normal_x;
		pp.normal_y = pc.points[i].normal_y;
		pp.normal_z = pc.points[i].normal_z;
		pp.curvature = pc.points[i].curvature;
		pp.intensity = color;

		float range = (pp.normal_x*pp.x+pp.normal_y*pp.y+pp.normal_z*pp.z)/(sqrt(pow(pp.normal_x,2)+pow(pp.normal_y,2)+pow(pp.normal_z,2)));
		pp.x = range * p.x;
		pp.y = range * p.y;
		pp.z = range * p.z;

		pc_out2.points.push_back(pp);
	}

	filtering(pc_out, &pc_out_c);//これがモデルとの違いの場所

	*pc_output = pc_out;
	//*pc_output_rm = pc_out_rm;
	*pc_output_rm = pc_out_c;
	*pc_output2 = pc_out2;
	*pc_output2_rm = pc_out2_rm;
	
	pc_out.points.clear();
	pc_out_rm.points.clear();
	pc_out2.points.clear();
	pc_out2_rm.points.clear();

}


int main (int argc, char** argv)
{
	ros::init(argc, argv, "gauss_sphere");
	ros::NodeHandle n;
	
	ros::Rate roop(10);//10//0803にこの値から変更(主平面の確認のために)
						//20//1008
						
	
	//
	//use ↓ with "rosrun pcd_loader pcd_loader"
	//ros::Subscriber sub = n.subscribe("pcd_file_all",1,pc_callback);//1026hagi
	
	ros::Subscriber sub = n.subscribe("perfect_velodyne/normal",1,pc_callback);//0928//normal_estimation用に変更した
	//ros::Subscriber sub = n.subscribe("normal",1,pc_callback);
	
	//ros::Subscriber sub = n.subscribe("PointCloud_21",1,pc_callback);//1026hagi
	//ros::Subscriber sub = n.subscribe("pcd_file_local_initialized_position",1,pc_callback);//0928//デフォのトピック名
	
   	ros::Publisher pub_gauss_sphere = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere",1);
   	ros::Publisher pub_gauss_sphere_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_filtered",1);
   	ros::Publisher pub_gauss_sphere_depth = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth",1);
   	ros::Publisher pub_gauss_sphere_depth_f = n.advertise<sensor_msgs::PointCloud2>("gauss_sphere_depth_filtered",1);
	cout << "Here We Go !!!" << endl;
	
	while(ros::ok()){

		bool pc_flag = false;
		pcl::PointCloud<pcl::PointXYZINormal> pc_in;
		if(pc_callback_flag && pc_input.points.size()>0){
			pc_callback_flag = false;
			pc_in = pc_input;
			pc_input.points.clear();
			pc_flag = true;
		}else{
		}
		
		//////////////処理時間の計測//////////////
		struct timeval b_sec, a_sec;
		gettimeofday(&b_sec, NULL);
	    /////////////////////////////////////////
		
		pcl::PointCloud<pcl::PointXYZINormal> pc_gauss_sphere, pc_gauss_sphere_depth, pc_gauss_sphere_f, pc_gauss_sphere_depth_f;
		if(pc_flag) make_gauss_sphere(pc_in, &pc_gauss_sphere, &pc_gauss_sphere_f, &pc_gauss_sphere_depth, &pc_gauss_sphere_depth_f);
		else{
		}
		
		//////////////処理時間の計測//////////////
		gettimeofday(&a_sec, NULL);
		cout<<"process time is "<<(a_sec.tv_sec - b_sec.tv_sec)*1e3 + (a_sec.tv_usec - b_sec.tv_usec)*1e-3<<" [ms]"<<endl;
		/////////////////////////////////////////
		
		sensor_msgs::PointCloud2 ros_gauss_sphere, ros_gauss_sphere_f, ros_gauss_sphere_depth, ros_gauss_sphere_depth_f;
		pcl::toROSMsg(pc_gauss_sphere, ros_gauss_sphere);
		pcl::toROSMsg(pc_gauss_sphere_f, ros_gauss_sphere_f);
		pcl::toROSMsg(pc_gauss_sphere_depth, ros_gauss_sphere_depth);
		pcl::toROSMsg(pc_gauss_sphere_depth_f, ros_gauss_sphere_depth_f);
		ros_gauss_sphere.header.frame_id = "/velodyne";
		ros_gauss_sphere_f.header.frame_id = "/velodyne";
		ros_gauss_sphere_depth.header.frame_id = "/velodyne";
		ros_gauss_sphere_depth_f.header.frame_id = "/velodyne";
		ros_gauss_sphere.header.stamp = ros::Time::now();
		ros_gauss_sphere_f.header.stamp = ros::Time::now();
		ros_gauss_sphere_depth.header.stamp = ros::Time::now();
		ros_gauss_sphere_depth_f.header.stamp = ros::Time::now();

		pub_gauss_sphere.publish(ros_gauss_sphere);
		pub_gauss_sphere_f.publish(ros_gauss_sphere_f);
		pub_gauss_sphere_depth.publish(ros_gauss_sphere_depth);
		pub_gauss_sphere_depth_f.publish(ros_gauss_sphere_depth_f);

		pc_in.points.clear();
		pc_gauss_sphere.points.clear();
		pc_gauss_sphere_f.points.clear();
		pc_gauss_sphere_depth.points.clear();
		pc_gauss_sphere_depth_f.points.clear();
		ros_gauss_sphere.data.clear();
		ros_gauss_sphere_f.data.clear();
		ros_gauss_sphere_depth.data.clear();
		ros_gauss_sphere_depth_f.data.clear();

		ros::spinOnce();
		roop.sleep();
	}
	return 0;
}
